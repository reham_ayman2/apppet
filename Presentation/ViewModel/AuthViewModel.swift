//
// Created by NoobMaster69 on 05/04/2022.
//

import Foundation

class AuthViewModel: ObservableObject {
    func signIn(_ email: String, _ password: String, onFinish: @escaping (APIResponse<SignInResponse>) -> Void) {
        return UserAPIRequests.login(email: email, password: password, onFinish: onFinish)
    }

    func forgetPassword(email: String, onFinish: @escaping (APIResponse<ForgetPasswordResponse>) -> ()) {
        return UserAPIRequests.forgetPassword(email: email, onFinish: onFinish)
    }

    func signUp(name: String, email: String, password: String, phone: String, onFinish: @escaping (_ apiResponse: APIResponse<SignUpResponse>) -> ()) {
        return UserAPIRequests.signUp(name: name, email: email, password: password, phone: phone, onFinish: onFinish)
    }


    func storeUserData(_ user: APIUser, _ token: String?) {
        UserDefaultsUtil.setUserToken(token ?? "")
        UserDefaultsUtil.setUser(user)
    }
}
