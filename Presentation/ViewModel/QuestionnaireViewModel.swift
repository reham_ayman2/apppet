//
//  QuestionnaireViewModel.swift
//  Vet Questionnaire SwiftUI
//
//  Created by NoobMaster69 on 04/03/2022.
//

import Foundation
import SwiftUI
import Combine

class QuestionnaireViewModel: ObservableObject {
    
    init() {
        print("View Model Is init Created")
    }
    init(questionnaire : PresentationQuestionnaire){
        self.questionnaire = questionnaire
        print("View Model Is Created Questionnaire")
    }

    init(petsCount: Int) {
        self.petsCount = petsCount
        print("View Model Is Created petCounts")
    }
    @Published var petName: String = ""
    @Published private var currentQuestionIdx = 0
    @Published var currentPetImage: UIImage? = nil
    @Published private var questionnaire = PresentationQuestionnaire(questionsList: StaticQuestions.initialQuestions(petName: ""))
    @Published private var currentPetBuilder = PresentationPet.Builder()
    @Published private var currentUserBuilder = PresentationUser.Builder()
    @Published var petsCount: Int = 1
    @Published var currentPetCount: Int = 1
    
    private var cancellable = Set<AnyCancellable>()
    
    var currentQuestion: PresentationQuestion {
        print(currentQuestionIdx)
        print(questionnaire.getQuestions().count)
        return questionnaire.getQuestions()[currentQuestionIdx % questionnaire.getQuestions().count]
    }

    func fetchPrevQuestion() {
        currentQuestionIdx = currentQuestionIdx < 1 ? 0 : currentQuestionIdx - 1
        debugPrint("\(currentQuestionIdx)")
//        currentQuestionIdx -= 1
    }

    func setCurrentPetImage(_ image: UIImage?) {
        currentPetBuilder = currentPetBuilder.setImage(image)
        currentPetImage = image
    }

    func fetchNextQuestion() {
        currentQuestionIdx += 1
    }

//    func addQuestionAndAnswer(question: PresentationQuestion, answers: [PresentationAnswer]) {
//        if currentQuestionIdx < getQuestionsCount() {
//            questionnaire.addQuestionAndAnswers(question: question, answers: answers)
//        }
//    }

    func popUpQuestion() {
        questionnaire.popQuestion()
    }

    func getCurrentQuestionIdx() -> Int {
        currentQuestionIdx
    }

    func getQuestionsCount() -> Int {
        return questionnaire.getQuestions().count
    }

    func startQuestionnaire() {
        currentQuestionIdx = 1
    }

    func getAnswers() -> [PresentationAnswer] {
        questionnaire.getAnswers()
    }

    func restartQuestionnaire() {
//        DispatchQueue.main.async {
            self.currentQuestionIdx = 0
            self.questionnaire = PresentationQuestionnaire(questionsList: StaticQuestions.initialQuestions(petName: ""))
//        }
    }

    func submitQuestion(answerString: [String]) {
        if (currentQuestion.answerType == .text) {
            questionnaire.addQuestionAndAnswers(question: currentQuestion, answers: answerString.map(PresentationAnswer.fromString))
        } else {
            let answers: [PresentationAnswer] = currentQuestion.answers.filter { answer in
                answerString.contains(answer.content)
            }
            questionnaire.addQuestionAndAnswers(question: currentQuestion, answers: answers)
        }
        currentPetBuilder = currentQuestion.updatePetBuilder(currentPetBuilder, answerString[0])

    }

    func submitPetsCount(catCount: Int, dogCount: Int) {
    
        petsCount = catCount + dogCount
        currentUserBuilder = currentUserBuilder.setPetsCount(petsCount)
    }

    func isLastQuestion() -> Bool {
        return currentQuestionIdx == questionnaire.getQuestions().count - 1 && currentQuestionIdx > 0
    }

    func allPetsFilled() -> Bool {
     
        petsCount+1 == currentPetCount
        
    }

    func submitPet(onPetSubmit: @escaping (APIResponse<SubmitPetResponse>) -> ()) {
        print(currentPetBuilder.build())
        QuestionaryServices.submitPet(pets: currentPetBuilder.build())
            .receive(on: RunLoop.main)
            .subscribe(on: RunLoop.main)
            .sink { comp in

                debugPrint(comp)
            } receiveValue: { [weak self] result in
                onPetSubmit(result)
                guard let self = self else {return}
                self.currentPetBuilder = PresentationPet.Builder()
                self.currentPetImage = nil
                self.restartQuestionnaire()
            }.store(in: &cancellable)

//        PetAPIRequests.submitPet(currentPetBuilder.build()) { response in
//            onPetSubmit(response)
//            DispatchQueue.main.async {
//
//            }
//        }

    }

    func getCurrentSpecie() -> String {
        (currentPetBuilder.build().species ?? "").lowercased()
    }
}



