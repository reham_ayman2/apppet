//
// Created by NoobMaster69 on 11/04/2022.
//

import Foundation

class PetProfilesViewModel: ObservableObject {
    @Published var userPets: [PresentationPet] = []

    init() {
        updateUserPets()
    }

    func updateUserPets() {
        PetAPIRequests.getUserPets { response in
            DispatchQueue.main.async { 
                self.userPets = response.data?.createdPets.map { apiPet in
                    
                            PresentationPet.fromAPIPet(apiPet)
                        }
                        .uniqued() ?? []
            }

        
        }
    }
}
