//
//  HomePageViewModel.swift
//  Vet Questionnaire SwiftUI
//
//  Created by NoobMaster69 on 06/04/2022.
//

import Foundation

class HomePageViewModel: ObservableObject {
    @Published var userName = UserDefaults.standard.string(forKey: UserConstants.userName.rawValue) ?? ""
    @Published var userPets: [PresentationPet] = [PresentationPet]()
    @Published var currentPetIdx: Int = 0 {
        didSet {
            updatePetActivities()
            updatePetFood()
        }
    }
    @Published var currentPetCalories: [(String, Double)] = []
    @Published var currentPetActivities: [(String, Double)] = []
    @Published var currentActivity : (String,Double)?
    @Published var currentPetAverageCalories = "0 kcal"
    @Published var currentPetAverageActivityTime = "0H 0M"

    init() {
   
       
            updatePetActivities()
            updatePetFood()
        


    }
    func fetchPetData (){
        
        PetAPIRequests.getUserPets { [self] response in
            
            DispatchQueue.main.async { [self] in
                self.userPets = response
                        .data?
                        .createdPets
                        .map { apiPet in
                            PresentationPet.fromAPIPet(apiPet)
                        }
                        .uniqued() ?? []

                self.currentPetIdx = userPets.first?.id ?? 0
            }
        }
    }
    private func updatePetFood() {
        PetAPIRequests.getPetFood(petId: currentPetIdx) { response in
            if response.isSuccessful() {
                if let data = response.data {
                    
                    DispatchQueue.main.async { [self] in
                        let groupedData = Dictionary(grouping: data.foodActivity) { key -> String in
                            let dateFormatter = DateFormatter()
                            dateFormatter.locale = Locale(identifier: "en_US_POSIX") // set locale to reliable US_POSIX
                            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                            let date = dateFormatter.date(from: key.createdAt ?? "") ?? Date()
                            dateFormatter.dateFormat = "EEEE"
                            let dayInWeek = dateFormatter.string(from: date)
                            print(key)
                            print(dayInWeek)
                            return dayInWeek

                        }
                        self.currentPetCalories = groupedData.map { key, value -> (String, Double) in
                                    return (key, value.reduce(0.0) { acc, food -> Double in
                                        1.0 * acc + (Double(food.calPerUnit) * Double(food.noOfUnits))
                                    })
                                }
                                .sorted(by: { first, second in
                                    let dateFormatter = DateFormatter()
                                    dateFormatter.dateFormat = "EEEE"
                                    return dateFormatter.date(from: first.0)! < dateFormatter.date(from: second.0)!
                                })

                        let avg = self.currentPetCalories.reduce(0.0) { acc, pair -> Double in
                            acc + pair.1
                        }

                        self.currentPetAverageCalories = String("\(avg) Kcal")

                    }
                } else {
                    self.currentPetCalories = []
                    self.currentPetAverageCalories = "0 kcal"            }

                    }

        }
    }

    private func updatePetActivities() {

        PetAPIRequests.getPetActivities(petId: currentPetIdx) { response in
            if response.isSuccessful() {
                
                DispatchQueue.main.async {
                    if let data = response.data {
                        let groupedData = Dictionary(grouping: data.petExercises, by: { key -> String in
                            let dateFormatter = DateFormatter()
                            dateFormatter.locale = Locale(identifier: "en_US_POSIX") // set locale to reliable US_POSIX
                            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.000000Z"
                            let date = dateFormatter.date(from: key.createdAt)!
                            dateFormatter.dateFormat = "EEEE"
                            let dayInWeek = dateFormatter.string(from: date)

                            return dayInWeek

                        })
                        self.currentPetActivities = groupedData.map { key, value -> (String, Double) in

                                    (key, value.reduce(0.0) { acc, ex -> Double in
                                        1.0 * acc + (Double(ex.mins) / 60.0) + Double(ex.hours)
                                    })
                                }
                                .sorted(by: { first, second in
                                    let dateFormatter = DateFormatter()
                                    dateFormatter.dateFormat = "EEEE"
                                    return dateFormatter.date(from: first.0)! < dateFormatter.date(from: second.0)!
                                })

                        let avg = self.currentPetActivities.reduce(0.0) { acc, pair -> Double in
                            acc + pair.1
                        }
                        let hours = Int(avg)
                        let mins = Int((avg - Double(hours)) * 60)
                        self.currentPetAverageActivityTime = String("\(hours)h \(mins)m")
                    }
                    
                    self.currentActivity = self.currentPetActivities[0]
                    print("Current Activity = \(self.currentActivity)")
                }
              
            } else {
                DispatchQueue.main.async {
                self.currentPetActivities = []
                self.currentPetAverageActivityTime = "0H 0M";
                self.currentActivity = ("", 0.0)
                print("Current Activity 2 = \(self.currentActivity)")
                }
            }
        }
    }
}
