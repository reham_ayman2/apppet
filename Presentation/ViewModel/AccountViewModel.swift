//
// Created by NoobMaster69 on 11/04/2022.
//

import Foundation

class AccountViewModel: ObservableObject {
    @Published var currentUser = UserDefaultsUtil.getCurrentUser()
    @Published var userPets =  [PresentationPet]()
    
//            (1...10).map { i in
//        var d = DummyPets.dummyPet
//        d.id = i
//        return d
//    }
    init() {
        fetchUserPets()
        fetchUserData()

    }

    func fetchUserData() {

    }

    func fetchCatCount()-> Int {
      
        var catCount = 0
        for pet in userPets {
            if (pet.species == "cat") {
                catCount += 1
            }
        }
        return catCount
    }
    
    func fetchDogCount()-> Int {
        
        var dogCount = 0
        for pet in userPets {
            if (pet.species == "dog") {
                dogCount += 1
            }
        }
        return dogCount
    }

    func fetchUserPets() {
    
        
                PetAPIRequests.getUserPets { [self] response in
            
            DispatchQueue.main.async { [self] in
                self.userPets = response.data?.createdPets.map { apiPet in
                            return PresentationPet.fromAPIPet(apiPet)
                        }
                        .uniqued() ?? []
            }
        }

    }


    func deletePet(_ pet: PresentationPet, finish: @escaping (_ apiResponse: APIResponse<DeletePetResponse>) -> Void) {
        PetAPIRequests.deletePet(pet, finish: finish)
    }

    func signOut(onFinish: @escaping (_ apiResponse: APIResponse<LogOutResponse>) -> ()) {
        UserAPIRequests.signOut(onFinish: onFinish)
    }

    func clearUserData() {
        let defaults = UserDefaults.standard
        let dictionary = defaults.dictionaryRepresentation()
        dictionary.keys.forEach { key in
            defaults.removeObject(forKey: key)
        }
    }

    func updateProfile(name: String, email: String, phone: String, onResponse: @escaping (APIResponse<UpdateUserProfileResponse>) -> ()) {
        UserAPIRequests.updateProfile(name: name, email: email, phone: phone, onFinish: onResponse)
    }

    func storeUserData(_ user: APIUser) {
        UserDefaultsUtil.setUser(user)
    }
}
