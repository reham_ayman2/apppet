//
//  EntriesViewModel.swift
//  Vet Questionnaire SwiftUI
//
//  Created by NoobMaster69 on 19/04/2022.
//

import Foundation

class EntriesViewModel: ObservableObject {

    @Published var userPets: [PresentationPet] = []

    @Published var currentPetId: Int = 0 {
        didSet {
            fetchPetActivities()
        }
    }


    @Published var currentDate: String {
        didSet {
            fetchPetActivities()
        }
    }

    @Published var currentPetActivities: [PresentationActivity] = []

    var dates: [String] {
        let startDate = Calendar.current.date(byAdding: .day, value: -365, to: Date())!
        let endDate = Calendar.current.date(byAdding: .day, value: 365, to: Date())!
        let dayDurationInSeconds: TimeInterval = 60 * 60 * 24
        let formatter = DateFormatter()
        formatter.dateFormat = "dd-MM-yyyy"
        return stride(from: startDate, to: endDate, by: dayDurationInSeconds).map { date in
            formatter.string(from: date)
        }
    }

    init() {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd-MM-yyyy"
        currentDate = formatter.string(from: Date())
        fetchUserPets()

    }

    func fetchUserPets() {
        PetAPIRequests.getUserPets { response in
            self.userPets = response.data?.createdPets.map { apiPet in
                        return PresentationPet.fromAPIPet(apiPet)
                    }
                    .uniqued() ?? []

            self.currentPetId = self.userPets.first?.id ?? 0

            self.fetchPetActivities()
        }
    }

    func fetchPetActivities() {
        PetAPIRequests.getPetActivities(petId: currentPetId) { response in
            if response.isSuccessful() {
                self.currentPetActivities = response.data?.petExercises.map { exercise in
                    print(exercise.exerciseName)
                    let dateFormatter = DateFormatter()
                    dateFormatter.locale = Locale(identifier: "en_US_POSIX") // set locale to reliable US_POSIX
                    dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.000000Z"
                    return PresentationActivity(id: exercise.id,
                            type: .exercise,
                            title: exercise.exerciseName,
                            subtitle: "\(exercise.hours)H \(exercise.mins)M",
                            createdAt: dateFormatter.date(from: exercise.createdAt) ?? Date()
                    )
                } ?? []

                PetAPIRequests.getPetFood(petId: self.currentPetId) { apiResponse in
                    if apiResponse.isSuccessful() {
                        let petFood: [PresentationActivity] = apiResponse.data?.foodActivity.map { food in
                                    let dateFormatter = DateFormatter()
                                    dateFormatter.locale = Locale(identifier: "en_US_POSIX") // set locale to reliable US_POSIX
                                    dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                                    return PresentationActivity(id: food.foodActivityID,
                                            type: .food,
                                            title: food.products ?? "",
                                            subtitle: food.ingredients ?? "",
                                            createdAt: dateFormatter.date(from: food.createdAt ?? "")!
                                    )
                                }
                                .sorted(by: { $0.createdAt < $1.createdAt }) ?? []

                        self.currentPetActivities.append(contentsOf: petFood)
                        self.currentPetActivities = self.currentPetActivities.sorted(by: { $0.createdAt > $1.createdAt })

                    }
                }
            }
        }
    }

    func addActivity(_ type: String, _ exercise: String, _ food: String, _ hours: String, _ mins: String, _ calories: String) {

        if type == PresentationActivity.ActivityType.exercise.rawValue {
            PetAPIRequests.submitPetExercise(currentPetId, exercise, Int(hours) ?? 0, Int(mins) ?? 0) { response in
                self.fetchPetActivities()
            }
        } else {
            PetAPIRequests.submitPetFood(currentPetId, calories) { response in
                self.fetchPetActivities()
            }
        }
    }
}
