//
//  Colors.swift
//  Vet Questionnaire SwiftUI
//
//  Created by NoobMaster69 on 12/03/2022.
//

import SwiftUI
class AppColors{
    static let primaryColor = Color(red: 10/255.0, green: 37/255.0, blue: 168/255.0)
    static let secondaryColor = Color(red: 0/255.0, green: 173/255.0, blue: 53/255.0)
    static let lightGrey =  Color(red: 221/255.0, green: 219/255.0, blue: 219/255.0,opacity: 0.75)
    static let lightBlue = Color(red: 227/255.0, green: 239/255.0, blue: 255/255.0)
    static let lightGrayBlue = Color(red: 248/255.0, green: 251/255.0, blue: 255/255.0)
    static let mediumLightBlue = Color(red: 118/255.0, green: 149/255.0, blue: 197/255.0)
    static let mediumLightGreen = Color(red: 178/255.0, green: 230/255.0, blue: 194/255.0)
    static let backgroundBlue = Color(red: 10/255.0, green: 73/255.0, blue: 168/255.0,opacity: 1)


}
