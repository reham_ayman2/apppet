//
// Created by NoobMaster69 on 11/04/2022.
//

import Foundation
import SwiftUI

struct AddPetCardView: View {

    var body: some View {
        VStack{

            Image(systemName: "plus.circle.fill")
                    .resizable()
                    .scaledToFit()
                    .frame(maxHeight: 60)
                    .foregroundColor(AppColors.secondaryColor)
                    .padding()


            Text("Add Pet").bold().font(.custom("Poppins-Bold", size: 18))
                .padding(.bottom)
            

        }.padding(.horizontal)
            .padding()
                .background(Color.white)
                .clipShape(RoundedRectangle(cornerRadius: 24))
    }
}
struct AddPetView_Previews: PreviewProvider {
    static var previews: some View {
        ZStack{
            Color.black.ignoresSafeArea()
            AddPetCardView()

        }
    }
}
