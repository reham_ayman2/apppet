//
// Created by NoobMaster69 on 01/04/2022.
//

import Foundation
import SwiftUI
import Focuser

struct DatePickerAnswerView: View {
    @State var isShowingOptions: Bool = true
    @State var selectedDate: Date = Date()
    @State var years: String = " "
    @State var months: String = " "
    @State var date: Date = Date()

    @FocusStateLegacy var selectedField: String? = ""
    let onAnswerChange: ([String]) -> ()

    init(_ onAnswerChange: @escaping ([String]) -> ()) {
        self.onAnswerChange = onAnswerChange
    }


    @State var selected: String = "Answer"

    var body: some View {

        VStack {

            HStack(alignment: .top) {
                DatePicker("Birth date: ", selection: $selectedDate, displayedComponents: .date)

                Spacer(minLength: 75)

                Image(systemName: "calendar").foregroundColor(AppColors.secondaryColor)
                        .frame(maxHeight: 40, alignment: .center)
                        .font(.system(size: 30))

            }
                    .padding()
                    .overlay(
                            RoundedRectangle(cornerRadius: 8)
                                    .stroke(AppColors.secondaryColor, lineWidth: 2)
                    ).padding(16)
                    .onChange(of: selectedDate) { date in
                        let format = DateFormatter()
                        format.dateFormat = "YYYY-MM-dd"
                        self.date = date
                        onAnswerChange(["\(format.string(from: date)),\(years),\(months)"])
                    }
                    .onChange(of: years) { years in
                        let format = DateFormatter()
                        format.dateFormat = "YYYY-MM-dd"
                        onAnswerChange(["\(format.string(from: date)),\(years),\(months)"])
                    }
                    .onChange(of: months) { months in
                        let format = DateFormatter()
                        format.dateFormat = "YYYY-MM-dd"
                        onAnswerChange(["\(format.string(from: date)),\(years),\(months)"])
                    }
                    .animation(.linear)
            Text("If you’re unsure of your pet’s birthday, tell us your pet’s age instead.")
                    .bold()
                    .padding()
                    .font(.system(size: 18))

            RoundedBorderTextField(title: "Year(s)", hint: "1", boundValue: $years, focusedField: _selectedField)

            RoundedBorderTextField(title: "Month(s)", hint: "1", boundValue: $months, focusedField: _selectedField, isLast: true)
        }
    }
}
