//
// Created by NoobMaster69 on 01/04/2022.
//

import Foundation
import SwiftUI
import Focuser

struct InputAnswerView: View {

    let keyboardType: UIKeyboardType
    let onAnswerChange: ([String]) -> ()
    @State var answer: String = ""
    @FocusStateLegacy var focusedValue: String?

    var body: some View {
        RoundedBorderTextField(title: "", hint: "Answer", boundValue: $answer, focusedField: _focusedValue, isLast: true)
                .onChange(of: answer) { answer in
                    onAnswerChange([answer])
                }
    }
}
