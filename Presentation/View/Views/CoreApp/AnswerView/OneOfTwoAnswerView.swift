//
// Created by NoobMaster69 on 01/04/2022.
//

import Foundation
import SwiftUI

struct OneOfTwoAnswerView: View {
    let answers: [PresentationAnswer]
    let onAnswerChange: ([String]) -> ()

    @State var selectedItem: String = ""
    var body: some View {
        VStack {
            Spacer()
            HStack {
                ForEach(answers) { answer in
                    if let image = answer.image {
                        VStack {
                            Image(uiImage: image)
                                    .frame(width: 100, height: 100)
                                    .background(selectedItem == answer.content ? AppColors.secondaryColor : Color.white)
                                    .foregroundColor(AppColors.secondaryColor)
                                    .font(.largeTitle)
                                    .shadow(radius: 16.0)
                                    .background(Color.white)
                                    .clipShape(RoundedRectangle(cornerRadius: 16))
                                    .overlay(
                                            RoundedRectangle(cornerRadius: 16)
                                                    .stroke(selectedItem == answer.content ? AppColors.secondaryColor : AppColors.lightGrey,
                                                            lineWidth: selectedItem == answer.content ? 5.0 : 2.0))
                            Text(answer.content)
                                    .foregroundColor(selectedItem == answer.content ? AppColors.secondaryColor : Color.black)
                        }
                                .padding().onTapGesture {
                                    onAnswerChange([answer.content])
                                    selectedItem = answer.content
                                }
                    } else {
                        Text(answer.content)
                                .bold()
                                .padding()
                                .onTapGesture {
                                    onAnswerChange([answer.content])
                                    selectedItem = answer.content
                                }
                                .font(.system(size: 24))
                                .foregroundColor(selectedItem == answer.content ? Color.white : Color.black)
                                .frame(maxWidth: 100, maxHeight: 100, alignment: .center)
                                .shadow(radius: 16.0)
                                .background(selectedItem == answer.content ? AppColors.secondaryColor : Color.white)
                                .clipShape(RoundedRectangle(cornerRadius: 16))
                                .overlay(
                                        RoundedRectangle(cornerRadius: 16)
                                                .stroke(selectedItem == answer.content ? AppColors.secondaryColor : AppColors.lightGrey,
                                                        lineWidth: selectedItem == answer.content ? 5.0 : 2.0)).padding()
                    }
                }
            }
            Spacer()
        }
    }
}
