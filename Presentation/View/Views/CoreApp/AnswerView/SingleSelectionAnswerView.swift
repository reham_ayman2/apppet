//
// Created by NoobMaster69 on 01/04/2022.
//

import Foundation
import SwiftUI

struct SingleSelectionAnswerView: View {
    @State var isShowingOptions: Bool = false

    init(_ image: UIImage?, _ answers: [PresentationAnswer], _ onAnswerChange: @escaping ([String]) -> ()) {
        self.answers = answers
        self.onAnswerChange = onAnswerChange
        self.image = image
    }

    let onAnswerChange: ([String]) -> ()
    let answers: [PresentationAnswer]
    let image: UIImage?
    var answersString: [String] {
        answers.map { answer in
            answer.content
        }
    }

    @State var selected: String = "Answer"

    var body: some View {

        VStack {
            if let image = image {
                Image(uiImage: image)
                        .resizable()
                        .frame(maxWidth: .infinity)
                        .aspectRatio(contentMode: .fit)
                        .clipShape(RoundedRectangle(cornerRadius: 16))
                        .overlay(
                                RoundedRectangle(cornerRadius: 16)
                                        .stroke(AppColors.secondaryColor, lineWidth: 2)
                        )
                        .padding([.horizontal], 8)
                        .padding(.top, 10)


            }
            HStack(alignment: .top) {
                Text(selected)
                        .onChange(of: selected) { answer in
                            onAnswerChange([answer])
                        }
                        .frame(alignment: .leading)
                Spacer()

                Image(systemName: "chevron.up").foregroundColor(AppColors.secondaryColor)
                        .frame(maxHeight: 16, alignment: .center)

            }
                    .padding()
                    .overlay(RoundedRectangle(cornerRadius: 8)
                            .stroke(AppColors.secondaryColor, lineWidth: 2)
                    ).padding(16)

                    .frame(maxWidth: .infinity, maxHeight: 325, alignment: .topLeading)
                    .onTapGesture {
                        isShowingOptions.toggle()
                    }
                    .overlay(
                            withAnimation {
                                VStack {
                                    if isShowingOptions {
                                        SimpleDropDown(items: answersString) { answer in
                                            onAnswerChange([answer])
                                            isShowingOptions = false
                                            selected = answer
                                        }.padding()
                                    }
                                }.transition(.slide)
                            }.transition(.slide), alignment: .bottomLeading

                    )
                    .onChange(of: answers) { _ in
                        selected = "Answer"
                    }
                    .animation(.linear)
        }
    }
}
