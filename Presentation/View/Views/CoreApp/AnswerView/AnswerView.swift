//
//  AnswerView.swift
//  Vet Questionnaire SwiftUI
//
//  Created by NoobMaster69 on 12/03/2022.
//

import SwiftUI

struct AnswerView: View {

    let answers: [PresentationAnswer]
    let answerType: PresentationQuestion.AnswerType
    let image: UIImage?
    let specie: String
    var currentPetImage: UIImage?
    @Binding var showImagePicker: Bool
    @Binding var showPickerSource: Bool
    @Binding var source: UIImagePickerController.SourceType
    let onAnswerChange: ([String]) -> ()
    let onImageSubmit: ((UIImage) -> ())?
    var body: some View {
        switch answerType {
        case .singleChoice:
            SingleSelectionAnswerView(image, answers, onAnswerChange)

        case .date:
            DatePickerAnswerView(onAnswerChange)

        case .multiChoice:
            MultiSelectionAnswerView(answers, onAnswerChange)

        case .number:
            InputAnswerView(keyboardType: .numberPad, onAnswerChange: onAnswerChange)


        case .text:
            InputAnswerView(keyboardType: .default, onAnswerChange: onAnswerChange)

        case .oneOfTwo:
            OneOfTwoAnswerView(answers: answers, onAnswerChange: onAnswerChange)

        case .image:
            if let onImageSubmit = onImageSubmit {
                ImagePickerQuestionView(specie: specie,
                        onImageSubmit: onImageSubmit,
                        image:  currentPetImage,
                        showImagePicker: $showImagePicker,
                        showPickerSource: $showPickerSource,
                        source: $source)
            } else {
                EmptyView()
            }
        }
    }
}
