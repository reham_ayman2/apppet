//
//  ImagePickerView.swift
//  Vet Questionnaire SwiftUI
//
//  Created by NoobMaster69 on 31/03/2022.
//

import SwiftUI

struct ImagePickerQuestionView: View {

    let specie: String
    let onImageSubmit: (UIImage) -> ()

    var image: UIImage?
    @Binding var showImagePicker: Bool
    @Binding var showPickerSource: Bool
    @Binding var source: UIImagePickerController.SourceType

    var body: some View {
        VStack {


            ZStack {
                if let image = image {
                    Image(uiImage: image)
                        .resizable()
                        .scaledToFit()
                        .clipShape(Circle())
//                            .resizable()
//                            scaledToFill()
//                            .frame(minWidth: 0,  maxWidth: 180, minHeight: 0, maxHeight: 180)

                            
                } else {
                    if specie == "dog" {
                        Image("Image-Demo")
                            .resizable()
                            .scaledToFit()
                            .clipShape(Circle())
//                                .resizable()
//                                .scaledToFill()
//                                .frame(minWidth: 0,  maxWidth: 180, minHeight: 0, maxHeight: 180)

                                
                    } else {
                        Image("Image-Demo")
                            .resizable()
                            .scaledToFit()
                            .clipShape(Circle())
//                                .resizable()
//                                .scaledToFill()
//                                .frame(minWidth: 0,  maxWidth: 180, minHeight: 0, maxHeight: 180)
                                
                    }
                }
            }
                    .frame(minWidth: 0, maxWidth: 200)
                
                    .padding()
                    .padding(.horizontal, 50)

            Spacer()
            Button {
                    showPickerSource = true

            } label: {
                Image(systemName: "tray.and.arrow.up")
                                        .resizable()
                                        .scaledToFit()
                                        .foregroundColor(AppColors.secondaryColor)
                                        .frame(minWidth: 0, maxWidth: 20 ,maxHeight: 20)
                                        .padding(.vertical)
                Text("Upload")
                        .foregroundColor(AppColors.secondaryColor)

            }   .shadow(color: .gray, radius: 10, x: 0.5, y: 0.5)

//            HStack {
//
//                Image(systemName: "tray.and.arrow.up")
//                        .resizable()
//                        .scaledToFit()
//                        .foregroundColor(AppColors.secondaryColor)
//                        .frame(minWidth: 0, maxWidth: 200 )
//                        .padding(.vertical)
//                        .onTapGesture {
//                            showPickerSource = true
//                        }
//
////                Text("Upload")
////                    .foregroundColor(AppColors.secondaryColor)
//            }.shadow(color: .gray, radius: 8, x: 0.5, y: 0.5)
        }
                .sheet(isPresented: $showImagePicker) {
                    ImagePicker(sourceType: source) { image in
                        onImageSubmit(image)
                    }
                            .ignoresSafeArea()
                }


    }
}

