//
//  SignUpPageView.swift
//  Vet Questionnaire SwiftUI
//
//  Created by NoobMaster69 on 16/03/2022.
//

import SwiftUI
import Focuser
import KeyboardToolbars

struct SignUpPageView: View {
    @EnvironmentObject var viewModel: AuthViewModel

    @FocusStateLegacy var focusedField: String? = ""

    @State var userName: String = ""
    @State var emailAddress: String = ""
    @State var password: String = ""
    @State var mobileNumber: String = ""
    @State var isLoading: Bool = false
    @State var errorOccurred: Bool = false
    @State var currentError: String? = nil
    @State var destActivated: Bool = false
    let signUpDest = PetsCountQuestionView(viewModel: QuestionnaireViewModel())

    var body: some View {
        NavigationView {
            ZStack {
                VStack {

                    Image(uiImage: UIImage(named: "AppLogo")!)
                            .resizable()
                            .scaledToFit()
                            .frame(maxWidth: 200, alignment: .top)
                            .padding(20)
                            .padding(.top)


                    VStack(alignment: .trailing) {

                        VStack {
                            Text("Sign Up")
                                    .font(.custom("Poppins-SemiBold", size: 32))
                                    .bold()

                            Text("Sign up as a pet owner")
                                    .font(.custom("Poppins-Regular", size: 18))

                        }
                                .frame(maxWidth: .infinity)
                                .multilineTextAlignment(.center)
                                .foregroundColor(.black)
                                .padding([.top, .horizontal], 10)
                                .padding()

                        RoundedBorderTextField(title: "Username", hint: "e.g. sampleusername", boundValue: $userName, focusedField: _focusedField).padding(.horizontal)


                        RoundedBorderTextField(title: "Email Address", hint: "e.g. sample@gmail.com", type: .EMAIL, boundValue: $emailAddress, focusedField: _focusedField).padding(.horizontal)


                        RoundedBorderTextField(title: "Password",
                                hint: "Enter your password", type: .PASSWORD, boundValue: $password, focusedField: _focusedField).padding(.horizontal)


                        RoundedBorderTextField(title: "Mobile Number", hint: "000 - 000 - 000", boundValue: $mobileNumber, focusedField: _focusedField, isLast: true).padding(.horizontal)

                        NavigationLink("", destination: signUpDest, isActive: $destActivated)
                        Button("Sign up") {
                            isLoading = true
                            viewModel.signUp(name: $userName.wrappedValue,
                                    email: $emailAddress.wrappedValue,
                                    password: $password.wrappedValue,
                                    phone: $mobileNumber.wrappedValue) { apiResponse in
                                isLoading = false
                                if apiResponse.isSuccessful() {
                                    destActivated = true
                                    if let user = apiResponse.data?.user {
                                        viewModel.storeUserData(user, apiResponse.data?.token)
                                    }
                                } else {
                                    currentError = apiResponse.message
                                    errorOccurred = true
                                }
                            }
                        }
                                .frame(maxWidth: .infinity, alignment: .center)
                                .padding()
                                .foregroundColor(.white)
                                .background(AppColors.secondaryColor)
                                .clipShape(RoundedRectangle(cornerRadius: 12))
                                .padding()

                        HStack {
                            Text("Don’t have an account?")
                            NavigationLink(destination: SignInPageView()) {
                                Text("Sign in")
                            }
                        }
                                .frame(maxWidth: .infinity, alignment: .center)
                                .padding()
                                .padding(.bottom, 20)
                    }
                            .background(RoundedCorners(color: .white, tl: 30, tr: 30))
                            .frame(maxWidth: .infinity, alignment: .bottom)

                }
                        .overlay(LoadingView(isLoading: $isLoading))
                        .alert(isPresented: $errorOccurred) {
                            Alert(title: Text("Sign up"), message: Text(currentError ?? ""))
                        }
                        .navigationBarBackButtonHidden(true)
                        .navigationBarHidden(true)
                        .ignoresSafeArea(.all, edges: .bottom).frame(maxHeight: .infinity, alignment: .bottom)

            }
                    .ignoresSafeArea(.all, edges: .bottom)
                    .navigationBarBackButtonHidden(true)
                    .navigationBarHidden(true)
        }
                .navigationBarBackButtonHidden(true)
                .navigationBarHidden(true)
                .frame(maxHeight: .infinity, alignment: .bottom)
                .addHideKeyboardButton()

    }

}

struct SignUpPageView_Previews: PreviewProvider {
    static var previews: some View {
        SignUpPageView()
    }
}
