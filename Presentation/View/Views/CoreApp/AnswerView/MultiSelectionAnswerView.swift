//
// Created by NoobMaster69 on 01/04/2022.
//

import Foundation
import SwiftUI

struct MultiSelectionAnswerView: View {
    let onAnswerChange: ([String]) -> ()
    var answers: [PresentationAnswer]
    @State var selectedAnswers: [String]
    @State var checked: [Bool]

    init(_ answers: [PresentationAnswer], _ onAnswerChange: @escaping ([String]) -> ()) {
        self.answers = answers
        self.onAnswerChange = onAnswerChange
        selectedAnswers = []
        checked = Array(0...answers.count).map { i in
            false
        }
    }

    var body: some View {
        VStack(alignment: .leading) {
            ForEach(0..<answers.count, id: \.self) { idx in
                HStack {
                    Text(answers[idx].content)
                            .bold()
                            .onChange(of: checked, perform: { checked in
                                var newList: [String] = [String]()
                                for i in 0..<checked.count where checked[i] {
                                    newList.append(answers[i].content)
                                }
                                onAnswerChange(newList)
                            })
                            .font(.system(size: 20))
                    Spacer()
                    CheckBoxView(checked: $checked[idx])

                }.padding([.horizontal, .top])

            }
        }
                .onChange(of: answers, perform: { answers in
                    selectedAnswers = []
                    for i in 0..<checked.count {
                        checked[i] = false
                    }
                })
    }
}

