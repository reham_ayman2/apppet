//
//  ActivityListItemView.swift
//  Vet Questionnaire SwiftUI
//
//  Created by NoobMaster69 on 21/04/2022.
//

import SwiftUI

struct ActivityListItemView: View {
    let activity : PresentationActivity
    var body: some View {
        
        HStack{
            VStack(alignment: .leading){
                Text(activity.type.rawValue)
                    .font(.custom("Poppins-regular",size: 12))
                    .frame(maxWidth: .infinity,alignment: .leading).padding([.horizontal,.top])
                
                Text(activity.title)
                    .font(.custom("Poppins-SemiBold",size: 18))
                    .frame(maxWidth: .infinity,alignment: .leading).padding(.horizontal)
                
                Spacer()
                
                Text(activity.subtitle)
                    .font(.custom("Poppins-regular",size: 12))
                    .frame(maxWidth: .infinity,alignment: .leading).padding()
                
            }

            Spacer()
            Image(activity.type == .food ? "dog-food" : "wool").padding()
        }.frame(maxWidth:.infinity,maxHeight: 140)
            .background(activity.type == PresentationActivity.ActivityType.exercise ? AppColors.mediumLightBlue : AppColors.mediumLightGreen)
            .clipShape(RoundedRectangle(cornerRadius: 16))
            .padding()
            .foregroundColor(activity.type == .exercise ? .white : .black)

    }
}

struct ActivityListItemView_Previews: PreviewProvider {
    static var previews: some View {
        ZStack{
            Color.red.ignoresSafeArea()

            ActivityListItemView(activity: PresentationActivity(id: 5, type: .food, title: "Commercial Brand", subtitle: "20 Kcal",createdAt: Date()))
        }
    }
}
