//
//  RoundedBorderTextField.swift
//  Vet Questionnaire SwiftUI
//
//  Created by NoobMaster69 on 18/03/2022.
//

import SwiftUI
import Focuser
import Introspect

struct RoundedBorderTextField: View {
    let title : String
    let hint : String
    let type : FieldType
    @State var isLast : Bool
    @Binding var boundValue : String
    @FocusStateLegacy var focusedField: String?
    @State var borderColor = Color.gray
    var focusedValue : String {
        if isLast {
            return  "LAST"
        }
        else {
            return  title
        }
    }
    
    init(title:String,hint:String,
         type:FieldType = .TEXT,
         boundValue:Binding<String> ,
         focusedField : FocusStateLegacy<String>,isLast:Bool = false){
        self.title = title
        self.hint = hint
        self.type = type
        self._boundValue = boundValue
        self._focusedField = focusedField
        self.isLast = isLast
    }
    
    var body: some View {
        VStack{
            Text(title)
                .bold()
                .multilineTextAlignment(.leading)
                .foregroundColor(borderColor)
                .frame(maxWidth:.infinity,alignment: .leading)
                .padding(4)
            
            getTextField(type: type)
                .padding(12)
                .overlay(
                    RoundedRectangle(cornerRadius: 8)
                        .stroke(borderColor, lineWidth: 2)
                ).disableAutocorrection(true)
            
        }
            .padding(.top,4)
            .onChange(of: focusedField, perform: {focusedField in
                borderColor = focusedField == focusedValue ? AppColors.secondaryColor : Color.gray
            })
        
    }
    
    enum FieldType {
        case TEXT
        case PASSWORD
        case NUMBER
        case EMAIL
    }
    
    @ViewBuilder private func getTextField(type:FieldType) -> some View {
        
        switch type {
        case .TEXT:  TextField(hint,text: $boundValue)
                .focusedLegacy($focusedField, equals: focusedValue)

        case .PASSWORD:  SecureField(hint,text: $boundValue)
                .focusedLegacy($focusedField, equals: focusedValue)

        case .NUMBER: TextField(hint,text: $boundValue)
                .keyboardType(.numberPad)
                .focusedLegacy($focusedField, equals: focusedValue)
        case .EMAIL :
            TextField(hint,text: $boundValue)
                    .keyboardType(.emailAddress)
                    .focusedLegacy($focusedField, equals: focusedValue)

        }
    }
}

struct RoundedBorderTextField_Previews: PreviewProvider {
    @State static var v = ""
    @FocusStateLegacy static var focusedField: String?
    
    static var previews: some View {
        RoundedBorderTextField(title:"Password", hint:"Enter your password",boundValue: $v, focusedField: _focusedField)
    }
}
extension String: FocusStateCompliant {
    
    
    static public  var last: String {
        "LAST"
    }
    
    public var next: String? {
        ""
    }
}
