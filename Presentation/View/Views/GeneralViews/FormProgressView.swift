//
//  FormProgressView.swift
//  Vet Questionnaire SwiftUI
//
//  Created by NoobMaster69 on 14/03/2022.
//

import SwiftUI

struct FormProgressView: View {
    let totalSteps:Int
    let currentStepIdx : Int
    var body: some View {
        HStack{
            ForEach(1...totalSteps, id: \.self){idx in
                if(idx==currentStepIdx){
                    RoundedRectangle(cornerRadius: 50)
                        .fill(AppColors.primaryColor)
                        .frame(width: 15, height: 7)
                }
                else{
                    Circle()
                        .fill(AppColors.primaryColor)
                        .frame(width: 7, height: 7)
                        .opacity(0.5)
                }
                
            }
        }
    }
}

struct FormProgressView_Previews: PreviewProvider {
    static var previews: some View {
        FormProgressView(totalSteps: 10,currentStepIdx: 7)
    }
}
