//
//  CheckBoxView.swift
//  Vet Questionnaire SwiftUI
//
//  Created by NoobMaster69 on 15/03/2022.
//

import SwiftUI

struct CheckBoxView: View {
    @Binding var checked: Bool

    var body: some View {
        Image(systemName: checked ? "checkmark.square.fill" : "square")
            .foregroundColor(AppColors.secondaryColor)
            .onTapGesture { self.checked.toggle() }
            .font(.custom("Poppins-Regular",size: 24))

    }
}

struct CheckBoxView_Previews: PreviewProvider {
    struct CheckBoxViewHolder: View {
        @State var checked = false

        var body: some View {
            CheckBoxView(checked: $checked)
        }
    }

    static var previews: some View {
        CheckBoxViewHolder()
    }
}
