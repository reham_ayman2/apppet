//
// Created by NoobMaster69 on 11/04/2022.
//

import Foundation
import SwiftUI

struct HeaderView: View {
    var body: some View {
        HStack {
            Image("AppLogo").resizable()
                    .scaledToFit()
                    .frame(maxWidth: 115)
            Spacer()
            Image(systemName: "bell")
                    .resizable()
                    .scaledToFit()
                    .frame(maxWidth: 24)
                    .foregroundColor(.gray)

        }
                .padding()
                .background(Color.white)
    }
}
