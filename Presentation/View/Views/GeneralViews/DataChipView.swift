//
//  DataChipView.swift
//  Vet Questionnaire SwiftUI
//
//  Created by NoobMaster69 on 08/04/2022.
//

import SwiftUI

struct DataChipView: View {
    let imageName : String
    let title : String
    let subTitle : String
    var body: some View {
        HStack{
            Image(imageName)
                .resizable()
                .scaledToFit()
                .frame(maxWidth:40, maxHeight: 40)
                .padding(8)
            VStack{
                Text(title).font(.system(size: 16)).opacity(0.6).frame(maxWidth:.infinity,alignment: .leading)
                Text(subTitle).font(.system(size: 20)).bold().frame(maxWidth:.infinity,alignment: .leading)
            }.frame(maxWidth:.infinity,alignment: .leading)
                .multilineTextAlignment(.leading)
        } .frame(maxWidth:.infinity, maxHeight: 75)
            .background(Color.white)
            .clipShape(RoundedRectangle(cornerRadius: 15))

    }
}

struct DataChipView_Previews: PreviewProvider {
    static var previews: some View {
        ZStack{
            Color.black.ignoresSafeArea()
            DataChipView(imageName:"Icon-tennisball",
                         title: "Activities Avg",
                         subTitle: "1800 KCal").padding(4)
        }.ignoresSafeArea()
        
    }
}
