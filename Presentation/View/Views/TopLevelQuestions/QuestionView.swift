//
//  QuestionView.swift
//  Vet Questionnaire SwiftUI
//
//  Created by NoobMaster69 on 04/03/2022.
//

import SwiftUI

// MARK: QuestionViewContainer
struct QuestionView: View {
    
    init(content questionContent: String, image: UIImage? = nil) {
        self.questionContent = questionContent
        self.questionImage = image
    }
    
    let questionContent: String
    let questionImage: UIImage?
    var body: some View {
        VStack(alignment: .leading) {
            if let questionImage = questionImage {
                Image(uiImage: questionImage)
                    .resizable()
                    .frame(width: 100.0, height: 100.0)
                    .padding()
                
            }
            
            Text(questionContent)
                .font(.custom("Poppins-SemiBold", size: 28))
                .bold()
                .multilineTextAlignment(.leading)
                .frame(maxWidth: .infinity, alignment: .topLeading)
                .padding()
            
        }
    }
}
