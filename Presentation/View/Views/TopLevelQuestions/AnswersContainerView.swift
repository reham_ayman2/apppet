//
//  AnswersContainerView.swift
//  Vet Questionnaire SwiftUI
//
//  Created by NoobMaster69 on 12/03/2022.
//

import SwiftUI

// MARK: Answer Container
struct AnswersContainerView: View {
    
//    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    @ObservedObject var viewModel: QuestionnaireViewModel
    @State   var showingPopover = false
    @State   var currentAnswer: [String] = []
    @State   var currentSecondaryAnswer : [String] = []
    @Binding var questionsFilled: Bool
    @Binding var showImagePicker: Bool
    @Binding var showPickerSource: Bool
    @Binding var source: UIImagePickerController.SourceType
    @Binding var isLoading: Bool
//    @Binding var nav: Bool
    var onQuestionsFilled: () -> () = {}
    var onBackClicked: (_ onClicked: Bool ) -> () = {onClicked in
        if onClicked {
        var click = false
    }
    }
    var body: some View {
        VStack {
            
            ZStack{
                // ???????????????????????
                if viewModel.currentQuestion is TwoAnswerTypesQuestion{
                    TwoAnswerTypesAnswerView(question: viewModel.currentQuestion as! TwoAnswerTypesQuestion, onPrimaryAnswerChange: { answer in
                        currentAnswer = answer
                    }, onSecondaryAnswerChange: { answer in
                        currentSecondaryAnswer = answer
                    })
                }
                else {
                    
                    AnswerView(answers: viewModel.currentQuestion.answers,
                               answerType: viewModel.currentQuestion.answerType,
                               image: viewModel.currentQuestion.image,
                               specie: viewModel.getCurrentSpecie(),
                               currentPetImage: viewModel.currentPetImage,
                               showImagePicker: $showImagePicker,
                               showPickerSource: $showPickerSource,
                               source: $source,
                               onAnswerChange: { answer in
                        currentAnswer = answer
                    }, onImageSubmit: { image in viewModel.setCurrentPetImage(image) })
                }
                
            }
            .padding(.top, 20)
            Spacer()
            HStack {
               
                if viewModel.getCurrentQuestionIdx() > 0{
                    Button(action: {
                        currentAnswer = []
                        viewModel.fetchPrevQuestion()
                    }) {
                        Image("button-left")
                    }
                }else {
             
                    Button(action: {
                        onBackClicked(true)
//                        self.presentationMode.wrappedValue.dismiss()
                    }) {
                        Image("button-left")
                    }
                }
          
                Spacer()
                FormProgressView(totalSteps: StaticQuestions.initialQuestions(petName: "").count ,
                                 currentStepIdx: viewModel.getCurrentQuestionIdx() + 1 )
                Spacer()
                Image("button-right")
                    .onTapGesture {
                        if viewModel.isLastQuestion() && viewModel.allPetsFilled() {
                            questionsFilled = true
                            isLoading = true
                            onQuestionsFilled()
                        }else if viewModel.isLastQuestion(){
                            isLoading = true
                            onQuestionsFilled()
                        }
                        
                        else {
                            if currentAnswer.isEmpty &&
                                viewModel.currentQuestion.answerType != PresentationQuestion.AnswerType.image {
                                showingPopover = true
                            } else {
                                
                                if viewModel.currentQuestion.answerType != PresentationQuestion.AnswerType.image {
                                    viewModel.submitQuestion(answerString: currentAnswer)
                                }
//
                                viewModel.fetchNextQuestion()
                                currentAnswer = []
                            }
                        }
                    }
            }
        }
        .padding(.bottom, 40)
        .padding(.horizontal)
                .alert(isPresented: $showingPopover) {
                    Alert(
                        title: Text("Answer cannot be empty"),
                        message: Text("Please answer the question first")
                    )
                }
                .frame(maxWidth: 400)
                .background(RoundedCorners(color: .white, tl: 30, tr: 30))
                .clipped()
                .shadow(radius: 2)
                .ignoresSafeArea(.container, edges: [.vertical])
    }
    
    
}
