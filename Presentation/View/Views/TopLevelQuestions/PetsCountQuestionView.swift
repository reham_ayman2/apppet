//
//  PetsCountQuestionView.swift
//  Vet Questionnaire SwiftUI
//
//  Created by NoobMaster69 on 22/03/2022.
//

import SwiftUI
import Focuser
import KeyboardToolbars

struct PetsCountQuestionView: View {


    @State var showingPopover = false
    @State var navigating = false
    @StateObject var viewModel : QuestionnaireViewModel
    @State var catsCount = ""
    @State var dogsCount = ""
    
    @FocusStateLegacy var focusedField: String? = ""

    var body: some View {
        NavigationView {
            VStack {
                HStack {
                    Image("AppLogo")
                            .resizable()
                            .scaledToFit()
                            .frame(maxWidth: 115, alignment: .leading)
                            .padding()
                    Spacer()
                }


                QuestionView(content: "How many pets do you have?")
                        .padding(.vertical, 50)

                VStack {
                    Spacer()
                    HStack {

                        VStack {
                            Image("cat")
                                    .resizable()
                                    .scaledToFit()
                                    .frame(width: 120, height: 120)
                                    .foregroundColor(AppColors.secondaryColor)
                                    .font(.largeTitle)
                                    .shadow(radius: 16.0)
                                    .background(Color.white)
                                    .clipShape(RoundedRectangle(cornerRadius: 16))

                            RoundedBorderTextField(title: "Cat(s)", hint: "Num", type: .NUMBER, boundValue: $catsCount, focusedField: _focusedField, isLast: true).frame(maxWidth: 150)


                        }
                                .padding()

                        Spacer()

                        VStack {
                            Image("dog")
                                    .resizable()
                                    .scaledToFit()
                                    .frame(width: 120, height: 120)
                                    .foregroundColor(AppColors.secondaryColor)
                                    .font(.largeTitle)
                                    .shadow(radius: 16.0)
                                    .background(Color.white)
                                    .clipShape(RoundedRectangle(cornerRadius: 16))

                            RoundedBorderTextField(title: "Dogs(s)", hint: "Num", type: .NUMBER, boundValue: $dogsCount, focusedField: _focusedField, isLast: true).frame(maxWidth: 150)


                        }
                                .padding()
                    }
                            .padding()

                    Spacer()
                    HStack {

                        Button(action: {}) {
//                            Image("button-left")
                        }
                        Spacer()
                        Spacer()
                        FormProgressView(totalSteps: 1, currentStepIdx: 1)
                        Spacer()

                        NavigationLink("", destination: QuestionnaireView(isDialog: false).environmentObject(viewModel)
                                       , isActive: $navigating)
                        Image("button-right")
                                .onTapGesture {
                                    if !catsCount.isEmpty || !dogsCount.isEmpty {
                         
                                        viewModel.submitPetsCount(
                                                catCount: Int(catsCount) ?? 0,
                                                dogCount: Int(dogsCount) ?? 0)
                                        
                                        navigating = true
                                    } else {
                                        showingPopover = true
                                    }
                                }
                    }
                            .padding(.bottom, 40)
                            .padding(.horizontal)
                            .alert(isPresented: $showingPopover) {
                                Alert(
                                        title: Text("Answer cannot be empty"),
                                        message: Text("Please answer the question first")
                                )
                            }

                }
                        .background(RoundedCorners(color: .white, tl: 30, tr: 30))
                        .shadow(radius: 2)
            }
                    .ignoresSafeArea(.all, edges: .bottom)
                    .navigationBarBackButtonHidden(true)
                    .navigationBarHidden(true)

        }
                .navigationBarBackButtonHidden(true)
                .navigationBarHidden(true)
                .frame(maxHeight: .infinity, alignment: .bottom)
                .addHideKeyboardButton()


    }
}

struct PetsCountQuestionView_Previews: PreviewProvider {
    static var previews: some View {
       
        PetsCountQuestionView(viewModel: QuestionnaireViewModel())


    }
}
