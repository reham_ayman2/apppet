//
//  PetCounterView.swift
//  Vet Questionnaire SwiftUI
//
//  Created by NoobMaster69 on 31/03/2022.
//

import SwiftUI

struct PetCounterView: View {
    let currentPetCount: Int
    let totalPetCount: Int
    var body: some View {
        HStack {
            Text("\(currentPetCount) / \(totalPetCount)")
            Text("(")
            Image("Icon-awesome-cat")
            Text("/")
            Image("Icon-awesome-dog")
            Text(")")

        }
    }
}

struct PetCounterView_Previews: PreviewProvider {
    static var previews: some View {
        PetCounterView(currentPetCount: 1, totalPetCount: 2)
    }
}
