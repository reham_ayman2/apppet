//
//  QuestionnaireView.swift
//  Vet Questionnaire SwiftUI
//
//  Created by NoobMaster69 on 04/03/2022.
//

import SwiftUI
import HalfModal

struct QuestionnaireView: View {
    
    let isDialog: Bool
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    @EnvironmentObject var viewModel: QuestionnaireViewModel
    @State var questionsFilled: Bool = false
    @State var showPickerSource: Bool = false
    @State var showImagePicker: Bool = false
    @State var source: UIImagePickerController.SourceType = .photoLibrary
    @State var isLoading = false
    @State var destEnabled = false

    var onQuestionsFilled: () -> () = {
    }

    var onQuestionsEndDest: AnyView {
        let onBoardingViewModel = OnBoardingViewModel(onBoardingSteps: OnBoardingSteps.secondOnBoardingStep)
        return AnyView(BottomTextOnBoardingView(viewModel: onBoardingViewModel, onEndDestination: AnyView(GetStartedView())))
    }
    
    var answers: [String] {
        viewModel.getAnswers().map { ans in
            ans.content
        }
    }
    
    init(isDialog: Bool, _ onQuestionsFilled: (() -> ())? = nil ) {
        self.isDialog = isDialog
        self.onQuestionsFilled = onQuestionsFilled ?? {
            print("NonFind")
        }

    }
    
    var sheet: some View {
        VStack {
            if (UIImagePickerController.isSourceTypeAvailable(.camera)) {
                Button(action: {
                    source = .camera
                    showImagePicker = true
                    showPickerSource = false
                    
                }) {
                    Text("Take a photo")
                        .bold()
                        .frame(maxWidth: .infinity, alignment: .center)
                        .foregroundColor(Color.red)
                        .padding([.leading, .top], 4)
                        .font(.custom("Poppins-Bold", size: 21))
                }
                Divider().padding(.horizontal)
            }
            Button(action: {
                source = .photoLibrary
                showImagePicker = true
                showPickerSource = false
                
            }) {
                Text("Choose from library")
                    .bold()
                    .frame(maxWidth: .infinity, alignment: .center)
                    .foregroundColor(Color.black)
                    .padding([.leading, .top], 4)
                    .font(.custom("Poppins-Bold", size: 21))
            }
            
            Divider().padding(.horizontal)
            
            Button(action: {
                showImagePicker = false
                showPickerSource = false
                //                viewModel.setCurrentPetImage(nil)
            }) {
                Text("Remove current Image")
                    .frame(maxWidth: .infinity, alignment: .center)
                    .foregroundColor(Color.black)
                    .padding([.leading, .top], 4)
                    .font(.custom("Poppins-Regular", size: 21))
            }
            
            Button(action: {
                showImagePicker = false
                showPickerSource = false
            }) {
                Text("Cancel")
                    .frame(maxWidth: .infinity, alignment: .center)
                    .foregroundColor(Color.black)
                    .padding([.leading, .top], 4)
                    .font(.custom("Poppins-Regular", size: 21))
            }
            Divider().padding(.horizontal)
            
        }
        .frame(alignment: .topLeading)
    }
    var body: some View {
        
        NavigationView {
            ZStack {
                NavigationLink("", destination: onQuestionsEndDest, isActive: $destEnabled)
                VStack(alignment: .leading) {

                    HStack {
                        Image("AppLogo")
                            .resizable()
                            .scaledToFit()
                            .frame(maxWidth: 115, alignment: .center)
                        Spacer()
           
                            PetCounterView(currentPetCount: viewModel.currentPetCount,
                                       totalPetCount: viewModel.petsCount)
                    }
                    .padding()
                    
                    QuestionView(content: viewModel.currentQuestion.content)
                        .padding(.vertical, 50)
                    
                    AnswersContainerView(viewModel: viewModel, questionsFilled: $questionsFilled, showImagePicker: $showImagePicker, showPickerSource: $showPickerSource, source: $source, isLoading: $isLoading) {
                        viewModel.submitPet { response in
                            
                            if response.isSuccessful() {
                                isLoading = false
                                print(viewModel.currentPetCount)
                                print("Sucessful Add")
                                viewModel.currentPetCount += 1
                                onQuestionsFilled()
                                viewModel.restartQuestionnaire()
                                if viewModel.allPetsFilled() {
                                    destEnabled = true
                                }
                            }else {
                                print(viewModel.currentPetCount)
                                print("Not Sucessful Add")}
                            isLoading = false
                        }
                    } onBackClicked: { onClicked in
                        if onClicked {
                            self.presentationMode.wrappedValue.dismiss()
                        }
                    }

                }
                .overlay(LoadingView(isLoading: $isLoading))
                .frame(maxWidth: 400, alignment: .leading)
                .navigationBarBackButtonHidden(true)
                .navigationBarHidden(true)
                if showPickerSource {
                    HalfModalView(content: AnyView(sheet), header: nil, isPresented: $showPickerSource
                    )
                }
            }
        }
        .navigationBarBackButtonHidden(true)
        .navigationBarHidden(true)
   
        
    }
}

struct QuestionnaireView_Previews: PreviewProvider {
    static var previews: some View {
   
        QuestionnaireView(isDialog: false).environmentObject(QuestionnaireViewModel())
        //        QuestionnaireView(isDialog: false)
    }
}


//struct QuestionView_Previews: PreviewProvider {
//    static var previews: some View {
//        Group {
//            let q = StaticQuestions.initialQuestions(petName: "")[0]
//            QuestionView(content: q.content, image: q.image)
////            QuestionView(
//        }
//    }
//}



