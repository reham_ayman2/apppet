//
//  GetStartedView.swift
//  Vet Questionnaire SwiftUI
//
//  Created by NoobMaster69 on 30/03/2022.
//

import SwiftUI

struct GetStartedView: View {
    @State var navigationEnabled = false
    let dest = CoreAppContainerView()
    var body: some View {
        NavigationView {
            VStack(alignment: .leading) {
                Image(uiImage: UIImage(named: "AppLogo")!).resizable()
                    .scaledToFit()
                    .frame(maxWidth: 150, maxHeight: 150)
                    .padding()
                    .padding(.bottom,20)
                
                Image("ob6").padding()
                
                Text("Now you can track your pet’s activity level.")
                    .bold()
                    .font(.custom("Poppins-Bold",size: 32))
                    .padding()
                
                NavigationLink("", destination: dest,isActive: $navigationEnabled)
                
                Button("Let's Get Started"){
                    navigationEnabled = true
                }.frame(maxWidth: .infinity)
                    .padding(.vertical)
                    .background(AppColors.secondaryColor)
                    .clipShape(RoundedRectangle(cornerRadius: 25))
                    .padding()
                    .foregroundColor(.white)
                
            }.frame(maxHeight:.infinity,alignment:.top)
                .navigationBarBackButtonHidden(true)
                    .navigationBarHidden(true)
            
        }.navigationBarBackButtonHidden(true)
            .navigationBarHidden(true)
            .frame(maxHeight:.infinity,alignment:.top)
    }
    
}


struct GetStartedView_Previews: PreviewProvider {
    static var previews: some View {
        GetStartedView()
    }
}
