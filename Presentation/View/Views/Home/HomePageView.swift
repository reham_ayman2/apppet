//
//  HomePageView.swift
//  Vet Questionnaire SwiftUI
//
//  Created by NoobMaster69 on 06/04/2022.
//

import SwiftUI
import SwiftUICharts

struct HomePageView: View {
    @EnvironmentObject var viewModel: HomePageViewModel
    
    var body: some View {
        ScrollView {
            VStack {
                HeaderView()
                
                HStack {
                    Text("Hello, ")
                        .font(.custom("Poppins-Bold", size: 24))
                        .bold()
                    
                    Text(viewModel.userName)
                        .font(.custom("Poppins-Bold", size: 24))
                        .bold()
                        .foregroundColor(AppColors.primaryColor)
                    Text("!")
                        .font(.custom("Poppins-Bold", size: 24))
                    
                }
                .frame(maxWidth: .infinity, alignment: .leading)
                .padding()
                
                //                Image("cat")
                //                        .scaledToFit()
                //                        .frame(maxHeight: 200)
                //                        .padding()
                
                VStack {
                    Text("Pet Profiles")
                        .bold()
                        .foregroundColor(.black)
                        .frame(maxWidth: .infinity, alignment: .leading)
                        .font(.custom("Poppins-Bold", size: 24))
                        .padding()
                    
                    ScrollView(.horizontal) {
                        HStack {
                            ForEach(viewModel.userPets) { pet in
                                //                               var _ = print("PetsHere \(pets)")
                                NavigationLink(destination: PetProfilePageView().environmentObject(PetProfilePageViewModel(petId: pet.id))) {
                                    PetCardView(pet: pet).padding(4)
                                }.accentColor(.black)
                            }
                        }
                    }
                    HStack {
                        
                        Text("Pet Activities")
                            .bold()
                            .foregroundColor(.black)
                            .font(.custom("Poppins-Bold", size: 24))
                        
                        Spacer()
                        Button("Last week") {
                            
                        }
                        .padding(.horizontal, 20)
                        .padding(.vertical, 10)
                        .background(AppColors.secondaryColor)
                        .clipShape(RoundedRectangle(cornerRadius: 20))
                        .foregroundColor(.white)
                        
                    }
                    .frame(maxWidth: .infinity, alignment: .leading).padding()
                    
                    ScrollView(.horizontal) {
                        HStack {
                            
                            //                            func getUserPets() async{
                            //                        ForEach(viewModel.userPets) { pet in
                            //                            PetChipView(pet: pet)
                            //                                    .opacity(pet.id == viewModel.currentPetIdx ? 1 : 0.5)
                            //                                    .onTapGesture(perform: {
                            //                                        viewModel.currentPetIdx = pet.id
                            //                                    })
                            //                        }
                            //                            }
                            ForEach(viewModel.userPets) { pet in
                                PetChipView(pet: pet)
                                    .opacity(pet.id == viewModel.currentPetIdx ? 1 : 0.5)
                                    .onTapGesture(perform: {
                                        viewModel.currentPetIdx = pet.id
                                    })
                            }
                            
                        }
                    }
                    .padding(.horizontal)
                    
                    
                    VStack {
                 
                      
       
                        BarChartView(data: ChartData(values: viewModel.currentPetCalories), title: "Calories", style: Styles.barChartStyleNeonBlueLight, form: CGSize(width: 300, height: 300), dropShadow: false)
                            .padding(.vertical, 4)
                            .padding(.leading, 4)
                            .padding(.trailing, 2)
                        
                        
                        
                        BarChartView(data: ChartData(values: [("M", 20.0),("T", 20.0),("W",20.0),(viewModel.currentActivity ??  ("no",20.0)),("F", 20.0),("S", 20.0),("S", 20.0)]), title: "Activities", style: Styles.barChartStyleNeonBlueLight, form: CGSize(width: 300, height: 300), dropShadow: false)
                            .padding(.vertical, 4)
                            .padding(.trailing, 4)
                            .padding(.leading, 2)
                    }
                    HStack {
                        DataChipView(imageName: "noun-dog-food", title: "Calories Avg", subTitle: viewModel.currentPetAverageCalories)
                            .padding(.trailing, 4)
                        
                        DataChipView(imageName: "Icon-tennisball", title: "Activities Avg", subTitle: viewModel.currentPetAverageActivityTime)
                            .padding(.leading, 4)
                    }.padding(4)
                    
                }
                .padding()
                .frame(minWidth: 0.0, maxWidth: .infinity, minHeight: 0.0,
                       maxHeight: .infinity)
                .background(RoundedCorners(color: AppColors.lightBlue, tl: 24, tr: 24, bl: 0, br: 0))
                
                
            }
            .frame(maxWidth: .infinity, maxHeight: .infinity, alignment: .top)
            
        }
        .onAppear(perform: {
            viewModel.fetchPetData()
        })
        .navigationBarBackButtonHidden(true)
        .navigationBarHidden(true)
        .frame(maxWidth: .infinity, alignment: .top)
        
    }
}

struct HomePageView_Previews: PreviewProvider {
    static var previews: some View {
        HomePageView().environmentObject(HomePageViewModel())
    }
}
