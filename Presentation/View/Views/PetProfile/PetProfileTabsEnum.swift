//
//  PetProfileTabsEnum.swift
//  Vet Questionnaire SwiftUI
//
//  Created by NoobMaster69 on 04/05/2022.
//

import Foundation

enum PetProfileTabsEnum : String {
   
   case health = "Health"
   case overview = "Overview"
    
}
