//
//  PetProfileTabView.swift
//  Vet Questionnaire SwiftUI
//
//  Created by NoobMaster69 on 04/05/2022.
//

import SwiftUI

struct PetProfileTabView: View {
    @Binding var selectedTab : String
    var body: some View {
        HStack{
            Text(PetProfileTabsEnum.overview.rawValue)
                .padding(10)
                .foregroundColor(.white)
                .padding(.trailing,selectedTab == PetProfileTabsEnum.overview.rawValue ? 0 : 20)
                .padding(.horizontal,50)
                .background(AppColors.secondaryColor)
                .clipShape(RoundedRectangle(cornerRadius: 24))
                .opacity(selectedTab == PetProfileTabsEnum.overview.rawValue ? 1 : 0.5)
                .onTapGesture {
                    withAnimation{
                        selectedTab = PetProfileTabsEnum.overview.rawValue
                    }
                }

            
            Text(PetProfileTabsEnum.health.rawValue)
                .padding(10)
                .foregroundColor(.white)
                .padding(.leading,selectedTab == PetProfileTabsEnum.health.rawValue ? 0 : 20)
                .padding(.horizontal,50)
                .background(AppColors.secondaryColor)
                .clipShape(RoundedRectangle(cornerRadius: 24))
                .offset(x: -50, y: 0)
                .opacity(selectedTab == PetProfileTabsEnum.health.rawValue ? 1 : 0.5)
                .onTapGesture {
                    withAnimation{
                        selectedTab = PetProfileTabsEnum.health.rawValue
                    }
                }

        }.offset(x: 25, y: 0)
    }
}

struct PetProfileTabView_Previews: PreviewProvider {
    @State static var selectedTab = "Overview"
    static var previews: some View {
        PetProfileTabView(selectedTab: $selectedTab)
    }
}
