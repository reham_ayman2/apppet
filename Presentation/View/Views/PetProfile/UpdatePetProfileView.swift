//
//  UpdatePetProfileView.swift
//  Vet Questionnaire SwiftUI
//
//  Created by NoobMaster69 on 06/05/2022.
//

import SwiftUI
import Focuser

struct UpdatePetProfileView: View {

    @EnvironmentObject var viewModel: UpdatePetProfileViewModel
    @FocusStateLegacy var petNameFocused: String?
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>

    @State var petName = ""
    @State var petYears = ""
    @State var petMonths = ""
    @State var petBreed = ""
    @State var petWeight = ""
    @State var petGender = ""


    var body: some View {
        ScrollView {
            VStack {
                ZStack {
                    let _ = print("View Model = \(viewModel)")
                    if let petImage = viewModel.pet.image {
                        Image(uiImage: petImage)
                                .resizable()
                                .scaledToFit()

                    } else {
                        Image(viewModel.pet.gender?.lowercased() == "cat" ? "car" : "dog")
                                .resizable()
                                .scaledToFit()
                    }
                }
                        .frame(maxWidth: .infinity, maxHeight: 110)
                        .padding()
                        .clipShape(Circle())
                        .ignoresSafeArea()


                Text("Change pet profile picture")
                        .font(.custom("Poppins-Regular", size: 16))
                        .foregroundColor(.blue)

                VStack {
                    RoundedBorderTextField(title: "Pet Name", hint: "Eg Tom", boundValue: $petName, focusedField: _petNameFocused)
                            .padding(.top)

                    HStack {
                        RoundedBorderTextField(title: "Age", hint: "Years", type: .NUMBER, boundValue: $petYears, focusedField: _petNameFocused)
                                .padding(.trailing, 2)
                        RoundedBorderTextField(title: " ", hint: "Months", type: .NUMBER, boundValue: $petMonths, focusedField: _petNameFocused)
                                .padding(.leading, 2)
                    }
                            .padding(.top)

                    RoundedBorderTextField(title: "Pet Breed", hint: "Eg Presian", boundValue: $petBreed, focusedField: _petNameFocused)
                            .padding(.top)

                    RoundedBorderTextField(title: "Pet Weight (Pounds)", hint: "Eg 50", type: .NUMBER, boundValue: $petWeight, focusedField: _petNameFocused)
                            .padding(.top)

                    RoundedBorderTextField(title: "Pet Gender", hint: "Eg Male", type: .NUMBER, boundValue: $petGender, focusedField: _petNameFocused, isLast: true)
                            .padding(.vertical)

                    Button("Save") {
                        let newPet = PresentationPet.Builder()
                            .setId(viewModel.pet.id)
                            .setBreedId(viewModel.pet.breedId ?? 1)
                            .setUserId(viewModel.pet.userID ?? 1)
                            .setSpecies(viewModel.pet.species ?? "cow")
                            .setSpayed(false)
                            .setImage(viewModel.pet.image)
                            .setMixed(false)
                            .setName(petName)
                                .setAge(age: " \(viewModel.pet.birthDate!),\(petYears),\(petMonths)")
                                .setBreed(petBreed)
                                .setWeight(Int(petWeight) ?? 0)
                                .setGender(petGender)
                                .build()
                        let _ = print("newPet Info = \(newPet)")
                        viewModel.updatePet(newPet) { apiResponse in
                            
                            DispatchQueue.main.sync {
                                print("ApiUpdateRespons = \(apiResponse)")
                                presentationMode.wrappedValue.dismiss()
                            }
                            
                        }
                    }
                            .frame(maxWidth: .infinity)
                            .foregroundColor(.white)
                            .padding(.vertical)
                            .background(AppColors.secondaryColor)
                            .clipShape(RoundedRectangle(cornerRadius: 16))


                }
                        .padding()


            }
                    .navigationTitle("Edit Pet’s Profile")
        }
                .onAppear {
                    petName = viewModel.pet.name ?? ""
                    petYears = "\(viewModel.pet.years ?? 0)"
                    petMonths = "\(viewModel.pet.months ?? 0)"
                    petBreed = viewModel.pet.breed ?? ""
                    petWeight = "\(viewModel.pet.weight ?? 0)"
                    petGender = viewModel.pet.gender ?? ""
                }
    }
}

//struct UpdatePetProfileView_Previews: PreviewProvider {
//    static var previews: some View {
//        UpdatePetProfileView().environmentObject(UpdatePetProfileViewModel(petId: 1))
//    }
//}
