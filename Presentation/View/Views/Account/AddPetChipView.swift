//
//  AddPetChipView.swift
//  Vet Questionnaire SwiftUI
//
//  Created by NoobMaster69 on 11/04/2022.
//

import SwiftUI

struct AddPetChipView: View {
    
    var body: some View {
        VStack{
            
            Image(systemName: "plus.circle.fill")
                    .resizable()
                    .scaledToFit()
                    .frame(minWidth:60, maxWidth:60, minHeight: 60,maxHeight: 60)
                    .foregroundColor(AppColors.secondaryColor)

            Text("Add pet")
                .font(.custom("Poppins-Bold", size: 18))
                .bold()
        }

    }

}

struct AddPetChipView_Previews: PreviewProvider {
    static var previews: some View {
        AddPetChipView()
    }
}
