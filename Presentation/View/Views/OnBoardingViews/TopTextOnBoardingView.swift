//
//  TopTextOnBoardingView.swift
//  Vet Questionnaire SwiftUI
//
//  Created by NoobMaster69 on 15/03/2022.
//

import SwiftUI

struct TopTextOnBoardingView: View {
    @ObservedObject var viewModel: OnBoardingViewModel
//    let questionnaireViewModel = QuestionnaireViewModel()
    let onEndDestination: AnyView
    
    var isNavigationDone: Bool {
        viewModel.stepsCount == viewModel.currentStepIdx + 1
    }
    
    var body: some View {
        
        NavigationView {
            VStack(alignment: .leading) {
                HStack {
                    Image(uiImage: UIImage(named: "AppLogo")!).resizable()
                        .scaledToFit()
                        .frame(maxWidth: 115)
                    Spacer()
                    NavigationLink {
                        onEndDestination
                    } label: {
                        Text("Skip").foregroundColor(.blue).padding()
                    }
                    
                }
                .padding(.bottom)
                Text(viewModel.currentStep.content)
                    .bold()
                    .font(.custom("Poppins-SemiBold",size: 28))
                    .padding(.vertical)
                Image(uiImage: viewModel.currentStep.image)
                    .padding(.bottom)
                Spacer()
                HStack(alignment: .center) {
                    FormProgressView(totalSteps: viewModel.stepsCount, currentStepIdx: viewModel.currentStepIdx + 1)
                    Spacer()
                    if isNavigationDone {
                        NavigationLink(destination: { onEndDestination }
                                       , label: {
                            Image("button-right")
                        })
                    } else {
                        Image("button-right")
                            .background(Color.white)
                            .onTapGesture {
                                withAnimation {
                                    viewModel.fetchNextOnBoardingStep()
                                }
                            }
                            .transition(.slide)
                            .animation(.linear)
                    }
                    
                }.padding(.vertical)
                    .padding(.leading)
            }.padding()
                .navigationBarHidden(true)
        }
    }
}

struct OnBoardingView_Previews: PreviewProvider {
    static var previews: some View {
        let viewModel = OnBoardingViewModel(onBoardingSteps: OnBoardingSteps.firstOnBoardingSteps)
        //        let dest = QuestionnaireView(viewModel:questionnaireViewModel)
        let dest = EntryAuthenticationView()
        TopTextOnBoardingView(viewModel: viewModel, onEndDestination: AnyView(dest))
    }
}
