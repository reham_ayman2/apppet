//
//  ContentView.swift
//  Vet Questionnaire SwiftUI
//
//  Created by NoobMaster69 on 04/03/2022.
//

import SwiftUI

struct MainView: View {
    var body: some View {
        ZStack{
            let defaults = UserDefaults.standard
            if defaults.string(forKey: UserConstants.authToken.rawValue) != nil {
                CoreAppContainerView()
            }
            else {
                let viewModel = OnBoardingViewModel(onBoardingSteps: OnBoardingSteps.firstOnBoardingSteps)
                let dest = EntryAuthenticationView()
                TopTextOnBoardingView(viewModel: viewModel, onEndDestination: AnyView(dest))
            }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        MainView()
    }
}
