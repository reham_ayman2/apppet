//
//  QuestinaryServices.swift
//  Vet Questionnaire SwiftUI
//
//  Created by Oufaa on 08/06/2022.
//

import Foundation
import Combine
enum CustomError: Error {
    case noData
}

class QuestionaryServices {
   static  func submitPet(pets: PresentationPet) -> AnyPublisher<APIResponse<SubmitPetResponse>, CustomError> {
        Deferred {
            Future { promise in
                PetAPIRequests.submitPet(pets) { response in
                    if let _  = response.data {
                        promise(.success(response))
                    }else {
                        promise (.failure(CustomError.noData))
                    }
                
                }
            }
        }
        .eraseToAnyPublisher()
//        PetAPIRequests.submitPet(pets) { response in
//            onPetSubmit(response)
//            DispatchQueue.main.async {
//                self.currentPetBuilder = PresentationPet.Builder()
//                self.currentPetImage = nil
//                self.restartQuestionnaire()
//            }
//        }

    }
}
