//
//  TwoAnswerTypesQuestion.swift
//  Vet Questionnaire SwiftUI
//
//  Created by NoobMaster69 on 06/05/2022.
//

import Foundation

class TwoAnswerTypesQuestion : PresentationQuestion {
    var primaryType,secondaryType : PresentationQuestion.AnswerType
    var primaryAnswers,secondaryAnswers : [PresentationAnswer]
    var primaryTitle,secondaryTitle : String

    
    init(primaryTitle : String,
         secondaryTitle : String,
         primaryAnswers:[PresentationAnswer],
         secondaryAnswers : [PresentationAnswer],
         primaryType:PresentationQuestion.AnswerType,
         secondaryType: PresentationQuestion.AnswerType) {

         
        self.primaryTitle = primaryTitle
        self.secondaryTitle = secondaryTitle
        self.primaryAnswers = primaryAnswers
        self.secondaryAnswers = secondaryAnswers
        self.primaryType = primaryType
        self.secondaryType = secondaryType
        
        super.init(content: primaryTitle, answerType: primaryType,answers: primaryAnswers){builder , pet in
             return builder
         }
        
    }
}
