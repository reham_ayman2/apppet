//
//  PresentationActivity.swift
//  Vet Questionnaire SwiftUI
//
//  Created by NoobMaster69 on 21/04/2022.
//

import Foundation
struct PresentationActivity : Identifiable{
    
    let id : Int
    let type : ActivityType
    let title : String
    let subtitle : String
    let createdAt : Date
    
    enum ActivityType : String{
        case food = "Food" , exercise = "Exercise"
    }
}
