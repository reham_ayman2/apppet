//
//  User.swift
//  Vet Questionnaire SwiftUI
//
//  Created by NoobMaster69 on 28/03/2022.
//

import Foundation
class PresentationUser {
    private init(){}
    var email:String? = nil
    var petsCount : Int = 1
    var pets : [PresentationPet] = []
    
    func addPet(pet:PresentationPet){ pets.append(pet) }
    
    class Builder{
        let user : PresentationUser = PresentationUser()
        
        func setEmail(_ email:String)-> Builder{
            user.email = email
            return self
        }
        func setPetsCount(_ petsCount :Int) -> Builder{
            user.petsCount = petsCount
            return self
        }
        func setPets(_ pets:[PresentationPet])-> Builder{
            user.pets = pets
            return self
        }
        func build()-> PresentationUser {
            return user
        }
    }
}
