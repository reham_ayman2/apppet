//
//  PresentationPet.swift
//  Vet Questionnaire SwiftUI
//
//  Created by NoobMaster69 on 28/03/2022.
//

import Foundation
import SwiftUI

struct PresentationPet: Identifiable, Hashable {
    private init() {
    }

    var id: Int = 1
    private(set) var name: String? = nil
    private(set) var species: String? = nil
    private(set) var breed: String? = nil
    private(set) var secondaryBreed: String? = nil
    private(set) var isMixed: Bool? = nil
    private(set) var birthDate: String? = nil
    private(set) var years: Int? = nil
    private(set) var months: Int? = nil
    private(set) var weight: Int? = nil
    private(set) var gender: String? = nil
    private(set) var breedId: Int? = nil
    private(set) var userID: Int? = nil
    private(set) var isSpayed: Bool? = nil
    private(set) var bodyScore: Int? = nil
    private(set) var mealsPerDay: Int? = nil
    private(set) var image: UIImage? = nil

    static func fromAPIPet(_ apiPet: APIPet) -> PresentationPet {
        return PresentationPet.Builder()
                .setId(apiPet.id)
                .setSpecies(apiPet.specie)
                .setName(apiPet.petName)
                .setBreed(apiPet.breedOne)
                .setGender(apiPet.gender)
                .setSpayed(apiPet.spayedOr == 1 )
                .setWeight(apiPet.weight)
                .setBreedId(apiPet.breedID)
                .setUserId(apiPet.userID)
                .setAge(age: "\(apiPet.birthday),\(apiPet.petYears),\(apiPet.petMonths)")
                .setMixed(apiPet.mixed == 1)
                .build()
        
    }

    class Builder {
        private var pet = PresentationPet()

        func setId(_ id: Int) -> Builder {
            pet.id = id
            return self
        }

        func setName(_ name: String) -> Builder {
            pet.name = name
            return self
        }

        func setSpecies(_ species: String) -> Builder {
            pet.species = species
            return self
        }

        func setBreed(_ breed: String) -> Builder {
            pet.breed = breed
            return self
        }
        func setBreedId(_ breedId: Int) -> Builder {
            pet.breedId = breedId
            return self
        }
        func setUserId(_ userId: Int) -> Builder {
            pet.userID = userId
            return self
        }
        func setWeight(_ weight: Int) -> Builder {
            pet.weight = weight
            return self
        }

        func setGender(_ gender: String) -> Builder {
            pet.gender = gender
            return self
        }

        func setSpayed(_ isSpayed: Bool) -> Builder {
            pet.isSpayed = isSpayed
            return self
        }
        
        func setMixed(_ isMixed: Bool) -> Builder {
            pet.isMixed = isMixed
            return self
        }


        func setBodyScore(_ bodyScore: Int) -> Builder {
            pet.bodyScore = bodyScore
            return self
        }

        func setMealsPerDay(_ mealsCount: Int) -> Builder {
            pet.mealsPerDay = mealsCount
            return self
        }

        func setImage(_ image: UIImage?) -> Builder {
            pet.image = image
            return self
        }

        func setAge(age: String) -> Builder {
            print("Age is = \(age)")
            let split = age.split(separator: ",")
            pet.birthDate = String(split[0])
            pet.years = Int(split[1].trimmingCharacters(in: .whitespaces)) ?? 0
            pet.months = Int(split[2].trimmingCharacters(in: .whitespaces)) ?? 0
            return self
        }

        func build() -> PresentationPet {
//            print("build Activated   pet ID = \(pet.id) , Pet Name = \(String(describing: pet.name) )  , Pet  gender \(pet.gender)")
           return pet
        }
    }


}
