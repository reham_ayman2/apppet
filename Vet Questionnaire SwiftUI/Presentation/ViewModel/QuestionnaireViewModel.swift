//
//  QuestionnaireViewModel.swift
//  Vet Questionnaire SwiftUI
//
//  Created by NoobMaster69 on 04/03/2022.
//

import Foundation
import SwiftUI
import Combine




class QuestionnaireViewModel: ObservableObject {
    
    init() {
        print("View Model Is init Created")
    }
    init(questionnaire : PresentationQuestionnaire){
        self.questionnaire = questionnaire
        print("View Model Is Created Questionnaire")
    }
    
    init(petsCount: Int) {
        self.petsCount = petsCount
        print("View Model Is Created petCounts")
    }
    
    
    
    
    @Published var petName: String = ""
    
    
    
    
   //added for med 3 arrayes
    @Published var Fooditems : [[String]] = []
    @Published var CurrentMedicens : [[String]] = []
    @Published var  conditions  : [[String]] = []
  
    @Published private var currentQuestionIdx = 0
    @Published var currentPetImage: UIImage? = nil
    @Published private var questionnaire = PresentationQuestionnaire(questionsList: StaticQuestions.initialQuestions(petName: ""))
    
    @Published private var currentPetBuilder = PresentationPet.Builder()
    @Published private var currentUserBuilder = PresentationUser.Builder()
    @Published var petsCount: Int = 1
    @Published var currentPetCount: Int = 1
    @Published var catsCount: Int = 0
    @Published var dogsCount: Int = 0
    @Published var currentCatCount: Int = 0
    @Published var currentDogCount: Int = 0
    
    private(set) var breed: String? = nil
    private(set) var breedTwo: String? = nil
    private(set) var IsMixed : Bool? = nil
   
    
    
    private var cancellable = Set<AnyCancellable>()
    
    var currentQuestion: PresentationQuestion {
        print(currentQuestionIdx)
        print(questionnaire.getQuestions().count)
        print("returnet question \(currentQuestionIdx % questionnaire.getQuestions().count)")
        return questionnaire.getQuestions()[currentQuestionIdx % questionnaire.getQuestions().count]
    }
    
    func fetchPrevQuestion() {
        currentQuestionIdx = currentQuestionIdx < 1 ? 0 : currentQuestionIdx - 1
        debugPrint("🟢 current question is : \(currentQuestionIdx)")
        //        currentQuestionIdx -= 1
    }
    
    func setCurrentPetImage(_ image: UIImage?) {
        currentPetBuilder = currentPetBuilder.setImage(image)
        currentPetImage = image
    }
    
    
    
    // 3 arrayes
    func setCurrentPetMedication (_ med : [[String]]? ) {
        print("🔴 🔴 setCurrentPetMedication 🔴 🔴 ")

        currentPetBuilder = currentPetBuilder.setMedicens(med)
        CurrentMedicens = med ?? []
        
    }
    
    
    func setCurrentPetBilderFoods ( _ foods : [[String]]?) {
        
        print("🔴 🔴 setCurrentPetBilderFoods 🔴 🔴 ")
        currentPetBuilder = currentPetBuilder.setFood(foods: foods)
      
        
    }
    
    
    func SetcurrentPetBuilderCond (conds : [[String]]? ) {
        print("🔴 🔴 SetcurrentPetBuilderCond  🔴 🔴 ")
        currentPetBuilder = currentPetBuilder.setCond(cond: conds)

        
    }
    
    func fetchNextQuestion() {
        currentQuestionIdx += 1
    }
    
    //    func addQuestionAndAnswer(question: PresentationQuestion, answers: [PresentationAnswer]) {
    //        if currentQuestionIdx < getQuestionsCount() {
    //            questionnaire.addQuestionAndAnswers(question: question, answers: answers)
    //        }
    //    }
    
    func popUpQuestion() {
        questionnaire.popQuestion()
    }
    
    func getCurrentQuestionIdx() -> Int {
        currentQuestionIdx
    }
    
    func getQuestionsCount() -> Int {
        return questionnaire.getQuestions().count
    }
    
    func startQuestionnaire() {
        currentQuestionIdx = 1
    }
    
    func getAnswers() -> [PresentationAnswer] {
        
       // print("👋🏻👋🏻👋🏻👋🏻👋🏻👋🏻trigger get answer ")
        
       
        questionnaire.getAnswers()
        
        
    }
    
    func restartQuestionnaire() {
        DispatchQueue.main.async {
            self.currentQuestionIdx = 0
            self.questionnaire = PresentationQuestionnaire(questionsList: StaticQuestions.initialQuestions(petName: ""))
        }
    }
    
    func submitQuestion(answerString: [String]) {
        if (currentQuestion.answerType == .text) {
            
        
            questionnaire.addQuestionAndAnswers(question: currentQuestion, answers: answerString.map(PresentationAnswer.fromString))
            
            
        } else {
            let answers: [PresentationAnswer] = currentQuestion.answers.filter { answer in
                answerString.contains(answer.content)
            }
            questionnaire.addQuestionAndAnswers(question: currentQuestion, answers: answers)
        }
        if answerString.count > 1 {
            
            currentPetBuilder = currentQuestion.updatePetBuilder(currentPetBuilder, answerString)
        }else {
        currentPetBuilder = currentQuestion.updatePetBuilder(currentPetBuilder, answerString[0])
        }
    }
    
    func submitPetsCount(catCount: Int, dogCount: Int) {
        catsCount = catCount
        dogsCount = dogCount
        if catCount > 0  {
            currentCatCount = 1
            currentDogCount = 0
        }else {
            currentDogCount = 1
        }
        petsCount = catsCount + dogsCount
        currentUserBuilder = currentUserBuilder.setPetsCount(petsCount)
    }
    
    func isLastQuestion() -> Bool {
        return currentQuestionIdx == questionnaire.getQuestions().count - 1 && currentQuestionIdx > 0
        
        
        
        
        
    }
    
    func allPetsFilled() -> Bool {
        petsCount+1 == currentPetCount
    }
    
    //    func submitPet(onPetSubmit: @escaping (APIResponse<SubmitPetResponse>) -> ()) {
    //        print(currentPetBuilder.build())
    //        QuestionaryServices.submitPet(pets: currentPetBuilder.build())
    //            .receive(on: RunLoop.main)
    //            .subscribe(on: RunLoop.main)
    //            .sink { comp in
    //
    //                debugPrint("Errors Is = \(comp)")
    //            } receiveValue: { [weak self] result in
    //                onPetSubmit(result)
    //                guard let self = self else {return}
    //                self.currentPetBuilder = PresentationPet.Builder()
    //                self.currentPetImage = nil
    //                self.restartQuestionnaire()
    //            }.store(in: &cancellable)
    //
    //        PetAPIRequests.submitPet(currentPetBuilder.build()) { response in
    //            onPetSubmit(response)
    //            DispatchQueue.main.async {
    //     self.currentPetBuilder = PresentationPet.Builder()
    //    self.currentPetImage = nil
    //    self.restartQuestionnaire()
    //            }
    //        }
    //
    //    }
    //
    
    
    
    
   
    
    
    
    
    func submitPet(onPetSubmit: @escaping (APIResponse<SubmitPetResponse> ) -> ()) {
        
        
     
        
        
        
        print("my next step with api ")
        print("👀👀👀👀👀create pet  builder 👀")
        print(currentPetBuilder.build())
        print(currentPetBuilder.build().birthDate )
      
        let requestURL = URL(string: APIService.BASE_URL + "CreatePet")
        // --------
        
        
    
        
        
        var breed1 :String
        var breed2  :String
        var isMixed :Int
        
        
        if currentPetBuilder.build().breed != "" {
        let split = currentPetBuilder.build().breed!.split(separator: ",")
   
        breed1 = String(split[0])
        breed2 = String(split[1].trimmingCharacters(in: .whitespaces))
        isMixed =  String(split[2]) == "true" ? 1 : 0
        if isMixed == 1 {
           breed = breed1
            breedTwo = breed2
            IsMixed = true
         
        }else{
          breed = breed1
            
            IsMixed = false
        }
    
  
     
        // how to upload image
           
            
           
        
        
        
        
        // --------
        
        
        print("⚠️⚠️⚠️⚠️⚠️⚠️⚠️   check create pet paramters  ⚠️⚠️⚠️⚠️⚠️⚠️⚠️ ")
        let params: [String: Any] = ["PetName": currentPetBuilder.build().PetName ?? ""   ,
                                     "specie": currentPetBuilder.build().species?.lowercased() ?? "",
                                     // birthday make sure to formate
                                     
                                     "birthday": currentPetBuilder.build().birthDate ?? "",
                                     "PetYears": currentPetBuilder.build().years ?? 0,
                                     "PetMonths": currentPetBuilder.build().months ?? 0,
                                     "weight": currentPetBuilder.build().weight ?? 0,
                                     "gender": currentPetBuilder.build().gender?.lowercased() ?? "male",
                                     "spayedOr": currentPetBuilder.build().isSpayed ?? false,
                                     "mixed": IsMixed ?? "" ,
                                     "breedOne": breed ?? ""  ,
                                        //currentPetBuilder.build().breed ?? "",
                                     "image_path":   currentPetBuilder.build().image?.jpegData(compressionQuality: 0.8) ?? "",
                                        
                                        //uploadImge ,
                                      
                                     
                                     // added
                                     "breedTwo": breedTwo ?? "" ,
                                        //currentPetBuilder.build().breedTwo ?? "" ,
                                     "BodyConditionScore":currentPetBuilder.build().bodyScore ?? 0 ,
                                     "worklevel" : currentPetBuilder.build().work ?? "" ,
                                     "watchingweight" : currentPetBuilder.build().watchingweight ?? false ,
                                     "losingweight" : currentPetBuilder.build().losingweigh ?? false ,
       
                                     // 3 arrayes added "food" "medications"  "conditions"
                                     
                                     "food" : Fooditems ,
                                     "medications" :  CurrentMedicens ,
                                     "conditions" : conditions
                                     
                                     
                                     
                                     
                                     
        
        
       ]
      
        print(params)
        
        
        

        if let requestURL = requestURL {
//            APIService.uploadImage(urlString: APIService.BASE_URL + "CreatePet", paramName: "image_path", fileName: "sss", image: image)
            APIService.callPost(url: requestURL, params: params, finish: onPetSubmit)
        }
        
        
        
       
   
//        PetAPIRequests.submitPet(currentPetBuilder.build()) { response in
//            onPetSubmit(response)
//
//            DispatchQueue.main.async {
//
//                if response.isSuccessful(){
//                    UserDefaults.standard.set(true, forKey: defualtCompleteQuestion)
//
//
//                    self.currentPetBuilder = PresentationPet.Builder()
//                    self.currentPetImage = nil
//                    self.Fooditems.removeAll()
//
//                    self.restartQuestionnaire()
//
//
//
//                }else {
//                   }
//            }
//        }
            
            
        }
        
    }
    
    func getCurrentSpecie() -> String {
        (currentPetBuilder.build().species ?? "").lowercased()
    }
    
    func getNextPet (){
        
        if currentCatCount < catsCount {
            currentCatCount += 1
        }else if currentDogCount < dogsCount && currentCatCount == catsCount {
            currentDogCount += 1
        }else {
            petsCount  = currentPetCount - 1
        }
    }
    
    
    
    
//    func submitPet(_ pet: PresentationPet  , _ onPetSubmit: @escaping (APIResponse<SubmitPetResponse>) -> ()) {
//        let requestURL = URL(string: APIService.BASE_URL + "CreatePet")
//        let params: [String: Any] = ["PetName": pet.PetName ?? "",
//                                     "specie": pet.species?.lowercased() ?? "",
//                                     // birthday make sure to formate
//
//                                     "birthday": pet.birthDate ?? "",
//                                     "PetYears": pet.years ?? 0,
//                                     "PetMonths": pet.months ?? 0,
//                                     "weight": pet.weight ?? 0,
//                                     "gender": pet.gender?.lowercased() ?? "male",
//                                     "spayedOr": pet.isSpayed ?? false,
//                                     "mixed": pet.isMixed ?? false,
//                                     "breedOne": pet.breed ?? "",
//                                     "image_path": pet.image?.jpegData(compressionQuality: 0.8) ?? "",
//
//                                     // added
//                                     "breedTwo":pet.breedTwo ?? "" ,
//                                     "BodyConditionScore":pet.bodyScore ?? 0 ,
//                                     "worklevel" : pet.work ?? "" ,
//                                     "watchingweight" : pet.watchingweight ?? false ,
//                                     "losingweight" : pet.losingweigh ?? false ,
//
//                                     // 3 arrayes added "food" "medications"  "conditions"
//
//                                     "food" : pet.food ?? [] ,
//                                     "medications" : pet.medications ?? [] ,
//                                     "conditions" : pet.conditions ?? []
//
//
//
//
//
//
//
//       ]
//
//        if let requestURL = requestURL {
////            APIService.uploadImage(urlString: APIService.BASE_URL + "CreatePet", paramName: "image_path", fileName: "sss", image: image)
//            APIService.callPost(url: requestURL, params: params, finish: onPetSubmit)
//        }
//
//
//
//
//}
}
