//
// Created by NoobMaster69 on 04/05/2022.
//

import Foundation

class PetProfilePageViewModel: ObservableObject {
    init(petId: Int) {
        PetAPIRequests.getUserPets { apiResponse in
            if let data = apiResponse.data {
                let currentPet = data.createdPets.first(where: { $0.id == petId })
                if let apiPet = currentPet {
                    DispatchQueue.main.sync {
                        
                        self.displayedPet = PresentationPet.fromAPIPet(apiPet)
                        print("DisplayedPet = \(self.displayedPet)")
                    }
                }
            }
        }
    }

    @Published var displayedPet: PresentationPet = PresentationPet.Builder().build()
    @Published var currentPetActivityInsights: String = "20% Higher for Today"
    @Published var currentPetFoodInsights: String = "Extra 1/2 cup"

}
