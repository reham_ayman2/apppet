//
//  EntriesViewModel.swift
//  Vet Questionnaire SwiftUI
//
//  Created by NoobMaster69 on 19/04/2022.
//

import Foundation

class EntriesViewModel: ObservableObject {
    
    @Published var userPets: [PresentationPet] = []
    
    @Published var currentPetId: Int = 0 {
        didSet {
            fetchExerciseActivities()
            fetchFoodActivity()
        }
    }
    
    
    @Published var currentDate: String {
        didSet {
            fetchExerciseActivities()
            fetchFoodActivity()
        }
    }
    
    @Published var currentPetActivities: [PresentationActivity] = []
    
    var dates: [String] {
        let startDate = Calendar.current.date(byAdding: .day, value: -365, to: Date())!
        let endDate = Calendar.current.date(byAdding: .day, value: 365, to: Date())!
        let dayDurationInSeconds: TimeInterval = 60 * 60 * 24
        let formatter = DateFormatter()
        formatter.dateFormat = "dd-MM-yyyy"
        return stride(from: startDate, to: endDate, by: dayDurationInSeconds).map { date in
            formatter.string(from: date)
        }
    }
    
    init() {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd-MM-yyyy"
        currentDate = formatter.string(from: Date())
        fetchUserPets()
        
    }
    
    func fetchUserPets() {
        
        PetAPIRequests.getUserPets { response in
            DispatchQueue.main.async {
                self.userPets = response.data?.createdPets.map { apiPet in
                    return PresentationPet.fromAPIPet(apiPet)
                }
                .uniqued() ?? []
                
                self.currentPetId = self.userPets.first?.id ?? 0
            }
            self.fetchExerciseActivities()
        }
    }
    
    func fetchFoodActivity(){
        PetAPIRequests.getPetFood(petId: self.currentPetId) { apiResponse in
            if apiResponse.isSuccessful() {
                
                DispatchQueue.main.async {
                    
                    let petFood: [PresentationActivity] = apiResponse.data?.foodActivity.map { food in
                        let dateFormatter = DateFormatter()
                        dateFormatter.locale = Locale(identifier: "en_US_POSIX") // set locale to reliable US_POSIX
                        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                        return PresentationActivity(id: food.foodActivityID,
                                                    type: .food,
                                                    title: food.products ?? "",
                                                    subtitle: food.ingredients ?? "",
                                                    createdAt: dateFormatter.date(from: food.createdAt ?? "")!
                        )
                    }
                        .sorted(by: { $0.createdAt < $1.createdAt }) ?? []
                    self.currentPetActivities.append(contentsOf: petFood)
                    self.currentPetActivities = self.currentPetActivities.sorted(by: { $0.createdAt > $1.createdAt })
                }
                
            }
        }
    }
    
    func fetchExerciseActivities() {
        PetAPIRequests.getPetActivities(petId: self.currentPetId) { response in
            if response.isSuccessful() {
                DispatchQueue.main.async {
                    self.currentPetActivities = response.data?.petExercises.map { exercise in
                        print(exercise.exerciseName)
                        let dateFormatter = DateFormatter()
                        dateFormatter.locale = Locale(identifier: "en_US_POSIX") // set locale to reliable US_POSIX
                        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.000000Z"
                        return PresentationActivity(id: exercise.id,
                                                    type: .exercise,
                                                    title: exercise.exerciseName,
                                                    subtitle: "\(exercise.hours)H \(exercise.mins)M",
                                                    createdAt: dateFormatter.date(from: exercise.createdAt) ?? Date()
                        )
                    } ?? []
                }
            }
        }
    }
    
    func addExerciseActivity( _ exercise: String,  _ hours: String, _ mins: String) {
        
            PetAPIRequests.submitPetExercise(currentPetId, exercise, Int(hours) ?? 0, Int(mins) ?? 0) { response in
                self.fetchExerciseActivities()
            }
    }
    func addFoodActivity(_ food: String, _ calories: String){
        PetAPIRequests.submitPetFood(currentPetId, calories) { response in
            self.fetchExerciseActivities()
        }
    }
}


