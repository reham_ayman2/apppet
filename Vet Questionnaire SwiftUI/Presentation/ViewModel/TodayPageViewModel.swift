//
// Created by NoobMaster69 on 03/05/2022.
//

import Foundation

class TodayPageViewModel: ObservableObject {

    @Published var currentStreak: String = "2 Days"
    @Published var entriesTotal: String = "7"
    @Published var bestStreak: String = "10 Days"
    @Published var userPets: [PresentationPet] = []
    @Published var currentPet: PresentationPet = PresentationPet.Builder().build()
    @Published var currentPetActivities: [(String, Double)] = []
    @Published var currentActivity:(String,Double)?
    @Published var currentDate: String {
        didSet {
            updatePetActivities()
            updateCurrentPet()
        }
    }
    @Published var currentPetIdx: Int = 0 {
        didSet {
            updatePetActivities()
            updateCurrentPet()
        }
    }

    var currentPetName: String {
        currentPet.PetName ?? ""
    }
    var dates: [String] {
        let startDate = Calendar.current.date(byAdding: .day, value: -365, to: Date())!
        let endDate = Calendar.current.date(byAdding: .day, value: 365, to: Date())!

        let dayDurationInSeconds: TimeInterval = 60 * 60 * 24
        let formatter = DateFormatter()
        formatter.dateFormat = "dd-MM-yyyy"
        return stride(from: startDate, to: endDate, by: dayDurationInSeconds).map { currentDate in
            return formatter.string(from: currentDate)
        }
    }

    private func updateCurrentPet() {
        
        DispatchQueue.main.async {
            self.currentPet = self.userPets.first(where: { pet in pet.id == self.currentPetIdx })
                    ?? PresentationPet.Builder().build()
        }
    }

    init() {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd-MM-yyyy"
        currentDate = formatter.string(from: Date())
        fetchUserPets()

    }
    
    func fetchUserPets() {
        PetAPIRequests.getUserPets { response in
            DispatchQueue.main.async {
                self.userPets = response.data?.createdPets.map { apiPet in
                            return PresentationPet.fromAPIPet(apiPet)
                        }
                        .uniqued() ?? []

                self.currentPetIdx = self.userPets.first?.id ?? 0
            }
     
            self.updatePetActivities()
        }
    }

    private func updatePetActivities() {

        PetAPIRequests.getPetActivities(petId: currentPetIdx) { response in
            if response.isSuccessful() {
                if let data = response.data {
                    let groupedData = Dictionary(grouping: data.petExercises, by: { key -> String in
                        let dateFormatter = DateFormatter()
                        dateFormatter.locale = Locale(identifier: "en_US_POSIX") // set locale to reliable US_POSIX
                        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.000000Z"
                        let date = dateFormatter.date(from: key.createdAt)!
                        dateFormatter.dateFormat = "EEEE"
                        let dayInWeek = dateFormatter.string(from: date)

                        return dayInWeek

                    })
                    self.currentPetActivities = groupedData.map { key, value -> (String, Double) in

                                (key, value.reduce(0.0) { acc, ex -> Double in
                                    1.0 * acc + (Double(ex.mins) / 60.0) + Double(ex.hours)
                                })
                            }
                            .sorted(by: { first, second in
                                let dateFormatter = DateFormatter()
                                dateFormatter.dateFormat = "EEEE"
                                return dateFormatter.date(from: first.0)! < dateFormatter.date(from: second.0)!
                            })
               
                        self.currentActivity = self.currentPetActivities[0]
    
                }
            } else {
                DispatchQueue.main.async {
                    self.currentPetActivities = []
                    self.currentActivity = ("W",20.0)
                }
            }
        }
    }


}
