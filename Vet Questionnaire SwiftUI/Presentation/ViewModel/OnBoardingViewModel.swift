//
//  OnBoardingViewModel.swift
//  Vet Questionnaire SwiftUI
//
//  Created by NoobMaster69 on 15/03/2022.
//

import Foundation

class OnBoardingViewModel : ObservableObject{
    init(onBoardingSteps : [OnBoardingStep]){
        self.onBoardingSteps = onBoardingSteps
    }
    @Published private var onBoardingSteps : [OnBoardingStep];
    @Published var currentStepIdx = 0
    var currentStep : OnBoardingStep {onBoardingSteps[currentStepIdx]  }
    var stepsCount : Int {onBoardingSteps.count}
    func fetchNextOnBoardingStep(){
        currentStepIdx += 1
    }
}
