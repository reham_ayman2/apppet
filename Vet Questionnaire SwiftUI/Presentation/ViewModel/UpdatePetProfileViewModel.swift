//
//  UpdatePetProfileViewModel.swift
//  Vet Questionnaire SwiftUI
//
//  Created by NoobMaster69 on 06/05/2022.
//

import Foundation

class UpdatePetProfileViewModel: ObservableObject {

    @Published var pet: PresentationPet = PresentationPet
            .Builder()
            .build()

    init(petId: Int) {
        PetAPIRequests.getUserPets { apiResponse in
            if let data = apiResponse.data {
                let currentPet = data.createdPets.first(where: { $0.id == petId })
                if let apiPet = currentPet {
                    self.pet = PresentationPet.fromAPIPet(apiPet)
                }
            }
        }
    }

    func updatePet(_ pet: PresentationPet, finish: @escaping (_ apiResponse: APIResponse<UpdatePetResponse>) -> Void) {
        PetAPIRequests.updatePet(pet, finish: finish)
    }

}
