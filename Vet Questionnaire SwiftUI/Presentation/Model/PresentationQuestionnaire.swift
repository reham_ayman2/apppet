//
//  Questionnaire.swift
//  Vet Questionnaire SwiftUI
//
//  Created by NoobMaster69 on 07/03/2022.
//

import Foundation
import SwiftUI


// MARK: PresentationQuestionnaire
//  Beygama3 al Questions w al Answers m3a ba3d
//  edits by reham on 22 aug depend on eng bahy request





class PresentationQuestionnaire {
    
    private var answeredQuestions :[(q:PresentationQuestion, a:[PresentationAnswer])] = []
    private var questionsList:[PresentationQuestion] = []
    var petName = ""
    
    
    init(questionsList:[PresentationQuestion]=[], answeredQuestions:[(q:PresentationQuestion, a:[PresentationAnswer])] = []){
        
        self.questionsList.append(contentsOf: questionsList)
        self.answeredQuestions = answeredQuestions
        print("PresentationQuestionnaire Is Created")
    }
    
    
    func addQuestionAndAnswers( question:PresentationQuestion ,answers : [PresentationAnswer]){
        
        let questionIdx = questionsList.firstIndex(where: {q in q.id == question.id})!
        
        //        print("Answers = \(answers[0].content)")
        
        if (question == StaticQuestions.initialQuestions(petName: "")[0]){
            
            questionsList.removeAll()
           
                questionsList.append(contentsOf: StaticQuestions.initialQuestions(petName: answers[0].content))
               
                petName = answers[0].content
            
            
            
            
            
                // ⚠️ we have 21 questions ⚠️
            
            
           
        }else if (questionIdx == 1) {
            print("Pet Name = \(petName)")
            if answers[0].content == "Dog" {
                print("🐶🐶🐶🐶🐶🐶 hello its dog 🤝")
                UserDefaults.standard.set( false , forKey: "ISCAT")
                questionsList.remove(at: 2)
                questionsList.insert(StaticQuestions.whatIsYourDogBreed(petName: petName), at: 2)
            }else{
                
                
                print("😼😼😼😼😼😼😼 hello its cat 🤝")
                UserDefaults.standard.set( true , forKey: "ISCAT")

                // delete dogs questions
                
                questionsList.removeSubrange(ClosedRange(uncheckedBounds: (lower: 16, upper: 18)))
                
                if questionsList[2].content.contains("dog breed?"){
                    questionsList.remove(at: 2)
                    questionsList.insert(StaticQuestions.whatIsYourCatBreed(petName: petName), at: 2)
                }
            }
    
            
        }
        
        
        
        
        // 8 -  doesYourPetHaveAnyMedicalCondition ?
        
        else if (questionIdx == 8 ){
         
            if answers.isEmpty || answers[0].content == "No"    {
                if (questionsList.count > 15) {
                  
                    questionsList.removeSubrange(9...15)
                    
                    print("🙋‍♀️🙋‍♀️🙋‍♀️🙋‍♀️🙋‍♀️🙋‍♀️🙋‍♀️🙋‍♀️🙋‍♀️🙋‍♀️🙋‍♀️🙋‍♀️🙋‍♀️🙋‍♀️🙋‍♀️")
                  
                }
            }
//            else  if answers[0].content == "Yes" {
//                if (questionsList.count < 15) {
//                    questionsList.remove(at: 10)
//                    questionsList.append(contentsOf: StaticQuestions.medicalQuestions(petName: petName))
//
//                    questionsList.append(StaticQuestions.letsTakePhoto(petName: petName))
//                }
//            }
            
            
        }else{
            answeredQuestions.append((question,answers))
            answers.forEach{answer in
                questionsList.insert(contentsOf: answer.nextQuestions, at: questionIdx+1)
            }
        }
        questionsList = questionsList.uniqued()
        
        
    }
    
    //    func addQuestionAndAnswers( question:PresentationQuestion ,answers : [PresentationAnswer]){
    //
    //        let questionIdx = questionsList.firstIndex(where: {q in q.id == question.id})!
    //        if (question==StaticQuestions.whatIsYourPetName){
    //            questionsList.append(contentsOf: StaticQuestions.initialQuestions(petName: answers[0].content))
    //        }else{
    //            answeredQuestions.append((question,answers))
    //            answers.forEach{answer in
    //                questionsList.insert(contentsOf: answer.nextQuestions, at: questionIdx+1)
    //            }
    //        }
    //
    //        questionsList = questionsList.uniqued()
    //
    //    }
    
    func popQuestion(){
        
        if  let lastAnsweredQuestion :(q:PresentationQuestion, a:[PresentationAnswer]) = answeredQuestions.last{
            lastAnsweredQuestion.a.forEach{answer in
                questionsList.removeAll(where: { question in
                    answer.nextQuestions.contains{ q in q.id == question.id }
                })
            }
            answeredQuestions.removeLast()
        }
    }
    
    
    
    
    func getAnswers() -> [PresentationAnswer]{
        
        let answersList:[[PresentationAnswer]] = answeredQuestions.map{items in items.a}
        debugPrint("AnswerLis = \(answersList)")
        return answersList.reduce([]){acc, answers in acc + answers}
    }
    
    func getAnsweredQuestions()->[PresentationQuestion]{answeredQuestions.map{items in items.0}}
    
    func getQuestions()-> [PresentationQuestion]{questionsList}
}



extension Array {
  mutating func remove(at indexes: [Int]) {
    for index in indexes.sorted(by: >) {
      remove(at: index)
    }
  }
}
