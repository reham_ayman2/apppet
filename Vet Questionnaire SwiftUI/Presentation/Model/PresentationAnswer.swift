//
//  PresentationAnswer.swift
//  Vet Questionnaire SwiftUI
//
//  Created by NoobMaster69 on 04/03/2022.
//

import Foundation
import UIKit

class PresentationAnswer: Identifiable,Equatable,CustomStringConvertible{
    
    static func == (lhs: PresentationAnswer, rhs: PresentationAnswer) -> Bool {
        lhs.id == rhs.id
    }

    let id : Int
    let image : UIImage?
    let content : String
    let nextQuestions : [PresentationQuestion]
    private static var answersCount = 1
    var description : String{
        """
        id: \(id)
        image: \(String(describing: image))
        content: \(content)
        nextQuestions: \(nextQuestions)
        answersCount: \(PresentationAnswer.answersCount)
       """
    }

  
    init(content:String,image:UIImage?=nil, nextQuestions:[PresentationQuestion]=[]){
        self.id = PresentationAnswer.answersCount
        self.content = content
        self.nextQuestions = nextQuestions
        self.image = image
        PresentationAnswer.answersCount += 1
    }

    static func fromString(_ answer:String) ->PresentationAnswer{
        PresentationAnswer(content: answer)
    }
}
