////
////  Question.swift
////  Vet Questionnaire SwiftUI
////
////  Created by NoobMaster69 on 04/03/2022.
////
//
import Foundation
import UIKit
import SwiftUI

// MARK: PresentationQuestion

class PresentationQuestion: Identifiable, CustomStringConvertible, Hashable {

    var id: Int
    let image: UIImage?
    let med : [[String]]?
    var food : [[String]]?
    let content: String
    let answers: [PresentationAnswer]
    let answerType: AnswerType
    let questionType: QuestionType
    let updatePetBuilder: (PresentationPet.Builder, Any) -> PresentationPet.Builder
    
//    var description : String {
//        """
//        id: \(id)
//        image: \(String(describing: image))
//        content: \(content)
//        answers: \(answers)
//        answerType: \(answerType)
//        """
//    }
    
    
    
    var description: String {
        """
        id: \(id) content: \(content)
        """
    }

    private static var questionsCount = 0

   
    init(image: UIImage? = nil,
    
    med : [[String]]? = nil ,
         food : [[String]]? = nil ,
         cond : [[String]]? = nil
         
         
         ,
         content: String,
         answerType: AnswerType = .multiChoice,
         answers: [PresentationAnswer],questionType: QuestionType,
         updatePetBuilder: @escaping (PresentationPet.Builder, Any) -> PresentationPet.Builder) {
        self.content = content
        self.answers = answers
        self.image = image
        
        self.med = med
        self.food = food
        id = PresentationQuestion.questionsCount
        self.answerType = answerType
        PresentationQuestion.questionsCount += 1
        self.updatePetBuilder = updatePetBuilder
        self.questionType = questionType
        let _ = print(self.description)
    }

    static func ==(lhs: PresentationQuestion, rhs: PresentationQuestion) -> Bool {
        lhs.id == rhs.id || lhs.content == rhs.content
    }

    func hash(into hasher: inout Hasher) {
        hasher.combine(id)
    }

 
    enum AnswerType {
        case singleChoice
        case multiChoice
        case number
        case text
        case oneOfTwo
        case date
        case image
   
        
        case bodyCondition
        case caloriesEat
        case medicalIssue
        case medicationQ
        case multiLineInserts
        //  Oufaa
        case multiCheckBoxInserts
        // Kholy
        case multiChoicesCheckBox
        case gastrointestinal
        
        
        // added
        case multi
    
    }

    
       enum QuestionType {
           case optional
           case nonOptional
       }
    
    
    
}

