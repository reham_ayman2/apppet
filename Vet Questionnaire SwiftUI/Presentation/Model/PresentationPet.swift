//
//  PresentationPet.swift
//  Vet Questionnaire SwiftUI
//
//  Created by NoobMaster69 on 28/03/2022.
//

import Foundation
import SwiftUI

struct PresentationPet: Identifiable, Hashable {
    private init() {
    }

    var id: Int = 1
    private(set) var PetName: String? = nil
    private(set) var species: String? = nil
    private(set) var breed: String? = nil
    private(set) var breedTwo: String? = nil
    private(set) var secondaryBreed: String? = nil
    private(set) var isMixed: Int? = nil
    private(set) var birthDate: String? = nil
    private(set) var years: Int? = nil
    private(set) var months: Int? = nil
    private(set) var weight: Int? = nil
    private(set) var gender: String? = nil
    private(set) var breedId: Int? = nil
    private(set) var userID: Int? = nil
    private(set) var isSpayed: Bool? = nil
    private(set) var bodyScore: Int? = nil
    private(set) var mealsPerDay: Int? = nil
    private(set) var image: UIImage? = nil
    private(set) var imageUrl: String? = nil
    
    
    // added work
    private(set) var IsAnimalWorking : Bool? = nil
    private(set) var work: String? = nil
    private(set) var watchingweight : Bool? = nil
    private(set) var losingweigh : Bool? = nil
    private(set) var feedDaily : String? = nil
    
    // added 3 arrays
    private(set) var medications  : [[String]]? = nil
    private(set) var food :  [[String]]? = nil
    private(set) var conditions :  [[String]]? = nil
    
    
    
    
    
    
        
    
    
    static func fromAPIPet(_ apiPet: APIPet) -> PresentationPet {
        return PresentationPet.Builder()
                .setId(apiPet.id)
                .setSpecies(apiPet.specie)
                .setName(apiPet.petName)
                .setBreedOne(apiPet.breedOne)
                .setBreedTwo(apiPet.breedTwo ?? "")
                .setGender(apiPet.gender)
                .setSpayed(apiPet.spayedOr == 1 )
                .setWeight(apiPet.weight)
                .setBreedId(apiPet.breedID)
                .setUserId(apiPet.userID)
                .setAge(age: "\(apiPet.birthday),\(apiPet.petYears),\(apiPet.petMonths)")
                .setMixed(apiPet.mixed )
        
        
        
        
                .setFood(foods: apiPet.food ?? [["test nil "]] )
        
        
        
                
                .setImageUrl(url: apiPet.imagePath)
        
              
        
                .build()
        
    }

    class Builder {
        private var pet = PresentationPet()

        func setId(_ id: Int) -> Builder {
            pet.id = id
            return self
        }

        func setName(_ name: String) -> Builder {
            pet.PetName = name
            return self
        }

        func setSpecies(_ species: String) -> Builder {
            pet.species = species
            return self
        }

        
        
        
        func setBreed(_ breed: String) -> Builder {
            var breed1 :String
            var breed2  :String
            var isMixed :Int
            if breed != "" {
            let split = breed.split(separator: ",")
       
            breed1 = String(split[0])
            breed2 = String(split[1].trimmingCharacters(in: .whitespaces))
            isMixed =  String(split[2]) == "true" ? 1 : 0
            if isMixed == 1 {
                pet.breed = breed1
                pet.breedTwo = breed2
                pet.isMixed = isMixed
                
            }else{
                pet.breed = breed
            }
        }
            pet.breed = breed
            return self
        }
        
        // added
        func setBreedOne(_ breed: String) -> Builder {
            pet.breed = breed
            return self
        }
        func setBreedTwo(_ breedTwo: String) -> Builder {
            pet.breedTwo = breedTwo
            return self
        }
        func setBreedId(_ breedId: Int) -> Builder {
            pet.breedId = breedId
            return self
        }
        func setUserId(_ userId: Int) -> Builder {
            pet.userID = userId
            return self
        }
        func setWeight(_ weight: Int) -> Builder {
            pet.weight = weight
            return self
        }

        func setGender(_ gender: String) -> Builder {
            pet.gender = gender
            return self
        }

        func setSpayed(_ isSpayed: Bool) -> Builder {
            pet.isSpayed = isSpayed
            return self
        }
        
        func setMixed(_ isMixed: Int) -> Builder {
            pet.isMixed = isMixed
            return self
        }


        func setBodyScore(_ bodyScore: Int) -> Builder {
            pet.bodyScore = bodyScore
            return self
        }

        func setMealsPerDay(_ mealsCount: Int) -> Builder {
            pet.mealsPerDay = mealsCount
            return self
        }
        
        
        // added
        
        func setWork (_ work : String ) -> Builder {

            pet.work = work
            return self

        }
        func setIsAnimalWork (_ Iswork : Bool ) -> Builder {

            pet.IsAnimalWorking = Iswork
            return self

        }
        
        
        
        func setImage(_ image: UIImage?) -> Builder {
            pet.image = image
            return self
        }
        
        // arrayes
        func setMedicens ( _ medicen : [[String]]? ) -> Builder{
            
            
        //    pet.medications = medicens
            pet.medications = medicen
           
            return self
        }
        
        func setFood ( foods : [[String]]? ) -> Builder {
            
            pet.food = foods
            
            return self
        }
        
        
        func setCond ( cond : [[String]]? ) -> Builder {
            
            pet.conditions = cond
            return self
        }
        
        
        
        func setGoals (Iswatch : Bool) -> Builder {
            if Iswatch {
                pet.watchingweight = true
                pet.losingweigh = false
                
            } else {
                pet.watchingweight = false
                pet.losingweigh = true
                
            }
           
            
            return self
        }

        
        func setTimesFeed(_ Tiems: String) -> Builder {
            pet.feedDaily = Tiems
            return self
        }
       
        
        
        

        func setAge(age: String) -> Builder {
            if age != "" {
                print("Age is = \(age)")
                let split = age.split(separator: ",")
                pet.birthDate = String(split[0])
                
                
                pet.years = Int(split[1].trimmingCharacters(in: .whitespaces)) ?? 0
                pet.months = Int(split[2].trimmingCharacters(in: .whitespaces)) ?? 0
                
               
                
                
                return self
            }else {
                return self
            }
        }

        func setImageUrl(url:String) -> Builder{

            pet.imageUrl = url
            return self
        }
        
        func build() -> PresentationPet {
//            print("build Activated   pet ID = \(pet.id) , Pet Name = \(String(describing: pet.name) )  , Pet  gender \(pet.gender)")
           return pet
        }
    }


}
