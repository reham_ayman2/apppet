//
//  AllEntiersResponse.swift
//  Vet Questionnaire SwiftUI
//
//  Created by Reham Ayman on 29/08/2022.
//

import Foundation


struct ALlEntiers  : Codable {
    let status : String?
    let message : String?
    let AllENtiersdata : Data?

    enum CodingKeys: String, CodingKey {

        case status = "status"
        case message = "message"
        case AllENtiersdata = "data"
    }}


struct AllENtiersdata : Codable {
    let food : Food?
    let activity : Activity?
    let medical : [Medical]?

    enum CodingKeys: String, CodingKey {

        case food = "food"
        case activity = "activity"
        case medical = "Medical"
    }}

struct Medical : Codable {
    let daily_medications : [Daily_medications]?

    enum CodingKeys: String, CodingKey {

        case daily_medications = "daily medications"
    }
}
struct Daily_medications : Codable {
    let id : Int?
    let medicationName : String?
    let frequency : String?
    let petId : Int?
    let updated_at : String?
    let created_at : String?

    enum CodingKeys: String, CodingKey {

        case id = "id"
        case medicationName = "medicationName"
        case frequency = "frequency"
        case petId = "petId"
        case updated_at = "updated_at"
        case created_at = "created_at"
    }
}



struct Activity : Codable {
    let averageHoursForEachDay : Double?
    let HoursForEachDay : HoursForEachDay?
    let bCS : Int?

    enum CodingKeys: String, CodingKey {

        case averageHoursForEachDay = "average hours for each day"
        case HoursForEachDay = "hours for each day"
        case bCS = "BCS"
    }
}



struct Food : Codable {
    let amountOfFood : Double?
    let avegFoodAmount : Double?
    let caloriesForEachDay : CaloriesForEachDay?

    enum CodingKeys: String, CodingKey {

        case amountOfFood = "Amount of food"
        case avegFoodAmount = "aveg food amount"
        case caloriesForEachDay = "calories for each day"
    }
        }





struct CaloriesForEachDay : Codable {
    let saturday : Int?
    let sunday : Int?
    let monday : Int?
    let tuesday : Int?
    let wednesday : Int?
    let thursday : Int?
    let friday : Int?

    enum CodingKeys: String, CodingKey {

        case saturday = "Saturday"
        case sunday = "Sunday"
        case monday = "Monday"
        case tuesday = "Tuesday"
        case wednesday = "Wednesday"
        case thursday = "Thursday"
        case friday = "Friday"
    }

}
struct HoursForEachDay : Codable {
    let saturday : Int?
    let sunday : Double?
    let monday : Int?
    let tuesday : Int?
    let wednesday : Double?
    let thursday : Double?
    let friday : Int?

    enum CodingKeys: String, CodingKey {

        case saturday = "Saturday"
        case sunday = "Sunday"
        case monday = "Monday"
        case tuesday = "Tuesday"
        case wednesday = "Wednesday"
        case thursday = "Thursday"
        case friday = "Friday"
    }}
