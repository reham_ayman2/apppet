//
//  OnBoardingStep.swift
//  Vet Questionnaire SwiftUI
//
//  Created by NoobMaster69 on 15/03/2022.
//

import Foundation
import UIKit
class OnBoardingStep{
    init(title:String? = nil,content:String, image:UIImage){
        self.content = content
        self.image = image
        self.title = title
    }
    let image : UIImage
    let content : String
    let title:String?
}
