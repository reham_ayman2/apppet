import SwiftUI

enum Tabs: String {
    case home
    case today
    case entries
    case petProfiles
    case account

}

struct CoreAppContainerView: View {
    
    
    @State var currentTab: Tabs = .home
    @State var signOutDestEnabled = false
    let defualts = UserDefaults.standard

    var body: some View {


        NavigationView {
            if signOutDestEnabled {


                EntryAuthenticationView()
                
            } else {
                
                TabView(selection: $currentTab) {
                    
                    HomePageView().tabItem {
                                Label("Home", systemImage: "house")
                            }
                            .environmentObject(HomePageViewModel())
                            .tag(Tabs.home)
                    
                    

                    TodayPageView().tabItem {
                                Label("Today", systemImage: "calendar")
                            }
                            .environmentObject(TodayPageViewModel())
                            .tag(Tabs.today)


                    EntriesPageView().tabItem {
                                Label("Entries", systemImage: "square.grid.2x2")
                            }
                            .environmentObject(EntriesViewModel())
                            .tag(Tabs.entries)

                    VitalsPageView().tabItem {
                                Label {
                                    Text("Vitals")
                                } icon: {
                                    Image("ios-pulse")
                                }
                            }
                            .tag(Tabs.petProfiles)


                    AccountPageView(destEnabled: $signOutDestEnabled).tabItem {
                                Label("Account", systemImage: "person.fill")
                            }
                            .environmentObject(AccountViewModel())
                            .tag(Tabs.account)


                }
                        .accentColor(AppColors.secondaryColor)

                        .addHideKeyboardButton()
            }
        }
                .navigationBarBackButtonHidden(true)
                .navigationBarHidden(true)
    }
}

struct CoreAppContainerView_Previews: PreviewProvider {
    static var previews: some View {
        CoreAppContainerView()
    }
}
