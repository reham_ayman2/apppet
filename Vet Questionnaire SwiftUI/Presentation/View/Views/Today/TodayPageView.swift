//
//  TodayPageView.swift
//  Vet Questionnaire SwiftUI
//
//  Created by NoobMaster69 on 03/05/2022.
//

import SwiftUI
import SwiftUICharts

struct TodayPageView: View {

    @EnvironmentObject var viewModel: TodayPageViewModel
    let formatter = DateFormatter();

    init() {
        formatter.dateFormat = "dd-MM-yyyy"
    }

    var body: some View {

        ScrollView {
            VStack {
                HeaderView()

                HStack {
                    Text("Here are for ")
                            .font(.custom("Poppins-Bold", size: 24))
                            .bold()

                    Text("today.")
                            .font(.custom("Poppins-Bold", size: 24))
                            .bold()
                            .foregroundColor(AppColors.primaryColor)
                }
                        .frame(maxWidth: .infinity, alignment: .leading)
                        .padding()


                ScrollView(.horizontal) {
                    HStack {
                        ForEach(viewModel.userPets) { pet in
                            PetChipView(pet: pet)
                                    .opacity(pet.id == viewModel.currentPetIdx ? 1 : 0.5)
                                    .onTapGesture(perform: {
                                        viewModel.currentPetIdx = pet.id
                                    })
                        }

                    }
                }
                        .frame(minHeight: 100, maxHeight: 100)
                        .padding(.horizontal)

                VStack {
                    HStack {

                        Text("Pet Activities")
                                .bold()
                                .foregroundColor(.black)
                                .font(.custom("Poppins-Bold", size: 24))

                        Spacer()
                        Button("Last week") {

                        }
                                .padding(.horizontal, 20)
                                .padding(.vertical, 10)
                                .background(AppColors.secondaryColor)
                                .clipShape(RoundedRectangle(cornerRadius: 20))
                                .foregroundColor(.white)

                    }
                            .frame(maxWidth: .infinity, alignment: .leading).padding()

                    ScrollViewReader { proxy in
                        ScrollView(.horizontal) {

                            LazyHGrid(rows: [GridItem(.flexible(minimum: 120))]) {
                                ForEach(viewModel.dates, id: \.self) { dateItem in

                                    DateChipView(date: formatter.date(from: dateItem)!,
                                            selected: dateItem == viewModel.currentDate)
                                            .id(dateItem)
                                            .onTapGesture {
                                                viewModel.currentDate = dateItem
                                            }
                                            .padding(.horizontal, 2)
                                }

                            }

                        }
                                .onAppear {
                                    proxy.scrollTo(viewModel.currentDate, anchor: .center)
                                }

                    }

                    BarChartView(data: ChartData(values: [("M", 20.0),("T", 20.0),("W",20.0),(viewModel.currentActivity ??  ("no",20.0)),("F", 20.0),("S", 20.0),("S", 20.0)]), title: "Activities", style: Styles.barChartStyleNeonBlueLight, form: CGSize(width: 300, height: 300), dropShadow: false)
                            .padding()
                    
                    HStack{
                        VStack{
                            Text(viewModel.currentStreak)
                                .font(.custom("Poppins-SemiBold", size: 20))
                            Text("Current\nStreak")                                .font(.custom("Poppins-Regular", size: 14))
                                .multilineTextAlignment(.center)
                                .foregroundColor(.gray)

                            
                        }.frame(minWidth:115,maxWidth :115, minHeight:100,maxHeight: 100)
                            .background(Color.white)
                            .clipShape(RoundedRectangle(cornerRadius: 16))
                            .shadow(radius: 4.0)

                        
                        
                        VStack{
                            Text(viewModel.entriesTotal)
                                .font(.custom("Poppins-SemiBold", size: 20))
                            Text("Entries\nTotal")                                .font(.custom("Poppins-Regular", size: 14))
                                .multilineTextAlignment(.center)
                                .foregroundColor(.gray)

                            
                        }.frame(minWidth:115,maxWidth :115, minHeight:100,maxHeight: 100)
                            .background(Color.white)
                            .clipShape(RoundedRectangle(cornerRadius: 16))
                            .shadow(radius: 4.0)
                        
                        VStack{
                            Text(viewModel.bestStreak)
                                .font(.custom("Poppins-SemiBold", size: 20))
                            Text("Best\nStreak")                                .font(.custom("Poppins-Regular", size: 14))
                                .multilineTextAlignment(.center)
                                .foregroundColor(.gray)

                            
                        }.frame(minWidth:115,maxWidth :115, minHeight:100,maxHeight: 100)
                            .background(Color.white)
                            .clipShape(RoundedRectangle(cornerRadius: 16))
                            .shadow(radius: 4.0)

                    
                    }.padding()

                    Text("Actionable Insights")
                        .font(.custom("Poppins-Bold", size: 24))
                        .frame(maxWidth : .infinity, alignment: .leading)
                        .padding(.horizontal)
                    
                    VStack{
                        HStack{
                            Image(systemName: "info")
                                .resizable()
                                .scaledToFit()
                                .foregroundColor(.white)
                                .padding(8)
                                .background(AppColors.primaryColor)
                                .clipShape(Circle())
                                .frame(width:40, height: 40, alignment: .leading)
                            Text("Recommendations")
                                .font(.custom("Poppins-Bold", size: 20))
                            
                        }.frame(maxWidth: .infinity,alignment: .leading)
                            .padding()
                        
                        Divider()
                            .padding(.horizontal)
                        
                        HStack{
                            Image("Icon-tennisball")
                                .resizable()
                                .scaledToFit()
                                .foregroundColor(.white)
                                .frame(width: 30, height: 30, alignment: .center)
                            
                            VStack{
                                Text("Exercise Insight")
                                    .font(.custom("Poppins-SemiBold", size: 12))
                                    .frame(maxWidth: .infinity,alignment: .leading)
                                Text("\(viewModel.currentPetName) got plenty of exercises this week.")
                                    .font(.custom("Poppins-Regular", size: 16))
                                    .frame(maxWidth: .infinity,alignment: .leading)
                            }.frame(maxWidth: .infinity,alignment: .leading)
                        }.frame(maxWidth: .infinity,alignment: .leading)
                            .padding()
                        
                    }.background(Color.white)
                        .clipShape(RoundedRectangle(cornerRadius: 16))
                    .padding()

                }
                        .background(RoundedCorners(color: AppColors.lightBlue, tl: 24, tr: 24, bl: 0, br: 0))

            }
                    .frame(maxWidth: .infinity, maxHeight: .infinity, alignment: .topLeading)
        }

    }
}

struct TodayPageView_Previews: PreviewProvider {
    static var previews: some View {
        TodayPageView().environmentObject(TodayPageViewModel())
    }
}
