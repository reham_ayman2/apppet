//
//  SimpleDropDown.swift
//  Vet Questionnaire SwiftUI
//
//  Created by NoobMaster69 on 12/03/2022.
//

import SwiftUI

struct SimpleDropDown: View {
    
    let items :[String]
    let action : (String) -> ()
    var body: some View {
        
    
        
            VStack(alignment: .leading, spacing: 4){
                
                
                ForEach(items, id: \.self){ valueStore in
                    
                    Button(action:{action(valueStore)}) { Text(valueStore)
                            .frame(maxWidth: .infinity,alignment: .leading)
                            .foregroundColor(Color.black)
                            .padding([.leading, .top], 4)
                            .font(.custom("Poppins-Regular",size: 14))
                    }
                }
            }.frame(maxWidth: .infinity,maxHeight: 150)
            .padding(.all, 12)
            .background(RoundedRectangle(cornerRadius:6)
                            .foregroundColor(.white)
                            .shadow(radius: 2))
    }
}

struct SimpleDropDown_Previews: PreviewProvider {
    
    static var previews: some View {
        SimpleDropDown(items: ["1","2"], action: {data in}).padding()
    }
}
