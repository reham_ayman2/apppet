//
// Created by NoobMaster69 on 20/04/2022.
//

import Foundation
import SwiftUI

struct DateChipView: View {
    let date: Date
    let calendar = Calendar.current
    var components: DateComponents
    var monthString: String
    var selected: Bool

    init(date: Date, selected: Bool) {
        self.date = date
        components = Calendar.current.dateComponents([.day, .month, .year], from: date)
        let formatter = DateFormatter()
        formatter.dateFormat = "MMM yyyy"
        self.monthString = formatter.string(from: date)
        self.selected = selected
    }

    var body: some View {

        VStack {
            Text("\(monthString)")
                    .font(.custom("Poppins-SemiBold", size: 16))
            Text("\(components.day ?? 1)")
                    .font(.custom("Poppins-Bold", size: 28))
                    .padding(.bottom, 16)
        }
                .padding()
                .foregroundColor(selected ? .white : .black)
                .background(selected ? AppColors.primaryColor : Color.white)
                .clipShape(RoundedRectangle(cornerRadius: 20))
    }
}

struct DateChipView_Previews: PreviewProvider {
    struct DateChipViewHolder: View {
        @State var checked = true

        var body: some View {
            ZStack {
                Color.red.ignoresSafeArea()
                DateChipView(date: Date(),selected: $checked.wrappedValue)
            }
        }
    }

    static var previews: some View {
        DateChipViewHolder()
    }
}
