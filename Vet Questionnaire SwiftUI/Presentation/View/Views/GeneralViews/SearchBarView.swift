//
//  SearchBarView.swift
//  Vet Questionnaire SwiftUI
//
//  Created by Oufaa on 21/06/2022.
//

import SwiftUI

struct SearchBarView: View {
    
    @Binding var searchText :String

    var body: some View {
        HStack{
            TextField("Search Messages",text: $searchText)
            Image(systemName: "magnifyingglass")
                .foregroundColor(AppColors.secondaryColor)
                
            
        } .font(.headline)
            .padding()
            .frame(height: 50)
            .background(RoundedRectangle(cornerRadius: 15).fill(AppColors.searchBarColor))
            .padding(30)
            
            
            
        
    }
}

struct SearchBarView_Previews: PreviewProvider {
    static var previews: some View {
        SearchBarView(searchText: .constant("")).previewLayout(.sizeThatFits)
            
    }
}
