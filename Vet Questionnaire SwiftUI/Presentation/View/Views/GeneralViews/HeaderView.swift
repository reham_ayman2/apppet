//
// Created by NoobMaster69 on 11/04/2022.
//

import Foundation
import SwiftUI

struct HeaderView: View {
    @State var chatIsShowing  = false
    var body: some View {
        HStack {
            Image("AppLogo").resizable()
                    .scaledToFit()
                    .frame(maxWidth: 115)
            Spacer()
            
                Image(systemName: "bell")
                        .resizable()
                        .scaledToFit()
                        .frame(maxWidth: 24)
                        .foregroundColor(.gray)
                        .onTapGesture {
                            chatIsShowing = true
                        }
                        .fullScreenCover(isPresented: $chatIsShowing) {
                            ChatListView(chatListIsShowing: $chatIsShowing).environmentObject(ChatListViewModel())
                        }
            }
                .padding()
                .background(Color.white)
    }
}
