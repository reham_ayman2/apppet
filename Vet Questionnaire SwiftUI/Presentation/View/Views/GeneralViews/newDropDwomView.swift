//
//  newDropDwomView.swift
//  Vet Questionnaire SwiftUI
//
//  Created by Reham Ayman on 19/08/2022.
//

import SwiftUI

struct newDropDwomView: View {
//  @State var isShowen : Bool
    let items :[String]
    let action : (String) -> ()
    
    
    var body: some View {
        
        ZStack (alignment: .bottom) {
            
            Color.black
                .opacity(0.3)
                .ignoresSafeArea()
                .onTapGesture {
                   
                }
                        VStack {
                            ForEach(items, id: \.self){ valueStore in
                            
                Text(valueStore)
                            }
            }
            .frame( height: 400)
            .frame(  maxWidth: .infinity)
            .background(Color.black)
           
            
        }
        .frame(  maxWidth: .infinity, maxHeight: .infinity, alignment: .bottom)
        .ignoresSafeArea()
        .animation(.easeInOut)
        .onAppear {
          
            
        }
        
   
    }
        
}

   


struct newDropDwomView_Previews: PreviewProvider {
    static var previews: some View {
        newDropDwomView( items: ["1","2"] , action: {data in}).padding()
    }
}
