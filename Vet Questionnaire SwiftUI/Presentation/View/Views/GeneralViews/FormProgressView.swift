//
//  FormProgressView.swift
//  Vet Questionnaire SwiftUI
//
//  Created by NoobMaster69 on 14/03/2022.
//

import SwiftUI

struct FormProgressView: View {
    let totalSteps:Int
    let currentStepIdx : Int
    var body: some View {
        HStack{
            ForEach(1...totalSteps, id: \.self){idx in
                if(idx==currentStepIdx){
                    RoundedRectangle(cornerRadius: 50)
                        .fill(AppColors.primaryColor)
                        .frame(width: 10, height: 5)
                }
                else{
                    Circle()
                        .fill(AppColors.primaryColor)
                        .frame(width: 5, height: 5)
                        .opacity(0.5)
                }
                
            }
        }.frame(maxWidth: 150)
    }
}

struct FormProgressView_Previews: PreviewProvider {
    static var previews: some View {
        FormProgressView(totalSteps: 10,currentStepIdx: 7)
    }
}
