//
//  PetChipView.swift
//  Vet Questionnaire SwiftUI
//
//  Created by NoobMaster69 on 07/04/2022.
//

import SwiftUI

struct PetChipView: View {
    let pet:PresentationPet
    let dismissible : Bool
    let onDismissClick : (PresentationPet)->()

    init(pet:PresentationPet, dismissible: Bool = false,onDismissClick : @escaping ((PresentationPet)->()) = { pet in print("delete clicked") })
    {
        self.dismissible = dismissible
        self.onDismissClick = onDismissClick
        self.pet = pet
    }


    var body: some View {
        VStack{
            ZStack {
//                if let image  = pet.image {
//                    Image(uiImage: image)
//                        .resizable()
//                        .scaledToFit()
//                        .frame(maxWidth: 80, maxHeight: 80)
//                        .clipShape(Circle())
//
//                }
                if let image =  pet.imageUrl {
                    
                    FacilitiesRemoteImage(urString: image)
                }
                else {
                    let defImage = pet.species?.lowercased() ==  "cat" ? "cat" : "dog"
                    Image(defImage)
                        .resizable()
                        .scaledToFit()
                        .frame(maxWidth: 80, maxHeight: 80)
                        .clipShape(Circle())

                }
               
                if dismissible {
                    closeButton.padding(-2)
                }
            }.frame(maxWidth:80, maxHeight: 80)
            Text(pet.PetName ?? "").font(.custom("Poppins-Bold", size: 16)).frame(width: 60,  alignment: .center).lineLimit(0)
        }
    }

    var closeButton: some View {
        VStack {
            HStack {
                Spacer()
                Button(action: {
                    onDismissClick(pet)
                    }) {
                    Image(systemName: "xmark.circle")
                            .foregroundColor(.white)
                            .background(Color.red).clipShape(Circle())
                    }
            }
            Spacer()
        }
    }
}

struct PetChipView_Previews: PreviewProvider {
    static var previews: some View {
        PetChipView(pet: DummyPets.dummyPet,dismissible: true, onDismissClick: {pet in })
    }
}
