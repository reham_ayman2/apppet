//
//  AddPetActivityView.swift
//  Vet Questionnaire SwiftUI
//
//  Created by NoobMaster69 on 27/04/2022.
//

import SwiftUI

struct AddButtonFullWidthView: View {
    var body: some View {
        VStack{

            Image(systemName: "plus.circle.fill")
                    .resizable()
                    .scaledToFit()
                    .frame(maxHeight: 60)
                    .foregroundColor(AppColors.secondaryColor)
                    .padding()


            Text("Add").bold().font(.custom("Poppins-Bold", size: 18))
                .padding(.bottom)
            

        }.padding(.horizontal)
                .frame(maxWidth:.infinity)
                .background(Color.white)
                .clipShape(RoundedRectangle(cornerRadius: 24))




    }
}

struct AddPetActivityView_Previews: PreviewProvider {
    static var previews: some View {
        ZStack{
            Color.red.ignoresSafeArea()
            AddButtonFullWidthView()

        }
    }
}
