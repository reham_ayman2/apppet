//
//  LoadingView.swift
//  Vet Questionnaire SwiftUI
//
//  Created by NoobMaster69 on 05/04/2022.
//

import SwiftUI

struct LoadingView: View {
    @Binding var isLoading: Bool
    var body: some View {
        if isLoading {
            ZStack {
                Color(UIColor(AppColors.primaryColor)).ignoresSafeArea().opacity(0.4)
                ProgressView()
                        .progressViewStyle(CircularProgressViewStyle(tint: AppColors.secondaryColor))
                        .scaleEffect(3.0)
            }
        }
        else {
            EmptyView()
        }

    }
}

struct LoadingView_Previews: PreviewProvider {

    @State static var isLoading: Bool = true
    static var previews: some View {
        LoadingView(isLoading: $isLoading)
    }
}
