//
//  PetCardView.swift
//  Vet Questionnaire SwiftUI
//
//  Created by NoobMaster69 on 06/04/2022.
//

import SwiftUI

struct PetCardView: View {
    let pet : PresentationPet
    var body: some View {
        VStack{
            ZStack{
                if let image =  pet.imageUrl {
                    
                    FacilitiesRemoteImage(urString: image)
                }
                else {
                    let imageName = pet.species?.lowercased() == "cat" ? "cat" : "dog"
                    let image = UIImage(named:imageName)
                    Image(uiImage: image!)
                        .resizable()
                        .scaledToFit()
                }
            }.frame(width: 80, height: 80, alignment: .center)
                .clipShape(Circle())
            
            Text(pet.PetName ?? "").bold().font(.custom("Poppins-Bold", size: 18)).padding(.top,12)
            Text(pet.breed ?? "")
                .bold()
                .lineLimit(1)
                .font(.custom("Poppins-Bold", size: 14))
                .opacity(0.5)
            Text("\(pet.years ?? 0) year(s) \(pet.months ?? 0) month(s)").bold().font(.custom("Poppins-Regular", size: 12)).opacity(0.5)
            
        }.frame(width: 180, height: 200, alignment: .center)
            .background(Color.white)
            .clipShape(RoundedRectangle(cornerRadius: 24))
    }
}

struct PetCardView_Previews: PreviewProvider {
    static var previews: some View {
        let pet = PresentationPet.Builder()
            .setBreed("Birman")
            .setName("Mahdy")
            .setSpecies("cat")
            .build()
        PetCardView(pet: pet)
    }
}


//struct PetCardView: View {
//    @Binding var pet : PresentationPet?
//    var body: some View {
//        VStack{
//            ZStack{
//                if let image =  pet?.image {
//                    Image(uiImage: image)
//                        .resizable()
//                        .scaledToFit()
//                }
//                else {
//                    let imageName = pet?.species?.lowercased() == "cat" ? "cat" : "dog-profile"
//                    let image = UIImage(named:imageName)
//                    Image(uiImage: image!)
//                        .resizable()
//                        .scaledToFit()
//                }
//            }.frame(width: 75, height: 75, alignment: .center)
//                .clipShape(Circle())
//            
//            Text(pet?.name ?? "").bold().font(.custom("Poppins-Bold", size: 18)).padding(.top,12)
//            Text(pet?.breed ?? "")
//                .bold()
//                .lineLimit(1)
//                .font(.custom("Poppins-Bold", size: 18))
//                .opacity(0.5)
//            Text("\(pet?.years ?? 0) year(s) \(pet?.months ?? 0) month(s)").bold().font(.custom("Poppins-Bold", size: 16)).opacity(0.5)
//
//        }.frame(width: 180, height: 200, alignment: .center)
//            .background(Color.white)
//            .clipShape(RoundedRectangle(cornerRadius: 24))
//    }
//}
//
//struct PetCardView_Previews: PreviewProvider {
//    static var previews: some View {
//        let pet = PresentationPet.Builder()
//            .setBreed("Birman")
//            .setName("Mahdy")
//            .setSpecies("cat")
//            .build()
//        PetCardView(pet: pet)
//    }
//}
