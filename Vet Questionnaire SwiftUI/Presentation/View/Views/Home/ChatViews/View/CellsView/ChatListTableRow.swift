//
//  ChatListTableRow.swift
//  Vet Questionnaire SwiftUI
//
//  Created by Oufaa on 21/06/2022.
//

import SwiftUI


struct ChatListTableRow: View {
    
    @State var imageName : String = "cat"
    @State var userName: String = "Syamy"
    @State var lastMassage:String = "Hello"
    @State var date : String =  "22 Apr"
    @State var notification: Int = 2
    
    var body: some View {
        
        HStack{
            Image(imageName)
                .resizable()
                .aspectRatio(contentMode: .fit)
                .frame( height: 60)
                .clipShape(Circle())
                .padding(10)
            
            VStack(alignment: .leading,spacing: 4){
                Text(userName)
                    .font(.system(size: 24))
                    .font(.title.bold())
                   
                
                Text(lastMassage)
                    .font(.body)
                    .fontWeight(.light)
                    .foregroundColor(Color.gray)
                    .lineLimit(2)
                    .padding(.leading,4)
                    
                
            }.frame(width: 250,alignment: .leading)
            VStack{
                Text(date)
                    .fontWeight(.light)
                    .font(.caption)
                    .foregroundColor(Color.gray)
                    .padding(.bottom,4)
                
                Text("\(notification)")
                    .foregroundColor(.white)
                    .font(.system(size: 13))
                      .background(Circle()
                          .fill(.blue)
                          .frame(width: 20, height: 20))
            }.padding()
          
        }
   
        
    }
}

struct ChatListTableRow_Previews: PreviewProvider {
    static var previews: some View {
        ChatListTableRow().previewLayout(.sizeThatFits)
    }
}
