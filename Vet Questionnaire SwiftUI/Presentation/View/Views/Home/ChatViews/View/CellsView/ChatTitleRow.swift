//
//  ChatTitleRow.swift
//  Vet Questionnaire SwiftUI
//
//  Created by Oufaa on 21/06/2022.
//

import SwiftUI

struct ChatTitleRow: View {
   @State var imageName : String = "dog"
    @State var petName : String = "Syamy"
    @Binding var returnButton: Bool
    var body: some View {
        
        HStack(alignment: .center){
            
            Button {
                returnButton = false
            } label: {
                Image("button-left")
                    .renderingMode(.original)
                    .aspectRatio(contentMode: .fit)
                    .frame(width: 50, height: 50)
            }.padding()
                .padding()
                
            

            
            Image(imageName).resizable()
                .aspectRatio(contentMode: .fit)
                .frame(width: 70, height: 70)
                .cornerRadius(50)
                .padding()
            Text(petName).font(.largeTitle).fontWeight(.bold)
        }.padding(.leading,-120)
        
    }
}

struct ChatTitleRow_Previews: PreviewProvider {
    static var previews: some View {
        ChatTitleRow(returnButton: .constant(false))
            
            
            
    }
}
