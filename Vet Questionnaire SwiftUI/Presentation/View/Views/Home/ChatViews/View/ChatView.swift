//
//  ChatView.swift
//  Vet Questionnaire SwiftUI
//
//  Created by Oufaa on 21/06/2022.
//

import SwiftUI

struct ChatView: View {
    
    @State var imageName : String
    @State var petName : String
    @Binding var returnButton : Bool
    var body: some View {
        VStack{
        ChatTitleRow(imageName: imageName, petName: petName, returnButton: $returnButton)
            Divider()
            ScrollView{
                Text("Friday , Apr 22")
                    .font(.title3)
                    .fontWeight(.regular)
                    .foregroundColor(Color.gray)
                
                ForEach(MockMessagesData.messagesMockData) { row in
                    MessageBubbleView(message: row)
                }
            }
        }.navigationBarHidden(true)
            .navigationBarBackButtonHidden(true)
    }
}

struct ChatView_Previews: PreviewProvider {
    static var previews: some View {
        ChatView(imageName: "cat", petName: "sss", returnButton: .constant(false))
    }
}
