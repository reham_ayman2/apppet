//
//  MessageBubble.swift
//  Vet Questionnaire SwiftUI
//
//  Created by Oufaa on 21/06/2022.
//

import SwiftUI

struct MessageBubbleView: View {
    var message: Messages
    var body: some View {
        
        VStack(alignment: message.received ? .leading : .trailing ){
            HStack{
                Text(message.text)
                    .padding()
                    .background(message.received ? Color("recievedChatColor") : Color("senderChatColor"))
                    .cornerRadius(10)
            }.frame(maxWidth:300, alignment: message.received ? .leading : .trailing)
            
        }.frame(maxWidth:.infinity, alignment: message.received ? .leading : .trailing)
            .padding(message.received ? .leading : .trailing)
            .padding(.horizontal,10)
        
        
    }
}

struct MessageBubble_Previews: PreviewProvider {
    static var previews: some View {
        MessageBubbleView(message: MockMessagesData.messagesMockData[0])
    }
}
