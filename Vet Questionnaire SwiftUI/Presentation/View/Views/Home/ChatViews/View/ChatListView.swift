//
//  ChatListView.swift
//  Vet Questionnaire SwiftUI
//
//  Created by Oufaa on 21/06/2022.
//

import SwiftUI

struct ChatListView: View {
    
    @EnvironmentObject private  var viewModel: ChatListViewModel
    @Binding var chatListIsShowing : Bool
    @State var chatIsShowing : Bool = false
    var body: some View {
        
        NavigationView {
            ScrollView{
                
                SearchBarView(searchText: $viewModel.searchText)
                Divider()
    
                
            
                ForEach(ChatListMockData.chatListMockData) { row in
                    
//                    let _ = print("UserName is = \(row.userName) ")
                    
//                    NavigationLink(destination: ChatView(imageName: row.imageName, petName: row.userName, returnButton: $chatIsShowing),isActive: $chatIsShowing) {
                        
                    ChatListTableRow(imageName: row.imageName, userName: row.userName, lastMassage: row.lastMassage, date: row.date, notification: row.notification).onTapGesture {
                        viewModel.selectedName = row.userName
                        viewModel.selectedImage = row.imageName
                        chatIsShowing = true
                    }.fullScreenCover(isPresented: $chatIsShowing) {
                        
                        ChatView(imageName: viewModel.selectedImage, petName: viewModel.selectedName, returnButton: $chatIsShowing)
                    }
                        
//                    }.accentColor(.black)
                    
//                    ChatListTableRow(imageName: row.imageName, userName: row.userName, lastMassage: row.lastMassage, date: row.date, notification: row.notification)
                    
                }
                    
                }
                .navigationTitle("Messaging")
                .navigationBarTitleDisplayMode(.inline)
                .toolbar {
                    ToolbarItem(placement: .navigationBarLeading) {
                        Button {
                            chatListIsShowing.toggle()
                        } label: {
                            Image("button-left")
                                .resizable()
                                .frame(width: 80, height: 80)
                                .aspectRatio(contentMode: .fill)
                                .padding(.leading,-20)
                                
                        }
                        
                    }
                }
                
                
                
                
            }.navigationBarHidden(true)
            .navigationBarBackButtonHidden(true)
        }
    }
    
    struct ChatListView_Previews: PreviewProvider {
        static var previews: some View {
            ChatListView(chatListIsShowing: .constant(false)).environmentObject(ChatListViewModel())
        }
    }
