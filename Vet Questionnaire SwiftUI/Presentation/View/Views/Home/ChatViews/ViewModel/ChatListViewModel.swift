//
//  ChatListViewModel.swift
//  Vet Questionnaire SwiftUI
//
//  Created by Oufaa on 21/06/2022.
//

import Foundation

class ChatListViewModel:ObservableObject{
    @Published var selectedName: String = ""
    @Published var selectedImage: String = ""
    @Published var searchText: String = ""
}
