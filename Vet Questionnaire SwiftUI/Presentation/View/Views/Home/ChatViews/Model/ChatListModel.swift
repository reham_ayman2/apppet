//
//  ChatListModel.swift
//  Vet Questionnaire SwiftUI
//
//  Created by Oufaa on 21/06/2022.
//

import Foundation

struct ChatListModel: Hashable ,Identifiable {
  
    
    
    let id = UUID()
    var imageName:String
    var userName: String
    var lastMassage:String
    var date : String
    var notification: Int
    
}

struct ChatListMockData {
    
    static let chatListMockData = [
        ChatListModel(imageName: "cat", userName: "Samyy", lastMassage: "HelloFromMassages", date: "22 Jun", notification: 2),
        ChatListModel(imageName: "dog", userName: "kemi", lastMassage: "HelloFromMassage that chat just for demo to try code inside view thank you", date: "22 Jun", notification: 2),
        ChatListModel(imageName: "cat", userName: "marti", lastMassage: "HelloFromMassages", date: "22 Jun", notification: 3),
        ChatListModel(imageName: "dog", userName: "Helsinki", lastMassage: "HelloFromMassages", date: "22 Jun", notification: 5),
        ChatListModel(imageName: "cat", userName: "Dodge", lastMassage: "HelloFromMassages", date: "22 Jun", notification: 1),
        ChatListModel(imageName: "dog", userName: "crican", lastMassage: "HelloFromMassages", date: "22 Jun", notification: 7)
    ]
}
