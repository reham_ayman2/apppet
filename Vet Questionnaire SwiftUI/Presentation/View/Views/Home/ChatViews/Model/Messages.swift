//
//  Messages.swift
//  Vet Questionnaire SwiftUI
//
//  Created by Oufaa on 21/06/2022.
//

import Foundation

struct Messages: Identifiable,Hashable {
    
    var id = UUID()
    var text : String
    var received : Bool
    var timesTamp: Date
}

struct MockMessagesData {
    static let messagesMockData =
    [
        Messages(text: "Hello", received: false, timesTamp: Date()),
        Messages(text: "Hello", received: true, timesTamp: Date()),
        Messages(text: "How are you", received: true, timesTamp: Date()),
        Messages(text: "fine", received: false, timesTamp: Date()),
        Messages(text: "what about you", received: false, timesTamp: Date()),
        Messages(text: "fine ", received: true, timesTamp: Date()),
        Messages(text: "Always", received: false, timesTamp: Date())
    ]
    
}
