//
//  HomePageView.swift
//  Vet Questionnaire SwiftUI
//
//  Created by NoobMaster69 on 06/04/2022.
//

import SwiftUI
import SwiftUICharts





// static graph content // its sample change when api update

struct Download : Identifiable  {
    var id = UUID().uuidString
    let downloads : CGFloat
    var weekDay : String
    
}

struct HomePageView: View {
    @EnvironmentObject var viewModel: HomePageViewModel
    @State var segmentSelectedIndex = 0
    
    
    
    var Staticcal = [Download(downloads: 1000, weekDay: "M")
                     , Download(downloads: 400, weekDay: "T")
                     ,Download(downloads: 700, weekDay: "W")
                     ,Download(downloads: 400, weekDay: "T")
                     ,Download(downloads: 500, weekDay: "S")
                     ,Download(downloads: 500, weekDay: "S")
        
        
        ]
    
    init() {
        UISegmentedControl.appearance().selectedSegmentTintColor = UIColor(named: "segmentSelectColor")
       
        UISegmentedControl.appearance().setTitleTextAttributes([.foregroundColor: UIColor.white], for: .selected)
        UISegmentedControl.appearance().setTitleTextAttributes([.foregroundColor: UIColor(named: "segmentSelectColor") ?? .gray], for: .normal)
        UISegmentedControl.appearance().backgroundColor = UIColor(named: "segmentUnselectColor")
        UISegmentedControl.appearance().layer.cornerRadius = 25
        
    }
    
    
    
    var body: some View {
        
     
        ScrollView {
          
            VStack {
                HeaderView()
                
                HStack {
                    Text("Hello, ")
                        .font(.custom("Poppins-Bold", size: 24))
                        .bold()
                    
                    Text(viewModel.userName)
                        .font(.custom("Poppins-Bold", size: 24))
                        .bold()
                        .foregroundColor(AppColors.primaryColor)
                    Text("!")
                        .font(.custom("Poppins-Bold", size: 24))
                    
                }
                .frame(maxWidth: .infinity, alignment: .leading)
                .padding()
                
                //                Image("cat")
                //                        .scaledToFit()
                //                        .frame(maxHeight: 200)
                //                        .padding()
                
                VStack {
                    Text("Pet Profiles")
                        .bold()
                        .foregroundColor(.black)
                        .frame(maxWidth: .infinity, alignment: .leading)
                        .font(.custom("Poppins-Bold", size: 24))
                        .padding()
                    
                    ScrollView(.horizontal) {
                        HStack {
                            ForEach(viewModel.userPets) { pet in
                                //                               var _ = print("PetsHere \(pets)")
                                NavigationLink(destination: PetProfilePageView().environmentObject(PetProfilePageViewModel(petId: pet.id))) {
                                    PetCardView(pet: pet).padding(4)
                                }.accentColor(.black)
                            }
                        }
                    }
                    
                    // ========
                    
                    Text("Your Pet’s Insights")
                        .font(.custom("Poppins-Bold", size: 22))
                     
                        .frame(maxWidth: .infinity , alignment: .leading)
                    
                    
                    
                    HStack(  spacing: 25) {
                        VStack (spacing: 15){
                            Image("Group 12878")
                            Text("Male")
                                .font(.custom("Poppins-SemiBold", size: 16))
                        }
                    
                        VStack (spacing: 15){
                            Image("Group 12879")
                            Text("Kg")
                                .font(.custom("Poppins-SemiBold", size: 16))
                        }
                        
                        VStack(spacing: 15) {
                            Image("Group 12878-1")
                            Text("3")
                                .font(.custom("Poppins-SemiBold", size: 16))
                        }
                        
                    }
                    
                    .frame( height: 100 )
                    .frame( maxWidth: .infinity)
                    .background(Color.white)
                    .cornerRadius(25)
                    .shadow(color: .gray .opacity(0.4), radius: 3, x: 4, y: 4)
                    
                    
                    
                    
                    
                    
                             
                     //MARK: -  here sdd your segment
                             
                    Picker(selection: $segmentSelectedIndex , label:  Text("")) {
                        
                        
                        Text("Food").tag(0)
                        Text("Activity").tag(1)
                        Text("Medical").tag(2)
                    }.pickerStyle(SegmentedPickerStyle())
                     
                        .scaledToFit()
                        .cornerRadius(25)
                        .scaleEffect(CGSize(width: 1, height: 1.3))
                        
                        .padding()
                      
                       
                       
                    
                    //MARK: -  here add your chart
                    
                    
                    VStack {
                        VStack {
                        Image ("Group 12880")
                            Text( "600 kcal")
                                .font(.custom("Poppins-Bold", size: 16))
                            Text ( "11 - 17 Apr 2022")
                                .font(.custom("Poppins-Regular", size: 12))
                                .foregroundColor(.gray)
                                
                        }.frame(minWidth: .infinity , alignment: .leading )
                        
                        HStack {
                            
                            Text("#Tiems Food logged")
                                .rotationEffect(.degrees(-90))
                                .font(.custom("Poppins-Regular", size: 12))
                                              .fixedSize()
                                              .frame(width: 20, height: 180)
                                              .padding(.leading , 10 )
                                              .foregroundColor(.gray)
                            
                               // .rotationEffect(Angle(degrees: 270))
                               
                                
                            
                            
                            
                    GeometryReader { proxy in
                        ZStack {
                            
                            
                            VStack (spacing: 0) {
                            ForEach(getGraphLiens() , id: \.self ) { line in
                                HStack (spacing: 8) {
                                    
                                    Text("\( Int( line ))")
                                        .font(.custom("Poppins-SemiBold", size: 12))
                                        .foregroundColor(.gray)
                                    Rectangle().fill(Color.gray.opacity(0.3))
                                        .frame( height: 1)
                                    
                                }.frame(maxHeight: .infinity , alignment: .bottom)
                                    .padding(.bottom , 20)
                                
                            
                            }
                            
                            }
                              
                             
                            
                            
                            
                            HStack{
                                
                              
                                
                                
                                
                                ForEach (Staticcal) { download in
                                    
                                    
                                    VStack ( spacing: 0) {
                                   
                                    VStack ( spacing: 5 ){
                                        
                                        Capsule()
                                            .fill(Color("mainColor"))
                                           
                                        
                                    }.frame(width: 8)
                                      .frame( height: getBarHeight(point: download.downloads, size: proxy.size))
                                           
                                          
                                        
                                        Text( download.weekDay )
                                            .font(.custom("Poppins-Bold", size: 14))
                                            .foregroundColor(.gray)
                                            .frame( height: 25)
                                            
                                    
                                }
                                    
                                    
                                    .frame( maxWidth: .infinity , maxHeight: .infinity , alignment: .bottom )
                                
                                }
                                
                            }
                            
                            .frame( maxWidth: .infinity , maxHeight: .infinity , alignment: .center )
                            .padding(.leading , 30 )
                        //    .padding()
                            
                        }
                        
                        
                        .padding()
                        
                        
                    }
                            
                          
                            
                        }
                    }
                    
                    
                    .cornerRadius(25)
                    .frame( height: 260)
                   
                    .background(Color.white)
                    .cornerRadius(25)
                    .shadow(color: .gray .opacity(0.4), radius: 3, x: 4, y: 4)
                    
                    //MARK: -  here add ammount of food
                    VStack (alignment: .leading) {
                        
                        VStack {
                        HStack{
                        Image( "Path 10009")
                            
                        Text( "Amount of Food")
                        .font(.custom("Poppins-Regular", size: 14))
                        }
                        
                        Text ( "175 Calories")
                            .font(.custom("Poppins-Bold", size: 18))
                           
                          
                        }.padding()
                        
                        
                        
                        
                    }
                    .frame( maxWidth: .infinity , alignment: .leading  )
                    .frame( height: 110 )
                    
                    
                    .background(Color.white)
                    .cornerRadius(25)
                    .shadow(color: .gray .opacity(0.4), radius: 3, x: 4, y: 4)
                    .padding(.top , 10)
                    
                    
                    
                    // ====  end of ammount of food
                    
                    //MARK: - Usage Streak
                    
                    Text ("Usage Streak")
                        .font(.custom("Poppins-Bold", size: 22))
                     
                        .frame(maxWidth: .infinity , alignment: .leading)
                        .padding(.top , 15)
                    
                    
                    
                    HStack(alignment: .center, spacing: 15) {
                        
                        VStack{
                            Text( "2 days")
                                .font(.custom("Poppins-Bold", size: 14))
                            VStack{
                            Text ("Current")
                                Text ( "Streak")
                            }
                                .foregroundColor(.gray)
                                .lineLimit(2)
                            
                        }
                       
                        .frame(width:100 , height: 100)
                        .background(Color.white)
                        .cornerRadius(15)
                        
                        .shadow(color: .gray .opacity(0.4), radius: 3, x: 5, y: 5)
                        VStack {
                            Text ("10")
                                .font(.custom("Poppins-Bold", size: 18))
                            VStack {
                            Text( "Entries")
                            Text( "Total")
                            }
                                .font(.custom("Poppins-Regular", size: 14))
                                .foregroundColor(.gray)
                                .lineLimit(2)
                        }
                        .frame(width:100 , height: 100)
                        .background(Color.white)
                        .cornerRadius(15)
                        
                        .shadow(color: .gray .opacity(0.4), radius: 3, x: 4, y: 4)
                        VStack {
                            Text ( "7 days")
                                .font(.custom("Poppins-Bold", size: 18))
                            
                            VStack {
                            Text ("Best")
                            Text ("Streak")
                            }
                                .font(.custom("Poppins-Regular", size: 13))
                                
                                .foregroundColor(.gray)
                                .lineLimit(2)
                            
                        }
                        .frame(width:100 , height: 100)
                        .background(Color.white)
                        .cornerRadius(15)
                        
                        .shadow(color: .gray .opacity(0.4), radius: 3, x: 4, y: 4)
                        
                    }
                    .frame( maxWidth: .infinity  )
                    .frame( height: 140 )
                    
                    
                    .background(Color.white)
                    .cornerRadius(25)
                    .shadow(color: .gray .opacity(0.4), radius: 3, x: 4, y: 4)
                   // .padding(.top , )
                    
                    
                    
                    
                    
                    
                    
                    
                    // hide this h stack have pet activity + last week
                    
                    
                    
                    
//                    HStack {
//
//                        Text("Pet Activities")
//                            .bold()
//                            .foregroundColor(.black)
//                            .font(.custom("Poppins-Bold", size: 24))
//
//                        Spacer()
//                        Button("Last week") {
//
//                        }
//                        .padding(.horizontal, 20)
//                        .padding(.vertical, 10)
//                        .background(AppColors.secondaryColor)
//                        .clipShape(RoundedRectangle(cornerRadius: 20))
//                        .foregroundColor(.white)
//
//                    }
//                    .frame(maxWidth: .infinity, alignment: .leading).padding()
                    
//                    ScrollView(.horizontal) {
//                        HStack {
//
//                            //                            func getUserPets() async{
//                            //                        ForEach(viewModel.userPets) { pet in
//                            //                            PetChipView(pet: pet)
//                            //                                    .opacity(pet.id == viewModel.currentPetIdx ? 1 : 0.5)
//                            //                                    .onTapGesture(perform: {
//                            //                                        viewModel.currentPetIdx = pet.id
//                            //                                    })
//                            //                        }
//                            //                            }
//                            ForEach(viewModel.userPets) { pet in
//                                PetChipView(pet: pet)
//                                    .opacity(pet.id == viewModel.currentPetIdx ? 1 : 0.5)
//                                    .onTapGesture(perform: {
//                                        viewModel.currentPetIdx = pet.id
//                                    })
//                            }
//
//                        }
//                    }
//                    .padding(.horizontal)
                   
                    
//                    VStack {
//
//                      //viewModel.currentPetCalories
//
//                        BarChartView(data: ChartData(values: [("M", 20.0),("T", 20.0),("W",3.0),("T",3.0),("F",3.0) , ("S",10.0) , ("S",17.0)] ), title: "Calories", style: Styles.barChartStyleNeonBlueLight, form: CGSize(width: 300, height: 300), dropShadow: false)
//                            .padding(.vertical, 4)
//                            .padding(.leading, 4)
//                            .padding(.trailing, 2)
////
//
//
//
//                        BarChartView(data: ChartData(values: [("M", 20.0),("T", 20.0),("W",3.0),(viewModel.currentActivity ??  ("no",20.0)),("F", 20.0),("S", 20.0),("S", 20.0)]), title: "Activities", style: Styles.barChartStyleNeonBlueLight, form: CGSize(width: 300, height: 300), dropShadow: false)
//                            .padding(.vertical, 4)
//                            .padding(.trailing, 4)
//                            .padding(.leading, 2)
//                    }
//                    HStack {
//                        DataChipView(imageName: "noun-dog-food", title: "Calories Avg", subTitle: viewModel.currentPetAverageCalories)
//                            .padding(.trailing, 4)
//
//                        DataChipView(imageName: "Icon-tennisball", title: "Activities Avg", subTitle: viewModel.currentPetAverageActivityTime)
//                            .padding(.leading, 4)
//                    }.padding(4)
                    
                }
                .padding()
                .frame(minWidth: 0.0, maxWidth: .infinity, minHeight: 0.0,
                       maxHeight: .infinity)
                .background(RoundedCorners(color: AppColors.lightBlue, tl: 24, tr: 24, bl: 0, br: 0))
              
                
                
            }
            .frame(maxWidth: .infinity, maxHeight: .infinity, alignment: .top)
           
            
        }
        .onAppear(perform: {
            viewModel.fetchPetData()
        })
        .navigationBarBackButtonHidden(true)
        .navigationBarHidden(true)
        .frame(maxWidth: .infinity, alignment: .top)
        .edgesIgnoringSafeArea(.bottom)
        
    }
    
    //MARK: - GRAPH METHODS
    
    
    func getBarHeight (point : CGFloat , size : CGSize ) -> CGFloat {
        let max = getMax()
        
        // 25 text height
        // 5 spacing
        let height = ( point / max ) * ( size.height - 50 )
        
        return height
    }
    
    
    
    
    func getGraphLiens () -> [CGFloat ]{
        let max = getMax()
        var lines : [CGFloat] = []
        
        lines.append(max)
        for index in 1...4 {
            let progress = max / 4
            lines.append(max - ( progress * CGFloat (index)))
        }
        
        
        return lines
    }
    
    
    
    
    
    
    
    
    func getMax () -> CGFloat  {
        
        let max = Staticcal.max{ first , sec in
            return sec.downloads > first.downloads
        }?.downloads ?? 0
            
            
            return max
        }
        
    }


struct HomePageView_Previews: PreviewProvider {
    static var previews: some View {
        HomePageView().environmentObject(HomePageViewModel())
    }
}
