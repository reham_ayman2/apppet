//
//  MedicationView.swift
//  Vet Questionnaire SwiftUI
//
//  Created by Oufaa on 30/06/2022.
//

import SwiftUI
import Focuser

struct MedicationView: View {
    @State var hasMedication: Bool = false
    @State var boundValue : String = ""
    @State var isShowingOptions: Bool = false
    @State var selected: String = "How many takes"
    @State var mediciations : [String] = []
    var SelectedMedications : [[String]] = []
    
    @State var mediciationsAnswers : [[String]] = []
    
    @EnvironmentObject var viewModel: QuestionnaireViewModel

    
    
    
    let coloms : [GridItem] = [
        GridItem( .flexible() , spacing: 5  , alignment: nil),
        GridItem( .flexible() , spacing: 0  , alignment: nil),
        GridItem( .flexible() , spacing: 0  , alignment: nil)
    ]
        
    
    
    
      let onAnswerChange: ([String]) -> ()
 
    
        init(_ onAnswerChange: @escaping ([String]) -> ()) {

        self.onAnswerChange = onAnswerChange

       
    }
    
    
    
    
    @State var counter  : Int = 0
    var midicanTakes = ["once daily", "twice daily", "three times daily", "monthly", "every three months", "other"]
    
    
    @FocusStateLegacy var focusedValue: String?
    
    
    
    var body: some View {
        VStack{
           
            
            if hasMedication{
            
                Text("Please list the medication(s) name take.")
                
                                
                HStack(){
                    RoundedBorderTextField(title: "Medication Name", hint: "Type Here", boundValue: $boundValue, focusedField: _focusedValue).frame( height: 100)
                    
                        .font(.custom("Poppins-Regular", size: 12))
                        .onChange(of: boundValue) { answer in
                        
                     onAnswerChange(["\(boundValue)"])
                    }
                    
                    HStack {
                        Text(selected)
                            .font(.custom("Poppins-Regular", size: 12))
                        
                        Spacer()
                        
                        Image(systemName: "chevron.down").foregroundColor(AppColors.secondaryColor)
                            .frame(maxHeight: 16, alignment: .center)
                   
                    }
                    .padding(12)
                    .overlay(RoundedRectangle(cornerRadius: 8)
                        .stroke(AppColors.secondaryColor, lineWidth: 2)
                    )
                    
                    .frame(maxWidth: .infinity, maxHeight: 100, alignment: .topLeading)
                    .padding(.top , 3)
                    .onTapGesture {
                        
                        
                        isShowingOptions.toggle()
                              
                    }
                    .overlay(
                        withAnimation {
                            ZStack {
                                if isShowingOptions {
                                    SimpleDropDown(items: midicanTakes) { answer in
                                        selected = answer
                                        isShowingOptions.toggle()
                                    }.padding()
                                       
                                    
                                }
                            }.transition(.slide)
                            
                        }.transition(.slide)
                            .padding(.top, -45)
                        
                    )
                    .animation(.linear)
                    .padding(.top,45)
                    
                    VStack{
                         Button {
                             if mediciations.count > 0 {
                                 mediciations.removeLast()
                                 self.mediciationsAnswers.removeLast()
                                 viewModel.CurrentMedicens = self.mediciationsAnswers
                                 
                                 
                                 print("⚠️⚠️⚠️⚠️mediciationsAnswers⚠️⚠️⚠️⚠️ ")
                                 print(mediciationsAnswers)
                                 print("⚠️⚠️⚠️⚠️viewModel.CurrentMedicens3 ⚠️⚠️⚠️⚠️")
                                 print(viewModel.CurrentMedicens)
                             }
                         } label: {
                             if mediciations.count > 0 {
                                 Image(systemName: "minus")
                                     .resizable()
                                         .aspectRatio(contentMode: .fit)
                                         .frame(width: 20, height: 20)
                                         .background(Color.white)
                                         .foregroundColor(AppColors.secondaryColor)
                                         .padding(4)
                                              .overlay(
                                                  RoundedRectangle(cornerRadius: 50)
                                                     .stroke(AppColors.secondaryColor, lineWidth: 1)
                                              )
                             }
                         }
                         
                         Button {
                             if self.selected == "How many takes" || self.boundValue == "" {
                                 showWarningAlert(title: "", message: "Please Complete your data")
                             } else {
                                 
                                 mediciations.append(self.boundValue)
                                 
                                 let array1 = [selected , boundValue ]
                                 print("👆🏻")
                                 print(array1)
                                 print("🤘🏻")
                                 mediciationsAnswers.append(array1)
                                 viewModel.CurrentMedicens = mediciationsAnswers
                                 
                                
                                 self.boundValue = ""
                               
                                 
     //                            mediciationsAnswers.append(self.selected)
     //                            mediciationsAnswers.append(self.boundValue)
                                 
                             }
                             
                          
                             print("✒️✒️✒️✒️✒️✒️✒️✒️✒️✒️")
                             print(mediciations)
                             print(mediciationsAnswers)
                                                 
                             
                             
                         } label: {
                             
                             Image(systemName: "plus").resizable()
                                 .aspectRatio(contentMode: .fit)
                                 .frame(width: 20, height: 20)
                                 .background(Color.white)
                                 .foregroundColor(AppColors.secondaryColor)
                                 .padding(4)
                                      .overlay(
                                          RoundedRectangle(cornerRadius: 50)
                                             .stroke(AppColors.secondaryColor, lineWidth: 1)
                                      )
                             
                         }
                    }.frame(width: 50, height: 50)

                    
                    
                    
                    
                }.frame(maxWidth: .infinity, maxHeight: 300, alignment: .center)
                    
                
      //MARK: -  MY MED LIST VIEW
                
                
                ScrollView {
                FlexibleView(
                       availableWidth: UIScreen.main.bounds.width, data: mediciations,
                       spacing: 15,
                       alignment: .leading
                     ) { item in
                       Text(verbatim: item)
                         .padding(8)
                         .background(
                           RoundedRectangle(cornerRadius: 8)
                             .fill(Color.gray.opacity(0.2))
                          )
                     }
                }


                
                
            } else {
                HStack{
                    Button {
                        hasMedication = true
                    } label: {
                        Text("Yes")
                            .frame(width: 100, height: 100)
                            .foregroundColor( hasMedication ?
                                              Color.white : AppColors.secondaryColor   )
                            .background( hasMedication ?
                                         AppColors.secondaryColor : Color.white  )
                            .cornerRadius(15)
                            .overlay(
                                hasMedication ?    RoundedRectangle(cornerRadius: 0)
                                    .stroke(Color.purple, lineWidth: 0) : RoundedRectangle(cornerRadius: 15)
                                    .stroke(Color.gray, lineWidth: 1)
                            )
                    }
                    .padding()
                    Button {
                        hasMedication = false
                    } label: {
                        Text("No")
                            .frame(width: 100 , height: 100 )
                            .foregroundColor( hasMedication ?
                                              AppColors.secondaryColor : Color.white  )
                            .background( hasMedication ?
                                         Color.white : AppColors.secondaryColor   )
                            .cornerRadius(15)
                            .overlay(
                                hasMedication ? RoundedRectangle(cornerRadius: 15)
                                    .stroke(Color.gray, lineWidth: 1) :
                                    RoundedRectangle(cornerRadius: 0)
                                    .stroke(Color.purple, lineWidth: 0)
                            )
                    }
                    
                }
            }
            
            
        }
    }
}

//struct MedicationView_Previews: PreviewProvider {
//    static var previews: some View {
//        MedicationView()
//    }
//}

