//
//  MultiLineInsertsView.swift
//  Vet Questionnaire SwiftUI
//
//  Created by Oufaa on 01/07/2022.
//

import SwiftUI
import Focuser
struct MultiLineInsertsView: View {
    
    init( _ onAnswerChange: @escaping ([String]) -> ()) {
        self.onAnswerChange = onAnswerChange
    }

    let onAnswerChange: ([String]) -> ()
 
    @State var selected2: String = ""
    @State var selected: String = ""
    @State var selected3: String = ""
    @FocusStateLegacy var selectedField: String? = ""

    var body: some View {
        
        VStack{
            Text("*Please note homemade if you are making (his/her) food.")
                .font(.custom("Poppins-Regular", size: 16))
            
            RoundedBorderTextField(title: "", hint: "e.g. dog food", boundValue: $selected, focusedField: _selectedField)
                .onChange(of: selected) { answer in
                    onAnswerChange( ["\(answer),\(selected2),\(selected3)"])
                }

            RoundedBorderTextField(title: "", hint: "e.g. homemade", boundValue: $selected2, focusedField: _selectedField, isLast: true)
                .onChange(of: selected2) { answer in
                    onAnswerChange( ["\(selected),\(answer),\(selected3)"])
                }

            RoundedBorderTextField(title: "", hint: "e.g. homemade", boundValue: $selected3, focusedField: _selectedField)
                .onChange(of: selected3) { answer in
                    onAnswerChange( ["\(selected),\(selected2),\(answer)"])
                }
        }.padding()
        
        
    }
}

struct MultiLineInsertsView_Previews: PreviewProvider {
    static var previews: some View {
        MultiLineInsertsView { _ in }
    }
}
