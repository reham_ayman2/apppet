//
//  AnswerView.swift
//  Vet Questionnaire SwiftUI
//
//  Created by NoobMaster69 on 12/03/2022.
//

import SwiftUI

struct AnswerView: View {

    let answers: [PresentationAnswer]
    let answerType: PresentationQuestion.AnswerType
    let image: UIImage?
    let specie: String
    var currentPetImage: UIImage?
    
    
    
    let med : [[String]]?
  
    
    
    
    
    @Binding var showImagePicker: Bool
    @Binding var showPickerSource: Bool
    @Binding var source: UIImagePickerController.SourceType
    
    
    
    
    let onAnswerChange: ([String]) -> ()
    
    let onImageSubmit: ((UIImage) -> ())?
   
    
    
    
    
    var body: some View {
        switch answerType {
        case .singleChoice:
            SingleSelectionAnswerView(image, answers, onAnswerChange)

        case .date:
            DatePickerAnswerView(onAnswerChange).frame( maxHeight: .infinity)
            
            
          
            
            // added
            
        case .multi:
            
           
            newFoodView()
                
                
                
            

        case .multiChoice:
            MultiSelectionAnswerView(answers, onAnswerChange)

        case .multiLineInserts:
            MultiLineInsertsView(onAnswerChange)

        case .number:
            InputAnswerView(keyboardType: .numberPad, onAnswerChange: onAnswerChange)

        case .bodyCondition:
            BodyConditionView(UIImage(named: "bodies"), answers, onAnswerChange)
            
        case .caloriesEat:
            CaloriesView(keyboardType: .numberPad, onAnswerChange: onAnswerChange, answers: answers)

        case .text:
            InputAnswerView(keyboardType: .default, onAnswerChange: onAnswerChange)

        case .oneOfTwo:
            OneOfTwoAnswerView(answers: answers, onAnswerChange: onAnswerChange)

        case .medicalIssue:
            MedicalConditionView(answers, onAnswerChange)
            
        case .medicationQ:
            MedicationView(onAnswerChange)
            
        case .multiCheckBoxInserts:
            MultiCheckBoxAnswerView(answers, onAnswerChange)
            
        case .gastrointestinal:
            GastrointestinalIssueView(answers, onAnswerChange)
            
        case .image:
            if let onImageSubmit = onImageSubmit {
                ImagePickerQuestionView(specie: specie,
                        onImageSubmit: onImageSubmit,
                        image:  currentPetImage,
                        showImagePicker: $showImagePicker,
                        showPickerSource: $showPickerSource,
                        source: $source)
                
                
                
            } else {
                EmptyView()
            }
            
        case .multiChoicesCheckBox:
            MultiCheckBoxAnswerView(answers, onAnswerChange)
        }
    }
}
