//
//  GastrointestinalIssueView.swift
//  Vet Questionnaire SwiftUI
//
//  Created by Oufaa on 04/07/2022.
//

import SwiftUI

struct GastrointestinalIssueView: View {
    @State var isShowingOptions: Bool = false
    @State var isShowingOptions2: Bool = false

    @State var hasDiscomfort: Bool = true
    
    init(_ answers: [PresentationAnswer], _ onAnswerChange: @escaping ([String]) -> ()) {
        self.answers = answers
        self.onAnswerChange = onAnswerChange
    }

    let onAnswerChange: ([String]) -> ()
    let answers: [PresentationAnswer]
    @State var selected2: String = "Answer"
    @State var selected: String = "Answer"
    var answersString: [String] {
        answers.map { answer in
            answer.content
        }
    }
    var body: some View {
        ScrollView {
        VStack{
            HStack{
                Button {
                    hasDiscomfort = true
                } label: {
                    Text("Yes")
                        .frame(width: 120, height: 120)
                        .foregroundColor( hasDiscomfort ?
                                          Color.white : AppColors.secondaryColor   )
                        .background( hasDiscomfort ?
                                     AppColors.secondaryColor : Color.white  )
                        .cornerRadius(15)
                        .overlay(
                            hasDiscomfort ?    RoundedRectangle(cornerRadius: 0)
                                .stroke(Color.purple, lineWidth: 0) : RoundedRectangle(cornerRadius: 15)
                                .stroke(Color.gray, lineWidth: 1)
                        )
                }.padding()
                Button {
                    hasDiscomfort = false
                } label: {
                    Text("No")
                        .frame(width: 120, height: 120)
                        .foregroundColor( hasDiscomfort ?
                                          AppColors.secondaryColor : Color.white  )
                        .background( hasDiscomfort ?
                                     Color.white : AppColors.secondaryColor   )
                        .cornerRadius(15)
                        .overlay(
                            hasDiscomfort ? RoundedRectangle(cornerRadius: 15)
                                .stroke(Color.gray, lineWidth: 1) :
                                RoundedRectangle(cornerRadius: 0)
                                .stroke(Color.purple, lineWidth: 0)
                        )
                }
            }
            
            if hasDiscomfort {
                VStack(alignment: .leading){
                    
                    Text("Select all areas that apply").fontWeight(.bold).padding()
                        
                    VStack(alignment: .leading){
                    Text("Vomiting").font(.subheadline).fontWeight(.light).padding(.leading)
    HStack(alignment: .top) {
        Text(selected)
                .onChange(of: selected) { answer in
                    
                    onAnswerChange([""])
                }
                .frame(alignment: .leading)
        Spacer()

        Image(systemName: "chevron.down").foregroundColor(AppColors.secondaryColor)
                .frame(maxHeight: 16, alignment: .center)

        
        
    }
            .padding()
            .overlay(RoundedRectangle(cornerRadius: 8)
                    .stroke(AppColors.secondaryColor, lineWidth: 2)
            ).padding()
                            .padding(.top,-16)

            .frame(maxWidth: .infinity, maxHeight: 325, alignment: .topLeading)
            .onTapGesture {
                isShowingOptions.toggle()
            }
            .overlay(
                    withAnimation {
                        ZStack {
                            if isShowingOptions {
                                SimpleDropDown(items: answersString) { answer in
                                    onAnswerChange([""])
                                    isShowingOptions = false
                                    selected = answer
                                }.padding()
                                
                                   
                            }
                        }.transition(.slide)
                    }.transition(.slide)
            )
            .onChange(of: answers) { _ in
                selected = "Answer"
            }
            .animation(.linear)
                    }
                }
            VStack(alignment: .leading){
                    VStack(alignment: .leading){
                    Text("Diarrhea").font(.subheadline).fontWeight(.light).padding(.leading)
    HStack(alignment: .top) {
        Text(selected2)
                .onChange(of: selected2) { answer in
                    
                    onAnswerChange([""])
                }
                .frame(alignment: .leading)
        Spacer()

        Image(systemName: "chevron.down").foregroundColor(AppColors.secondaryColor)
                .frame(maxHeight: 16, alignment: .center)

        
        
    }
            .padding()
            .overlay(RoundedRectangle(cornerRadius: 8)
                    .stroke(AppColors.secondaryColor, lineWidth: 2)
            ).padding()
                            .padding(.top,-16)

            .frame(maxWidth: .infinity, maxHeight: 325, alignment: .topLeading)
            .onTapGesture {
                isShowingOptions2.toggle()
            }
            .overlay(
                    withAnimation {
                        ZStack {
                            if isShowingOptions2 {
                                SimpleDropDown(items: answersString) { answer in
                                    onAnswerChange([""])
                                    isShowingOptions2 = false
                                    selected2 = answer
                                }.padding()
                                .padding(.top,-16)
                                   
                            }
                        }.transition(.slide)
                    }.transition(.slide)
            )
            .onChange(of: answers) { _ in
                selected2 = "Answer"
            }
            .animation(.linear)
                    }
                }
                
        }
            
        }}
    }
}

struct GastrointestinalIssueView_Previews: PreviewProvider {
    static var previews: some View {
        GastrointestinalIssueView(StaticQuestions.injuriesRanges, {_ in })
    }
}
