//
// Created by NoobMaster69 on 01/04/2022.
//

import Foundation
import SwiftUI
import Focuser

struct InputAnswerView: View {

    let keyboardType: UIKeyboardType
    let onAnswerChange: ([String]) -> ()
    @State var answer: String = ""
    @FocusStateLegacy var focusedValue: String?

    var body: some View {
        
        if keyboardType == .numberPad {
            RoundedBorderTextField(title: "", hint: "Answer", boundValue: $answer, focusedField: _focusedValue, isLast: true,textLabelName: "")
                    .onChange(of: answer) { answer in
                        onAnswerChange([answer])
                    }
        }else{
            RoundedBorderTextField(title: "", hint: "Answer", boundValue: $answer, focusedField: _focusedValue, isLast: true)
                    .onChange(of: answer) { answer in
                        onAnswerChange([answer])
                    }
        }
      
        
    }
}

struct InputAnswerView_Previews: PreviewProvider {
    static var previews: some View {
        InputAnswerView(keyboardType: .numberPad, onAnswerChange: {_ in })
    }
}
