//
//  MedicalConditionView.swift
//  Vet Questionnaire SwiftUI
//
//  Created by Oufaa on 28/06/2022.
//

import SwiftUI

struct MedicalConditionView: View {
    @State var isShowingOptions: Bool = false
    
    
    
    init(_ answers: [PresentationAnswer], _ onAnswerChange: @escaping ([String]) -> ()) {
        self.answers = answers
        self.onAnswerChange = onAnswerChange
    }

    let onAnswerChange: ([String]) -> ()
    let answers: [PresentationAnswer]
    var answersString: [String] {
        answers.map { answer in
            answer.content
        }
    }
    @State var selected2: String = "Answer"
    @State var selected: String = "Answer"
    
    
    var body: some View {
        
        HStack(alignment: .top) {
            Text(selected)
                    .onChange(of: selected) { answer in
                        
                        onAnswerChange(["\(answer),\(selected2)"])
                    }
                    .frame(alignment: .leading)
            Spacer()

            Image(systemName: "chevron.down").foregroundColor(AppColors.secondaryColor)
                    .frame(maxHeight: 16, alignment: .center)

            
            
        }
                .padding()
                .overlay(RoundedRectangle(cornerRadius: 8)
                        .stroke(AppColors.secondaryColor, lineWidth: 2)
                ).padding(16)

                .frame(maxWidth: .infinity, maxHeight: 325, alignment: .topLeading)
                .onTapGesture {
                    isShowingOptions.toggle()
                }
                .overlay(
                    
                    
                        withAnimation {
                            ZStack {
                                
                                
                                if isShowingOptions {
                                    SimpleDropDown(items: answersString) { answer in
                                        onAnswerChange(["\(answer),\(selected2)"])
                                        isShowingOptions = false
                                        selected = answer
                                    }.padding()
                                }
                            } // .transition(.slide)
                        }
                        
                        
                        
                        
                        
                        // .transition(.slide)
                )
                .onChange(of: answers) { _ in
                    selected = "Answer"
                }
                .animation(.linear)
    }
}

struct MedicalConditionView_Previews: PreviewProvider {
    static var previews: some View {
        MedicalConditionView([PresentationAnswer(content: StaticQuestions.injuriesRanges[0].content),PresentationAnswer(content: StaticQuestions.injuriesRanges[1].content),PresentationAnswer(content: StaticQuestions.injuriesRanges[2].content),PresentationAnswer(content: StaticQuestions.injuriesRanges[3].content),
//             PresentationAnswer(content: StaticQuestions.injuriesRanges[3].content)
                             
                             
                             ]) { _ in}
    }
}
