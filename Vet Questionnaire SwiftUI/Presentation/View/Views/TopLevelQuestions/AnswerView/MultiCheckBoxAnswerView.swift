//
//  MultiCheckBoxAnswerView.swift
//  Vet Questionnaire SwiftUI
//
//  Created by User on 02/07/2022.
//
import Focuser
import SwiftUI

struct MultiCheckBoxAnswerView: View {
    let onAnswerChange: ([String]) -> ()
    var answers: [PresentationAnswer]
    @State var selectedAnswers: [String]
    @State var checked: [Bool]
    @State var hasDiscomfort: Bool = false
    @State var boundValue = ""
    @FocusStateLegacy var focus = ""
    init(_ answers: [PresentationAnswer], _ onAnswerChange: @escaping ([String]) -> ()) {
        self.answers = answers
        self.onAnswerChange = onAnswerChange
        selectedAnswers = []
        
        checked = Array(0...answers.count-1).map { i in
            false
        }
    }
    
    var body: some View {
        
        VStack{
            
            
            HStack{
                Button {
                    hasDiscomfort = true
                } label: {
                    Text("Yes")
                        .frame(width: 50, height: 50)
                        .foregroundColor( hasDiscomfort ?
                                          Color.white : AppColors.secondaryColor   )
                        .background( hasDiscomfort ?
                                     AppColors.secondaryColor : Color.white  )
                        .cornerRadius(15)
                        .overlay(
                            hasDiscomfort ?    RoundedRectangle(cornerRadius: 0)
                                .stroke(Color.purple, lineWidth: 0) : RoundedRectangle(cornerRadius: 15)
                                .stroke(Color.gray, lineWidth: 1)
                        )
                }
                .padding()
                Button {
                    hasDiscomfort = false
                } label: {
                    Text("No")
                        .frame(width: 50, height: 50)
                        .foregroundColor( hasDiscomfort ?
                                          AppColors.secondaryColor : Color.white  )
                        .background( hasDiscomfort ?
                                     Color.white : AppColors.secondaryColor   )
                        .cornerRadius(15)
                        .overlay(
                            hasDiscomfort ? RoundedRectangle(cornerRadius: 15)
                                .stroke(Color.gray, lineWidth: 1) :
                                RoundedRectangle(cornerRadius: 0)
                                .stroke(Color.purple, lineWidth: 0)
                        )
                }
                
            }
            
            
            if hasDiscomfort{
                VStack(alignment: .leading) {
                    ForEach(0..<answers.count, id: \.self) { idx in
                        HStack {
                            Text(answers[idx].content)
                                .font(.system(size: 20))
                            
                            
                            Spacer()
                            CheckBoxView(checked: $checked[idx])
                            
                        }.padding([.horizontal])
                            .padding(.top,4)
                        
                    }
                    
                    
                    if checked.last == true{
                        
                        
                        RoundedBorderTextField(title: "What is this other area?",
                                               hint: "e.g. dust allergies", boundValue: $boundValue, focusedField: _focus)
                    }
                    
                }
                
                
                
                .onChange(of: boundValue, perform: { newValue in
                    
                    onCheckedChange()
                })
                .onChange(of: checked, perform: { checked in
//                    if checked.last == false {
//       //                 boundValue = ""
//                        return
//                    }
                    onCheckedChange()
                })
                .onChange(of: answers, perform: { answers in
                    selectedAnswers = []
                    for i in 0..<checked.count {
                        checked[i] = false
                    }
                })
            }
        }
    }
    
    func onCheckedChange(){
        var newList: [String] = [String]()
        for i in 0..<checked.count where checked[i] == true {
            
            if i == checked.count-1 {
                if boundValue.isEmpty{
                    newList.append("Other")
                }else{
                    newList.append(boundValue)
                }
            }else{
                newList.append(answers[i].content)
                
            }
        }
        onAnswerChange(newList)
    }
    
}

struct MultiCheckBoxAnswerView_Previews: PreviewProvider {
    static var previews: some View {
        MultiSelectionAnswerView([PresentationAnswer(content: "Intervertebral disc disease (IVDD)"),
                                  PresentationAnswer(content: "Spinal arthritis"),
                                  PresentationAnswer(content: "Other")]) { answers in}
    }
}
