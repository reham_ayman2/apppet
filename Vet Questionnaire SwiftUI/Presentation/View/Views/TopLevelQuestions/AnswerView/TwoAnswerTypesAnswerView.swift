////
////  TwoAnswerTypesAnswerView.swift
////  Vet Questionnaire SwiftUI
////
////  Created by NoobMaster69 on 06/05/2022.
////
//
//import SwiftUI
//
//struct TwoAnswerTypesAnswerView: View {
//    let question : TwoAnswerTypesQuestion
//    let onPrimaryAnswerChange : ([String])->Void
//    let onSecondaryAnswerChange : ([String])->Void
//
//
//    @State var showImagePicker = false
//    @State var currentPetImage: UIImage? = nil
//    @State var src : UIImagePickerController.SourceType = .photoLibrary
//    var body: some View {
//        VStack{
////            AnswerView(answers: question.primaryAnswers, answerType: question.primaryType, image: nil, specie: "", currentPetImage: currentPetImage, showImagePicker: $showImagePicker, showPickerSource: $showImagePicker, source: $src,onAnswerChange: onPrimaryAnswerChange,onImageSubmit:nil)
////            AnswerView(answers: <#[PresentationAnswer]#>, image: <#UIImage?#>, specie: <#String#>, showImagePicker: <#Binding<Bool>#>, showPickerSource: <#Binding<Bool>#>, source: <#Binding<UIImagePickerController.SourceType>#>, onAnswerChange: <#([String]) -> ()#>, onImageSubmit: <#((UIImage) -> ())?#>)
////            Text(question.secondaryTitle)
////                .font(.custom("Poppins-SemiBold", size: 18)).frame(maxWidth: .infinity,alignment: .leading).padding(.horizontal)
//
//            AnswerView(answers: question.secondaryAnswers, answerType: question.secondaryType, image: nil, specie: "", currentPetImage: currentPetImage, showImagePicker: $showImagePicker, showPickerSource: $showImagePicker, source: $src,onAnswerChange: onSecondaryAnswerChange,onImageSubmit:nil)
//        }
//    }
//}
//
//struct TwoAnswerTypesAnswerView_Previews: PreviewProvider {
//    
//    static var previews: some View {
//        TwoAnswerTypesAnswerView(question: TwoAnswerTypesQuestion(primaryTitle: "Title", secondaryTitle: "Secondary", primaryAnswers: [], secondaryAnswers: [], primaryType: .singleChoice, secondaryType: .multiChoice)){ans in
//        } onSecondaryAnswerChange: { ans in
//
//        }
//    }
//}
