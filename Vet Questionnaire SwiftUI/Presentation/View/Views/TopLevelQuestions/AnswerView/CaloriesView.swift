//
//  CaloriesView.swift
//  Vet Questionnaire SwiftUI
//
//  Created by Oufaa on 22/06/2022.
//

import SwiftUI
import Focuser

struct CaloriesView: View {
    
    let keyboardType: UIKeyboardType
    let onAnswerChange: ([String]) -> ()
    @State var answer: String = ""
    let answers: [PresentationAnswer]
    @FocusStateLegacy var focusedValue: String?

    var body: some View {
//        [PresentationAnswer(content: ""), PresentationAnswer(content: "Kcal")]
        if keyboardType == .numberPad {
            RoundedBorderTextField(title: "", hint: "E.g 70", boundValue: $answer, focusedField: _focusedValue, isLast: true,textLabelName: answers[1].content)
                    .onChange(of: answer) { answer in
                        onAnswerChange([answer])
                    }
        }
      
        
    }
}

struct CaloriesView_Previews: PreviewProvider {
    static var previews: some View {
        CaloriesView(keyboardType: .numberPad, onAnswerChange: {_ in },answers: [PresentationAnswer(content: ""), PresentationAnswer(content: "Kcal")])
    }
}

