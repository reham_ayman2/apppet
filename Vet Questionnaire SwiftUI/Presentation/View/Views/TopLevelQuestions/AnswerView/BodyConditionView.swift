//
//  BodyConditionView.swift
//  Vet Questionnaire SwiftUI
//
//  Created by Oufaa on 22/06/2022.
//

import SwiftUI
import Focuser

struct BodyConditionView: View {
    
    
    
//    BCS 1
//
//    BCS 3
//    BCS 5
//    BCS 7
//    BCS 9

    
    let columns : [GridItem] = [GridItem(.flexible()),GridItem(.flexible())]
    @State var selectedItem: String = ""
    
    
    @State var arrayOfImages = [ "1" , "3" , "5" , "7"]
    @State var arrayOfDogsImages = ["BCS 1" , "BCS 3" , "BCS 5" , "BCS 7"]
    
    
    
    
    
    
    
    

    
    init(_ image: UIImage?, _ answers: [PresentationAnswer]
         , _ onAnswerChange: @escaping ([String]) -> ()) {
        self.answers = answers
        self.onAnswerChange = onAnswerChange
        self.image = image
        
    }
    @FocusStateLegacy var focusedValue: String?
    let onAnswerChange: ([String]) -> ()
    let answers: [PresentationAnswer]
    let image: UIImage?
    
    @State var answer: String = ""
    //    var answersString: [String] {
    //        answers.map { answer in
    //            answer.content
    //        }
    //    }
    var body: some View {
        ScrollView {
        VStack(alignment: .center) {
            Text("Body Condition Score (BCS) is a measure of how close your pet is to his ideal body weight and is measured on a nine-point scale. Use the images below to determine BCS.")
                .frame(maxWidth: .infinity, maxHeight: 130, alignment: .center)
                .multilineTextAlignment(.center)
                .padding(.bottom,4)
            Text("Which picture is closest to your pet?")
                .fontWeight(.bold)
                .padding([.bottom],4)
           
         
            LazyVGrid(columns: columns) {
                
                
                ForEach(0...3, id: \.self) { index in
                    // check if dog or cat
                    
                   
                    
             
                    
                    VStack {
                        
                        
                        
                        Image( UserDefaults.standard.bool(forKey: "ISCAT") ?  arrayOfImages[index] : arrayOfDogsImages[index] )
                            .resizable()
                            .frame(maxWidth: 90, maxHeight: 90)
                            .aspectRatio(contentMode: .fit)
                            .background(selectedItem == answers[index].content ? AppColors.secondaryColor : Color.white)
                            .foregroundColor(AppColors.secondaryColor)
                            .font(.largeTitle)
                            .shadow(radius: 16.0)
                            .background(Color.white)
                            .clipShape(RoundedRectangle(cornerRadius: 16))
                            .shadow(radius: 5)
                            .overlay(
                                RoundedRectangle(cornerRadius: 16)
                                    .stroke(selectedItem == answers[index].content ? AppColors.secondaryColor : AppColors.lightGrey,
                                            lineWidth: selectedItem == answers[index].content ? 5.0 : 2.0))
                    }
                    .padding(4).onTapGesture {
                        onAnswerChange([answers[index].content])
                        selectedItem = answers[index].content
                    }
                    
                }
            }.padding(4)
            VStack {
               
                Image (  UserDefaults.standard.bool(forKey: "ISCAT") ? "9" : "BCS 9" )
                    .resizable()
                    .frame(maxWidth: 90, maxHeight: 90)
                    .aspectRatio(contentMode: .fit)
                    .background(selectedItem == answers[4].content ? AppColors.secondaryColor : Color.white)
                    .foregroundColor(AppColors.secondaryColor)
                    .font(.largeTitle)
                    .shadow(radius: 16.0)
                    .background(Color.white)
                    .clipShape(RoundedRectangle(cornerRadius: 16))
                    .shadow(radius: 5)
                    .overlay(
                        RoundedRectangle(cornerRadius: 16)
                            .stroke(selectedItem == answers[4].content ? AppColors.secondaryColor : AppColors.lightGrey,
                                    lineWidth: selectedItem == answers[4].content ? 5.0 : 2.0))
            }
                .onTapGesture {
                onAnswerChange([answers[4].content])
                selectedItem = answers[4].content
                
            }
        }}.padding()
    }
}

struct BodyConditionView_Previews: PreviewProvider {
    static var previews: some View {
        BodyConditionView(UIImage(named: "bodies"), [PresentationAnswer.fromString("1"), PresentationAnswer.fromString("2"),PresentationAnswer.fromString("3"), PresentationAnswer.fromString("4"),PresentationAnswer.fromString("5")], {_ in })
    }
}
