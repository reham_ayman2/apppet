//
// Created by NoobMaster69 on 01/04/2022.
//

import Foundation
import SwiftUI

struct SingleSelectionAnswerView: View {
    @State  var isShowingOptions: Bool = false
    @State var isShowingOptions2: Bool = false

    init(_ image: UIImage?, _ answers: [PresentationAnswer], _ onAnswerChange: @escaping ([String]) -> ()) {
        self.answers = answers
        self.onAnswerChange = onAnswerChange
        self.image = image

    }

    let onAnswerChange: ([String]) -> ()
    let answers: [PresentationAnswer]

    let image: UIImage?
    var answersString: [String] {
        answers.map { answer in
            answer.content
        }
    }
    @State var selected2: String = "Answer"
    @State var selected: String = "Answer"
    @State var isMixed : Bool = false
    var body: some View {
      
        VStack {
            if let image = image {
                Image(uiImage: image)
                        .resizable()
                        .frame(maxWidth: .infinity)
                        .aspectRatio(contentMode: .fit)
                        .clipShape(RoundedRectangle(cornerRadius: 16))
                        .overlay(
                                RoundedRectangle(cornerRadius: 16)
                                        .stroke(AppColors.secondaryColor, lineWidth: 2)
                        )
                        .padding([.horizontal], 8)
                        .padding(.top, 10)


            }
           
                VStack{
            HStack(alignment: .top) {
                Text(selected)
                        .onChange(of: selected) { answer in
                            
                            onAnswerChange(["\(answer),\(selected2),\(isMixed)"])
                        }
                        .frame(alignment: .leading)
                Spacer()

                Image(systemName: "chevron.down").foregroundColor(AppColors.secondaryColor)
                        .frame(maxHeight: 16, alignment: .center)

                
                
            }
                    .padding()
                    .overlay(RoundedRectangle(cornerRadius: 8)
                            .stroke(AppColors.secondaryColor, lineWidth: 2)
                    ).padding(16)

                    .frame(maxWidth: .infinity, maxHeight: 325, alignment: .topLeading)
                    .onTapGesture {
                        isShowingOptions.toggle()
                    }
                    
                    .overlay(
                            withAnimation {
                                ZStack {
                                    if isShowingOptions {
                                        
                                        
                                        
                                        // go to simple drop down // add yours
                                        
                                        SimpleDropDown(items: answersString) { answer in
                                            onAnswerChange(["\(answer),\(selected2),\(isMixed)"])
                                            isShowingOptions = false
                                            selected = answer
                                        }.padding()
                                        
//                                        newDropDwomView(items: answersString ) { answer in
//                                            onAnswerChange(["\(answer),\(selected2),\(isMixed)"])
//                                                                                        isShowingOptions = false
//                                                                                        selected = answer
//
//                                        }
//
                                    }
                               } .transition(.slide)
                           } .transition(.slide)
                    )
                    .onChange(of: answers) { _ in
                        selected = "Answer"
                    }
                    .animation(.linear)
                }
            
            if(isMixed){
                VStack{
                HStack(alignment: .top) {
                    Text(selected2)
                            .onChange(of: selected2) { answer in
                                onAnswerChange(["\(selected),\(answer),\(isMixed)"])
                            }
                            .frame(alignment: .leading)
                    Spacer()

                    Image(systemName: "chevron.down").foregroundColor(AppColors.secondaryColor)
                            .frame(maxHeight: 16, alignment: .center)

                    
                    
                }
                        .padding()
                        .overlay(RoundedRectangle(cornerRadius: 8)
                                .stroke(AppColors.secondaryColor, lineWidth: 2)
                        ).padding(16)

                        .frame(maxWidth: .infinity, maxHeight: 325, alignment: .topLeading)
                        .onTapGesture {
                            isShowingOptions2.toggle()
                        }
                        .overlay(
                                withAnimation {
                                   ZStack {
                                        if isShowingOptions2 {
                                            SimpleDropDown(items: answersString) { answer in
                                                onAnswerChange(["\(selected),\(answer),\(isMixed)"])
                                                isShowingOptions2 = false
                                                selected2 = answer
                                            }.padding()
                                        }
                                    }.transition(.slide)
                                }.transition(.slide)

                        )
                        .onChange(of: answers) { _ in
                            selected2 = "Answer"
                        }
                        .animation(.linear)
                }
            }
            
            Spacer()
         
            HStack{
                Text("Mixed Breed")
                Spacer()
                CheckBoxView(checked: $isMixed).onChange(of: isMixed) { answer in
                    onAnswerChange(["\(selected),\(selected2),\(answer)"])
                }
            }.padding()
                .padding(.bottom, 40)
        }
        
//        .frame( maxHeight: 300)
    }
}

struct SingleSelectionAnswerView_Previews: PreviewProvider {
    static var previews: some View {
        SingleSelectionAnswerView(nil, [PresentationAnswer(content: "hello"),PresentationAnswer(content: "Selected"),PresentationAnswer(content: "Type")]) {_ in }
    }
}
