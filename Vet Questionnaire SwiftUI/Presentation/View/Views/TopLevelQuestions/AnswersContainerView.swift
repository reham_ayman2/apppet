//
//  AnswersContainerView.swift
//  Vet Questionnaire SwiftUI
//
//  Created by NoobMaster69 on 12/03/2022.
//

import SwiftUI

// MARK: Answer Container
struct AnswersContainerView: View {
    
    //    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    @ObservedObject var viewModel: QuestionnaireViewModel
    @State   var showingPopover = false
    @State   var currentAnswer: [String] = []
    @State   var currentSecondaryAnswer : [String] = []
    @Binding var questionsFilled: Bool
    @Binding var showImagePicker: Bool
    @Binding var showPickerSource: Bool
    @Binding var source: UIImagePickerController.SourceType
    @Binding var isLoading: Bool
    //MARK: -  LOADER VARS
    @State var  isPopUp = false
    @State var  rotate = false
    
    
    
    
    
    //    @Binding var nav: Bool
    var onQuestionsFilled: () -> () = {}
    var onBackClicked: (_ onClicked: Bool ) -> () = {onClicked in
        if onClicked {
            var click = false
        }
    }
    var body: some View {
      
        
      
        VStack(){
            
            
            
            AnswerView(answers: viewModel.currentQuestion.answers,
                       answerType: viewModel.currentQuestion.answerType,
                       image: viewModel.currentQuestion.image,
                       specie: viewModel.getCurrentSpecie(),
                       currentPetImage: viewModel.currentPetImage,
                       med: viewModel.currentQuestion.med ?? [] ,
                       
                       
                       
                       
                       showImagePicker: $showImagePicker,
                       showPickerSource: $showPickerSource,
                       source: $source,
                       
                       onAnswerChange: { answer in
                currentAnswer = answer
            }
                       
                       
                    
            , onImageSubmit: { image in viewModel.setCurrentPetImage(image) }
                       
            
            
            )
            
            .padding(.top , 16 )
            Spacer()
            
           
            // here to handel the bottoms
           
           
            
            HStack {
                
                if viewModel.getCurrentQuestionIdx() > 0{
                    Button(action: {
                        
                        print("getCurrentQuestionIdx() > 0 🤏🏻🤏🏻🤏🏻🤏🏻🤏🏻🤏🏻🤏🏻🤏🏻")
                        //currentAnswer = []
                        viewModel.fetchPrevQuestion()
                    }) {
                        Image("button-left")
                        
                    }
                }else {
                    Button(action: {
                        onBackClicked(true)
                        print("back clicked current question == 0  🤏🏻🤏🏻🤏🏻🤏🏻🤏🏻🤏🏻🤏🏻🤏🏻")
                        //                        self.presentationMode.wrappedValue.dismiss()
                    }){
                        Image("button-left")
                            .shadow(color: .gray, radius: 3 , x: 0, y: 0)
                    }
                    
                  
                }
               // Spacer(minLength: .infinity)
//                Spacer()
//                Spacer()
                Spacer(minLength: 100)
                Image("button-right")
                    .shadow(color: .gray, radius: 3 , x: 0, y: 0)
                    .onTapGesture {
                        if viewModel.isLastQuestion() && viewModel.allPetsFilled() {
                            questionsFilled = true
                            isLoading = true

                            
                            
                            print("👻👻👻👻👻👻👻👻👻👻👻👻👻👻👻👻👻👻👻👻👻👻👻👻👻👻👻👻👻👻👻")
                           
                            
                        
                            
                            onQuestionsFilled()
                            
                          
                            
                        }else if viewModel.isLastQuestion(){
                           isLoading = true
                            onQuestionsFilled()
                            
                            
                            
                            viewModel.restartQuestionnaire()
                            viewModel.Fooditems = []
                            viewModel.CurrentMedicens = []
                            viewModel.conditions = []
                        }
                        
                        else {
                           
                            
                            //MARK: - VALIDATION ON QUESTIONS
                            
                            if currentAnswer.isEmpty &&
                                viewModel.currentQuestion.questionType != .optional {
                               // showingPopover = true
                                showWarningAlert(title: "", message: "Please Complete Your Data First!")
                                
                                
                            } else if viewModel.currentQuestion.id == 0 && currentAnswer[0].count < 3  {
                                showWarningAlert(title: "", message: "The pet name must be at least 3 characters.")
                            }
                            else {
                                 
                                if currentAnswer.isEmpty {
                                    viewModel.submitQuestion(answerString: [""])
                                }else if viewModel.currentQuestion.answerType != PresentationQuestion.AnswerType.image {
                                    viewModel.submitQuestion(answerString: currentAnswer)
                                }
                                
                           
                                
                              
                                viewModel.fetchNextQuestion()
                                currentAnswer = []
                            }
                        }
                    }
            }.frame(maxWidth: .infinity, maxHeight: 80)
            
        }
        .padding(.bottom, 8)
        .padding(.horizontal)
        .alert(isPresented: $showingPopover) {
            Alert(
                title: Text("Answer cannot be empty"),
                message: Text("Please answer the question first")
            )
        }
        .frame(maxWidth: 400,maxHeight: .infinity )
        .background(RoundedCorners(color: .white, tl: 30, tr: 30))
        .clipped()
        .shadow(color: .gray, radius: 3 , x: 0, y: 0)
       .mask(Rectangle().padding(.top, -20))
       // .shadow(radius: 3)
        .ignoresSafeArea(.container, edges: [.vertical])
    
        //MARK: -  LOADER ISSSUES
        
        .overlay(
            ZStack {
                if isPopUp {
                    Color.primary.opacity(0.5)
                        .ignoresSafeArea()
                    
                    
                    animationViewLoader(ShowPopUp: $isPopUp, rotate: $rotate)
                }
            }
        
        
        )
    
    
    
    
    
}
    
    
}


struct AnswersContainerView_Previews: PreviewProvider {
    static var previews: some View {
        
        AnswersContainerView(viewModel: QuestionnaireViewModel(), questionsFilled: .constant(false), showImagePicker:  .constant(false), showPickerSource:  .constant(false), source:  .constant(UIImagePickerController.SourceType.camera), isLoading:  .constant(false))
        
    }
}
