//
//  QuestionView.swift
//  Vet Questionnaire SwiftUI
//
//  Created by NoobMaster69 on 04/03/2022.
//

import SwiftUI

// MARK: QuestionViewContainer
struct QuestionView: View {
    
    init(content questionContent: String, image: UIImage? = nil) {
        self.questionContent = questionContent
        self.questionImage = image
    }
    
    let questionContent: String
    let questionImage: UIImage?
    var body: some View {
        VStack(alignment: .leading) {
            if let questionImage = questionImage {
                Image(uiImage: questionImage)
                    .resizable()
                    .aspectRatio(contentMode: .fit)
                    .frame(maxWidth: .infinity)
                    .padding(.vertical,50)
                
            }
            
            Text(questionContent)
                .font(.custom("Poppins-SemiBold", size: 22))
                .bold()
                .multilineTextAlignment(.center)
                .frame(maxWidth: .infinity, alignment: .topLeading)
                .padding([.leading,.trailing])
            
        }
    }
}


struct QuestionView_Previews: PreviewProvider {
    static var previews: some View {
   
     QuestionView(content: " Body Condition Score   (Check Image Below) ?")
    }
}
