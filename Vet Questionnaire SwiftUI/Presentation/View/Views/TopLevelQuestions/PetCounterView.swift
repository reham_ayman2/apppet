//
//  PetCounterView.swift
//  Vet Questionnaire SwiftUI
//
//  Created by NoobMaster69 on 31/03/2022.
//

import SwiftUI

struct PetCounterView: View {
    let currentCatCount:Int?
    let totalCatCount: Int?
    let currentDogCount: Int?
    let totalDogCount: Int?
    var body: some View {
        HStack {
            
            if totalCatCount ?? 0 > 0{
               
                Image("Icon-awesome-cat")
                    .resizable()
                    .frame(width: 30, height: 30)
                Text("\(currentCatCount!)").font(.custom("Poppins-SemiBold", size: 17))
                Text("/\(totalCatCount! )  ")
                
            }

            if totalDogCount ?? 0 > 0 {
                if totalCatCount ?? 0 > 0{
                    Text("|")
                }
                Image("Icon-awesome-dog")
                    .resizable()
                    .frame(width: 30, height: 30)
                Text(" \(currentDogCount!)").font(.custom("Poppins-SemiBold", size: 17))
                Text("/\(totalDogCount! ) ")
            }
        }
    }
}

struct PetCounterView_Previews: PreviewProvider {
    static var previews: some View {
        PetCounterView(currentCatCount: 1, totalCatCount: 2, currentDogCount: 1, totalDogCount: 2)
    }
}
