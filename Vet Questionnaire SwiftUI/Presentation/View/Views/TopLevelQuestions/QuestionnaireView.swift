//
//  QuestionnaireView.swift
//  Vet Questionnaire SwiftUI
//
//  Created by NoobMaster69 on 04/03/2022.
//

import SwiftUI
import HalfModal
/*
    1- Error = true >> @PUblish
    2- Sheet View Error With Button Try
 */
struct QuestionnaireView: View {
    
    let isDialog: Bool
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    @EnvironmentObject var viewModel: QuestionnaireViewModel
    @State var questionsFilled: Bool = false
    @State var showPickerSource: Bool = false
    @State var showImagePicker: Bool = false
    @State var source: UIImagePickerController.SourceType = .photoLibrary
    @State var isLoading = false
    @State var destEnabled = false
    @State var errorOccurred = false
    //MARK: -  LOADER VARS
    @State var  isPopUp = false
    @State var  rotate = false
    
    
    
    
    
    
    var onQuestionsFilled: () -> () = {
    }

    var onQuestionsEndDest: AnyView {
        let onBoardingViewModel = OnBoardingViewModel(onBoardingSteps: OnBoardingSteps.secondOnBoardingStep)
        return AnyView(BottomTextOnBoardingView(viewModel: onBoardingViewModel, onEndDestination: AnyView(GetStartedView())))
    }
    
    var answers: [String] {
        viewModel.getAnswers().map { ans in
           
           ans.content
        }
    }
    
    init(isDialog: Bool, _ onQuestionsFilled: (() -> ())? = nil ) {
        self.isDialog = isDialog
        self.onQuestionsFilled = onQuestionsFilled ?? {
            print("NonFind")
        }

    }
    
    
    
    var sheet: some View {
        
     
            
        
        VStack {
            if (UIImagePickerController.isSourceTypeAvailable(.camera)) {
                Button(action: {
                    source = .camera
                    showImagePicker = true
                    showPickerSource = false
                    
                }) {
                    Text("Take a photo")
                        .bold()
                        .frame(maxWidth: .infinity, alignment: .center)
                        .foregroundColor(Color.red)
                        .padding([.leading, .top], 4)
                        .font(.custom("Poppins-Bold", size: 21))
                }
                Divider().padding(.horizontal)
            }
            Button(action: {
                source = .photoLibrary
                showImagePicker = true
                showPickerSource = false
                
            }) {
                Text("Choose from library")
                    .bold()
                    .frame(maxWidth: .infinity, alignment: .center)
                    .foregroundColor(Color.black)
                    .padding([.leading, .top], 4)
                    .font(.custom("Poppins-Bold", size: 21))
            }
            
            Divider().padding(.horizontal)
            
            Button(action: {
                showImagePicker = false
                showPickerSource = false
                //                viewModel.setCurrentPetImage(nil)
            }) {
                Text("Remove current Image")
                    .frame(maxWidth: .infinity, alignment: .center)
                    .foregroundColor(Color.black)
                    .padding([.leading, .top], 4)
                    .font(.custom("Poppins-Regular", size: 21))
            }
            
            Button(action: {
                showImagePicker = false
                showPickerSource = false
            }) {
                Text("Cancel")
                    .frame(maxWidth: .infinity, alignment: .center)
                    .foregroundColor(Color.black)
                    .padding([.leading, .top], 4)
                    .font(.custom("Poppins-Regular", size: 21))
            }
            Divider().padding(.horizontal)
            
        }
        
        
      
        .frame(alignment: .topLeading)
    }
    
    
    
    
    var body: some View {
        
        NavigationView {
            
//
            
            ZStack {
                NavigationLink("", destination: onQuestionsEndDest, isActive: $destEnabled)
                VStack(alignment: .leading) {

                    HStack {
                        Image("AppLogo")
                            .resizable()
                            .scaledToFit()
                            .frame(maxWidth: 115, alignment: .center)
                            .padding()
                        Spacer()
           
                        PetCounterView(currentCatCount: viewModel.currentCatCount, totalCatCount: viewModel.catsCount, currentDogCount: viewModel.currentDogCount, totalDogCount: viewModel.dogsCount)
                        
                        
                        
                    }
                    .padding(.top,10)
                  
                     
                    ZStack {
                        VStack() {
                            HStack{
                                
                                Text("Step \(viewModel.getCurrentQuestionIdx() + 1)")
                         Spacer()
                    if viewModel.currentQuestion.questionType == .optional{
                            Button {
                                if viewModel.isLastQuestion() && viewModel.allPetsFilled() {
                                   
                                    questionsFilled = true
                                    self.isPopUp = true
                                    onQuestionsFilled()
                                }else if viewModel.isLastQuestion(){
                                    
                                    print("is last question only 👻👻👻")
                                    self.isPopUp = true
                                    onQuestionsFilled()
                                    
                                    
                                    
                                }else{
                                    viewModel.submitQuestion(answerString: [""])
                                    viewModel.fetchNextQuestion()
                                }
                                

                            } label: {
                                Text("Skip").foregroundColor(Color.gray)
                            }
                                }
                            }
                            ProgressView("", value: Double(viewModel.getCurrentQuestionIdx()), total: Double(viewModel.getQuestionsCount() - 1))
                        }
                        .padding()
                    
                    }


                    QuestionView(content: viewModel.currentQuestion.content)
                        .padding(.vertical,10)
                       // .font(.custom("Poppins-SemiBold", size: 14))
                    
                 
                    
                    AnswersContainerView(viewModel: viewModel, questionsFilled: $questionsFilled, showImagePicker: $showImagePicker, showPickerSource: $showPickerSource, source: $source, isLoading: $isPopUp)
                        {
                            
                            
                        
                        viewModel.submitPet { response in
                        
                            DispatchQueue.main.asyncAfter(wallDeadline: .now() + 3) {
                                if response.isSuccessful() {
                                    self.isPopUp = false
                                    print(viewModel.currentPetCount)
                                    
                                    viewModel.getNextPet()
                                    onQuestionsFilled()
                                    viewModel.restartQuestionnaire()
                                    if viewModel.allPetsFilled() {
                                        destEnabled = true
                                        
                                        print("✅✅✅✅✅✅✅✅ done .. ")
                                        print(response)
                                    }
                                }else {
                                    self.isPopUp = false
                                    print("❌❌❌❌❌❌❌❌❌❌❌❌ \(errorOccurred.description)")
                                    
                                    showErrorAlert(title: "", message: "Poor Connection Try Connect Again .. ")
                                    print(viewModel.currentPetCount)
                                    print("Not Sucessful Add")
                                    
                                    
                                    
                                    
                                    // added for edit
                                    
                                    viewModel.restartQuestionnaire()
                                    
                                    
                                    // -----
                                  //  errorOccurred = true
                                    
                                    
                                }
                  
//                                isLoading = false
                            }
                        }
                        }
                
                
                onBackClicked: { onClicked in
                        if onClicked {
                            self.presentationMode.wrappedValue.dismiss()
                        }
                    }.padding([.top, .bottom], 10)

                }
                .overlay(LoadingView(isLoading: $isLoading))
//                .alert(isPresented: $errorOccurred) {
//                    Alert(
//                        title: Text("Poor Connection"),
//                        message: Text("Try Connect Again"),
//
//
//                        dismissButton: .default(Text("Error"), action: {
                            
                          
//                            viewModel.submitPet {resposne in
//                                if resposne.isSuccessful() {
//                                    self.isPopUp = false
//                                }else {
//                                    errorOccurred = true
//                                }
//                                debugPrint(resposne)
//                            }
//
//                            viewModel.submitPet { response in
//                                DispatchQueue.main.asyncAfter(wallDeadline: .now() + 2) {
//                                    if response.isSuccessful() {
//                                        isLoading = false
//                                        print(viewModel.currentPetCount)
//                                        print("Sucessful Add")
//                                        viewModel.getNextPet()
//                                        onQuestionsFilled()
//                                        viewModel.restartQuestionnaire()
//                                        if viewModel.allPetsFilled() {
//                                            destEnabled = true
//                                        }
//                                    }else {
//                                        print(viewModel.currentPetCount)
//                                        print("Not Sucessful Add")
//                                        errorOccurred = true
//                                    }
//
//    //                                isLoading = false
//                                }
//                            }
//                        })
//                    )
//                }
                .frame(maxWidth: .infinity, alignment: .leading)
                .navigationBarBackButtonHidden(true)
                .navigationBarHidden(true)
                
                if showPickerSource {
                    HalfModalView(content: AnyView(sheet), header: nil, isPresented: $showPickerSource
                    )
                }

            }
            
        
        .padding(.leading , 0 )
              .padding(.trailing , 0 )
        }
        .navigationBarBackButtonHidden(true)
        .navigationBarHidden(true)
        
        
        
        //MARK: -  LOADER ISSSUES
        
        .overlay(
            ZStack {
                if isPopUp {
                    Color.primary.opacity(0.5)
                        .ignoresSafeArea()
                    
                    
                    animationViewLoader(ShowPopUp: $isPopUp, rotate: $rotate)
                }
            }
        
        
        )
        
        
        
        
    }
}

struct QuestionnaireView_Previews: PreviewProvider {
    static var previews: some View {
   
        QuestionnaireView(isDialog: false).environmentObject(QuestionnaireViewModel())
        //        QuestionnaireView(isDialog: false)
    }
}


//struct QuestionView_Previews: PreviewProvider {
//    static var previews: some View {
//        Group {
//            let q = StaticQuestions.initialQuestions(petName: "")[0]
//            QuestionView(content: q.content, image: q.image)
////            QuestionView(
//        }
//    }
//}



