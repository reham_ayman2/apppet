//
//  QuestionnaireOutroView.swift
//  Vet Questionnaire SwiftUI
//
//  Created by NoobMaster69 on 07/03/2022.
//

import SwiftUI

struct QuestionnaireOutroView: View {
    let items :[String]
    let onStartOverClickListener: ()->()
    var body: some View {
        VStack{
            ScrollView{
                VStack(alignment: .leading){
                    Text("You've finished the questionnaire")
                        .font(.system(size: 32.0))
                        .bold()
                        .padding()
                    
                    Text("Those are your answers: ")
                        .font(.system(size: 28.0))
                        .padding()
                    
                    ForEach(items,id: \.self){item in
                        Text(item)
                            .font(.system(size: 21.0))
                            .padding()
                    }
                }
            }
            HStack(alignment:.center){
                Button("Start Over",action:onStartOverClickListener)
                    .padding()
                    .background(Color.green)
                    .foregroundColor(.white)
                    .clipShape(RoundedRectangle(cornerRadius: 16.0))
                    .font(.system(size: 21.0))
                    .padding()
            }.frame(
                minWidth: .some(16.0),
                maxWidth: .infinity,
                minHeight: 0,
                maxHeight: 0,
                alignment: .center
            ).padding()
            
        }.padding()
    }
}

struct QuestionnaireOutroView_Previews: PreviewProvider {
    static var previews: some View {
        QuestionnaireOutroView(items:["AAAAAAAA","BBBBBBBB","CCCCCCC"]){
            
        }
    }
}
