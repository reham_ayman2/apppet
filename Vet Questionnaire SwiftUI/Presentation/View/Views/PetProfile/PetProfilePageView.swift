//
//  PetProfilePageView.swift
//  Vet Questionnaire SwiftUI
//
//  Created by NoobMaster69 on 04/05/2022.
//

import SwiftUI


struct PetProfilePageView: View {
    @State var showingEditProfile = false
    @State var addingMedicalConditions = false
    @State var selectedTab = PetProfileTabsEnum.overview.rawValue
    @EnvironmentObject var viewModel: PetProfilePageViewModel

    var body: some View {
        ScrollView {
            VStack {
                ZStack {
                    if let petImage = viewModel.displayedPet.imageUrl {
                        if #available(iOS 15.0, *) {
                            AsyncImage(url: URL(string: petImage)) { image in
                                image.resizable()
                                    .scaledToFit()
                                    
                            } placeholder: {
                                ProgressView()
                            }

                                
                        } else {
                            FacilitiesRemoteImage(urString: petImage)
                        }
                    } else {
                        Image(viewModel.displayedPet.species?.lowercased() == "cat" ? "cat" : "dog")
                                .resizable()
                                .scaledToFit()
                                .frame(minWidth: 30, maxWidth: 250, minHeight: 30, maxHeight: 250, alignment: .center)
                    }
                }
                        .frame(maxWidth: .infinity, maxHeight: 350)
                        .padding()
                        .ignoresSafeArea()

                VStack {
                    Text(viewModel.displayedPet.PetName ?? "")
                            .font(.custom("Poppins-SemiBold", size: 32))
                            .frame(maxWidth: .infinity, alignment: .center)
                            .padding(.top)

                    Text("\(viewModel.displayedPet.years ?? 0) Years \(viewModel.displayedPet.months ?? 0) Months")
                            .font(.custom("Poppins-Regular", size: 16))
                            .frame(maxWidth: .infinity, alignment: .center)


                    NavigationLink("", destination: UpdatePetProfileView().environmentObject(UpdatePetProfileViewModel(petId: viewModel.displayedPet.id)), isActive: $showingEditProfile)
                    Button("Edit Profile") {
                        showingEditProfile = true
                    }
                            .foregroundColor(.white)
                            .padding(.vertical, 12)
                            .padding(.horizontal, 40)
                            .background(Color.black)
                            .clipShape(RoundedRectangle(cornerRadius: 18))
                            .padding(.vertical, 16)

                    Text("Your Pet’s Insights")
                            .font(.custom("Poppins-Bold", size: 24))
                            .frame(maxWidth: .infinity, alignment: .leading)
                            .padding(.horizontal)

                    VStack {
                        HStack {
                            Image("Icon-tennisball")

                                    .frame(width: 40, height: 40, alignment: .leading)
                            VStack {
                                Text("Activity Level")
                                        .font(.custom("Poppins-Regular", size: 12))
                                        .frame(maxWidth: .infinity, alignment: .leading)
                                Text(viewModel.currentPetActivityInsights)
                                        .font(.custom("Poppins-SemiBold", size: 16))
                                        .frame(maxWidth: .infinity, alignment: .leading)

                            }

                        }
                                .frame(maxWidth: .infinity, alignment: .leading)
                                .padding()


                    }
                            .background(Color.white)
                            .clipShape(RoundedRectangle(cornerRadius: 16))
                            .padding(.horizontal)


                    VStack {
                        HStack {
                            Image("noun-dog-food")

                                    .frame(width: 40, height: 40, alignment: .leading)
                            VStack {
                                Text("Amount to Feed")
                                        .font(.custom("Poppins-Regular", size: 12))
                                        .frame(maxWidth: .infinity, alignment: .leading)
                                Text(viewModel.currentPetFoodInsights)
                                        .font(.custom("Poppins-SemiBold", size: 16))
                                        .frame(maxWidth: .infinity, alignment: .leading)

                            }

                        }
                                .frame(maxWidth: .infinity, alignment: .leading)
                                .padding()


                    }
                            .background(Color.white)
                            .clipShape(RoundedRectangle(cornerRadius: 16))

                            .padding()
                    PetProfileTabView(selectedTab: $selectedTab)
                    if selectedTab == PetProfileTabsEnum.overview.rawValue {
                        VStack {
                            HStack {
                                VStack {
                                    HStack {
                                        Image(viewModel.displayedPet.species?.lowercased() == "cat" ? "Icon-awesome-cat" : "Icon-awesome-dog")
                                                .renderingMode(.template)
                                                .foregroundColor(AppColors.primaryColor)
                                        Text("Breed")
                                                .font(.custom("Poppins-Regular", size: 12))
                                                .foregroundColor(.gray)
                                    }
                                    Text(viewModel.displayedPet.breed ?? "")
                                            .font(.custom("Poppins-Bold", size: 18))
                                }
                                Divider().padding()
                                VStack {
                                    HStack {
                                        Image("weight")
                                                .renderingMode(.template)
                                                .foregroundColor(AppColors.primaryColor)
                                        Text("Weight")
                                                .font(.custom("Poppins-Regular", size: 12))
                                                .foregroundColor(.gray)
                                    }
                                    Text("\(viewModel.displayedPet.weight ?? 0)")
                                            .font(.custom("Poppins-Bold", size: 18))
                                }
                                Divider().padding()
                                VStack {
                                    HStack {
                                        Image(viewModel.displayedPet.gender?.lowercased() == "male" ? "male" : "female")
                                                .resizable()
                                                .renderingMode(.template)
                                                .scaledToFit()
                                                .foregroundColor(AppColors.primaryColor)
                                                .frame(maxWidth: 18, maxHeight: 18)
                                        Text("Gender")
                                                .font(.custom("Poppins-Regular", size: 12))
                                                .foregroundColor(.gray)
                                    }
                                    Text(viewModel.displayedPet.gender ?? "Male")
                                            .font(.custom("Poppins-Bold", size: 18))
                                }

                            }
                                    .padding()
                                    .background(Color.white)
                                    .clipShape(RoundedRectangle(cornerRadius: 16))
                                    .padding()
                            Text("Your Pet’s Info")
                                    .font(.custom("Poppins-Bold", size: 24))
                                    .frame(maxWidth: .infinity, alignment: .leading)
                                    .padding(.horizontal)
                            HStack {
                                Image(systemName: "pawprint.fill")
                                        .resizable()
                                        .renderingMode(.template)
                                        .scaledToFit()
                                        .foregroundColor(AppColors.primaryColor)
                                        .frame(maxWidth: 32, maxHeight: 32)

                                VStack {
                                    Text("Body Condition Score")
                                            .font(.custom("Poppins-Regular", size: 12))
                                            .foregroundColor(.gray)
                                            .frame(maxWidth: .infinity, alignment: .leading)
                                    Text("____")
                                            .font(.custom("Poppins-Regular", size: 12))
                                            .foregroundColor(.gray)
                                            .frame(maxWidth: .infinity, alignment: .leading)
                                }
                                        .padding(.horizontal)

                                Divider().padding(.vertical)

                                VStack {
                                    Text("Result")
                                            .font(.custom("Poppins-Regular", size: 12))
                                            .foregroundColor(.gray)
                                            .frame(alignment: .leading)

                                    Text("____")
                                            .font(.custom("Poppins-Regular", size: 12))
                                            .foregroundColor(.gray)
                                            .frame(alignment: .leading)


                                }
                                        .padding(.horizontal)


                            }
                                    .frame(maxWidth: .infinity, alignment: .leading)
                                    .padding()
                                    .background(Color.white)
                                    .clipShape(RoundedRectangle(cornerRadius: 16))
                                    .padding()
                            HStack {
                                Image("noun-dog-food")
                                        .resizable()
                                        .renderingMode(.template)
                                        .scaledToFit()
                                        .foregroundColor(AppColors.primaryColor)
                                        .frame(maxWidth: 32, maxHeight: 32)
                                        .frame(maxHeight: .infinity, alignment: .top)

                                VStack {
                                    HStack {
                                        VStack {
                                            Text("Amount of Food")
                                                    .font(.custom("Poppins-Regular", size: 12))
                                                    .foregroundColor(.gray)
                                                    .frame(maxWidth: .infinity, alignment: .leading)
                                            Text("900 kcal")
                                                    .font(.custom("Poppins-Bold", size: 16))
                                                    .frame(maxWidth: .infinity, alignment: .leading)
                                        }
                                                .padding(.horizontal)


                                        VStack {
                                            Text("Fed Per Day")
                                                    .font(.custom("Poppins-Regular", size: 12))
                                                    .foregroundColor(.gray)
                                                    .frame(alignment: .leading)

                                            Text("3 times")
                                                    .font(.custom("Poppins-Bold", size: 16))
                                                    .frame(alignment: .leading)


                                        }
                                                .padding(.horizontal)
                                    }

                                    VStack {
                                        Text("Commercial Brand")
                                                .font(.custom("Poppins-Regular", size: 12))
                                                .foregroundColor(.gray)
                                                .frame(alignment: .leading)
                                                .frame(maxWidth: .infinity, alignment: .leading)


                                        Text("Blue Buffalo")
                                                .font(.custom("Poppins-Bold", size: 16))
                                                .frame(maxWidth: .infinity, alignment: .leading)


                                    }
                                            .frame(maxWidth: .infinity, alignment: .leading)
                                            .padding(.horizontal)
                                            .padding(.vertical, 4)

                                    VStack {
                                        Text("Product of Commercial Brand")
                                                .font(.custom("Poppins-Regular", size: 12))
                                                .foregroundColor(.gray)
                                                .frame(maxWidth: .infinity, alignment: .leading)

                                        Text("Blue Buffalo")
                                                .font(.custom("Poppins-Bold", size: 16))
                                                .frame(maxWidth: .infinity, alignment: .leading)


                                    }
                                            .frame(maxWidth: .infinity, alignment: .leading)
                                            .padding(.horizontal)
                                            .padding(.vertical, 4)


                                    VStack {
                                        Text("Calories in Pet’s Food")
                                                .font(.custom("Poppins-Regular", size: 12))
                                                .foregroundColor(.gray)
                                                .frame(maxWidth: .infinity, alignment: .leading)

                                        Text("300 kcal per kg")
                                                .font(.custom("Poppins-Bold", size: 16))
                                                .frame(maxWidth: .infinity, alignment: .leading)


                                    }
                                            .frame(maxWidth: .infinity, alignment: .leading)
                                            .padding(.horizontal)
                                            .padding(.vertical, 4)


                                    VStack {
                                        Text("Can/bag/cup/pouch be given per meal")
                                                .font(.custom("Poppins-Regular", size: 12))
                                                .foregroundColor(.gray)
                                                .frame(maxWidth: .infinity, alignment: .leading)

                                        Text("5 times")
                                                .font(.custom("Poppins-Bold", size: 16))
                                                .frame(maxWidth: .infinity, alignment: .leading)


                                    }
                                            .frame(maxWidth: .infinity, alignment: .leading)
                                            .padding(.horizontal)
                                            .padding(.vertical, 4)


                                }


                            }
                                    .frame(maxWidth: .infinity, alignment: .topLeading)
                                    .padding()
                                    .background(Color.white)
                                    .clipShape(RoundedRectangle(cornerRadius: 16))
                                    .padding()

                            HStack {
                                Image("Icon-tennisball")
                                        .resizable()
                                        .renderingMode(.template)
                                        .scaledToFit()
                                        .foregroundColor(AppColors.primaryColor)
                                        .frame(maxWidth: 32, maxHeight: 32)

                                VStack {
                                    Text("Number of Exercise")
                                            .font(.custom("Poppins-Regular", size: 12))
                                            .foregroundColor(.gray)
                                            .frame(maxWidth: .infinity, alignment: .leading)
                                    Text("____")
                                            .font(.custom("Poppins-Regular", size: 12))
                                            .foregroundColor(.gray)
                                            .frame(maxWidth: .infinity, alignment: .leading)
                                }
                                        .padding(.horizontal)

                                Divider().padding(.vertical)

                                VStack {
                                    Text("Amount of Time")
                                            .font(.custom("Poppins-Regular", size: 12))
                                            .foregroundColor(.gray)
                                            .frame(alignment: .leading)

                                    Text("____")
                                            .font(.custom("Poppins-Regular", size: 12))
                                            .foregroundColor(.gray)
                                            .frame(alignment: .leading)


                                }
                                        .padding(.horizontal)


                            }
                                    .frame(maxWidth: .infinity, alignment: .leading)
                                    .padding()
                                    .background(Color.white)
                                    .clipShape(RoundedRectangle(cornerRadius: 16))
                                    .padding()

                        }
                                .padding(.bottom)
                    } else {
                        VStack {
                            Text("Your Pet’s Health")
                                    .font(.custom("Poppins-Bold", size: 24))
                                    .frame(maxWidth: .infinity, alignment: .leading)
                                    .padding(.horizontal)

                            AddButtonFullWidthView().padding().onTapGesture {
                                addingMedicalConditions = true
                            }
                        }
                    }

                }
                        .frame(maxWidth: .infinity, maxHeight: .infinity, alignment: .topLeading)
                        .background(RoundedCorners(color: AppColors.lightBlue, tl: 32, tr: 32, bl: 32, br: 32))
                        .ignoresSafeArea()

            }
                    .frame(maxWidth: .infinity, maxHeight: .infinity, alignment: .topLeading)
                    .sheet(isPresented: $addingMedicalConditions) {
//                        AddMedicalConditionView(showingDialog: $addingMedicalConditions)
//                                .environmentObject(
//                                        QuestionnaireViewModel(questionnaire: PresentationQuestionnaire(questionsList: StaticQuestions.medicalQuestions(petName: viewModel.displayedPet.name ?? "")))
//                                )
                    }
        }
                .frame(maxWidth: .infinity, maxHeight: .infinity, alignment: .topLeading)
                .ignoresSafeArea()
    }
}

struct PetProfilePageView_Previews: PreviewProvider {
    static var previews: some View {
        PetProfilePageView().environmentObject(PetProfilePageViewModel(petId: 5))
        
    }
}
