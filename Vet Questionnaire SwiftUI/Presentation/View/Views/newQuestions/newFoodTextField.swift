//
//  newFoodTextField.swift
//  Vet Questionnaire SwiftUI
//
//  Created by Reham Ayman on 24/08/2022.
//

import SwiftUI
import Focuser



struct newFoodTextField: View {
    
    let title : String
    @FocusStateLegacy var focusedField: String?
    @State var borderColor = Color.gray
    @State var isLast : Bool
    let hint : String
   @State var boundValue : String
    var textLabelName: String?
    
    var focusedValue : String {
        if isLast {
            return  "LAST"
        }
        else {
            return  title
        }
    }
    init(title:String,hint:String,
         
         boundValue: String ,
         focusedField : FocusStateLegacy<String>,isLast:Bool = false,textLabelName:String?){
        self.title = title
        self.hint = hint
     
        self.boundValue = boundValue
        self._focusedField = focusedField
        self.isLast = isLast
        self.textLabelName = textLabelName
    }
    

    var body: some View {
        
       
        
        VStack{
            Text(title)
                .bold()
                .multilineTextAlignment(.leading)
                .foregroundColor(borderColor)
                .frame(maxWidth:.infinity,alignment: .leading)
                .padding(4)
            HStack{
             
                TextField(hint,text: $boundValue)
                        .focusedLegacy($focusedField, equals: focusedValue)
             
                
                if let textLabelName = textLabelName {
                    Text(textLabelName)
                        .multilineTextAlignment(.trailing)
                }
                   
            }   .padding(12)
                .overlay(
                    RoundedRectangle(cornerRadius: 8)
                        .stroke(borderColor, lineWidth: 2)
                ).disableAutocorrection(true)
        }
            .padding(.top,4)
            .onChange(of: focusedField, perform: {focusedField in
                borderColor = focusedField == focusedValue ? AppColors.secondaryColor : Color.gray
            })


    }
}


