//
//  newFoodView.swift
//  Vet Questionnaire SwiftUI
//
//  Created by Reham Ayman on 22/08/2022.
//

import SwiftUI
import Focuser


struct newFoodView: View {
    
    @State private var texts: [String] = []
    @EnvironmentObject var viewModel: QuestionnaireViewModel
    
    @State var foodName : String = ""
    @State var cal : String = ""
    @State var cup : String = "cup"
    @State var manyTiems : String = ""
    @State var foodNum = 1
    @State var isShowingOptions: Bool = false
    @State var checked : Bool = false
// storage for results
    @State var foods = [
        NewFooditemModel( foodName: "", calPerUnit:  "" , unit: "", fedTiems: "" )
        
    ]
    
    @State var SELECTEDFOOD = []
    
    var foodUnits = ["cup" , "can" , "ounce" ]
    @FocusStateLegacy var focusedField: String? = ""

    
   
    
    var body: some View {
      
            
        ScrollView {
            VStack ( alignment: .leading ) {
            
        Text("*Please note homemade if you are making (his/her) food.")
            .font(.custom("Poppins-Regular", size: 12))
            .foregroundColor(Color.gray)
        
            
            
        
                
                VStack ( alignment: .leading ){
                    Text ("Food #\($foodNum.projectedValue.wrappedValue)")
                        .font(.custom("Poppins-Bold", size: 15))
                    
      
                    
              RoundedBorderTextField(title: "Name of Food", hint: "eg.brands/name of food", boundValue: $foodName , focusedField: _focusedField)
                        .font(.custom("Poppins-SemiBold", size: 14))
                        
                    HStack(alignment: .bottom) {
                
                RoundedBorderTextField(title: "Calories Per Units", hint: "eg. 70", boundValue: $cal, focusedField: _focusedField)
                        .font(.custom("Poppins-SemiBold", size: 14))
                
                    // herer cup edits
                    
                    
                    

//                        RoundedBorderTextField(title: " ", hint: "Cup", boundValue: $cup, focusedField: _focusedField)
//                                .font(.custom("Poppins-SemiBold", size: 14))
//
//                                .frame(width: 100 )
                    
                    HStack {
                    
                    Text(cup)
                        .font(.custom("Poppins-Regular", size: 12))
                    
                    Spacer()
                    
                    Image(systemName: "chevron.down").foregroundColor(AppColors.secondaryColor)
                        .frame(maxHeight: 16, alignment: .center)
                    
                    
                    }
                    .padding(14)
                    .overlay(RoundedRectangle(cornerRadius: 8)
                        .stroke(AppColors.secondaryColor, lineWidth: 2)
                    )
                    
                    .frame( maxHeight: 100, alignment: .topLeading)
                    .frame(width: 110)
                    .padding(.top , 3)
                    .onTapGesture {
                        
                        
                        isShowingOptions.toggle()
                              
                    }.overlay(
                        withAnimation {
                            ZStack {
                                if isShowingOptions {
                                    SimpleDropDown(items: foodUnits) { answer in
                                        cup = answer
                                        isShowingOptions.toggle()
                                    }.padding()
                                       
                                    
                                }
                            }.transition(.slide)
                            
                        }.transition(.slide)
                            .padding(.bottom, -45)
                        
                    )
                    .animation(.linear)
                    .padding(.top,45)
                    
                       
                        
                        
              
                    
                    
                }
                    
                    
                    
                    // -----
                
//                Text( "How manyTiems fed daily ? ")
//                    .font(.custom("Poppins-SemiBold", size: 16))
                    if checked {
                        
                      
                        
                        
                    } else {
                    
                    
            RoundedBorderTextField(title: "How manyTiems fed daily ?", hint: "eg. 5", boundValue: $manyTiems, focusedField: _focusedField)
                        .font(.custom("Poppins-SemiBold", size: 14))
                        
                    
                    }
                
                
                
            
        } .padding()
            
           
            
            
        
       //  .frame( maxWidth: .infinity )
         //.frame( height: 300)
               
           
                HStack {
                    Text("I free feed my pet.")
                        .font(.custom("Poppins-Regular", size: 15))
                    
                    Spacer ()
                    if checked {
                        Image(systemName:"checkmark.square.fill")
                            .frame(width: 40, height: 40)
                            .foregroundColor(AppColors.secondaryColor)
                            .font(.custom("Poppins-Regular",size: 24))
                        
                    } else {
                        Image( "Rectangle 7601" )
                            .frame(width: 40, height: 40)
                    }
                    
//                        .foregroundColor(AppColors.secondaryColor)
//                        .onTapGesture { self.checked.toggle() }
//                        .font(.custom("Poppins-Regular",size: 24))
//
//
//
//
                } .padding(.leading)
                    .onTapGesture {
                        withAnimation {
                            checked.toggle()
                        }
                       
                    }
            HStack  {
                
                Image(systemName: "plus")
                    .foregroundColor(.green)
//            Button {
//
//
//
//
//
//            } label: {
                Text("Add Food")
                    .font(.custom("Poppins-SemiBold", size: 16))
                    .foregroundColor(.green)
                
            
                
                
            }.frame( height: 40 )
            .frame( width: 150 )
            .background(Color.white)
            .cornerRadius(10)
            .shadow(radius: 4)
            .padding( .leading)
            
            
            
           
            
            .onTapGesture {
                
                // add food action here
                
                print("Add food Action")
//                self.SELECTEDFOOD.append(NewFooditemModel(
//                                foodName: $foodName.projectedValue.wrappedValue,
//                                calPerUnit: $cal.projectedValue.wrappedValue,
//                                unit: $cup.projectedValue.wrappedValue,
//                                fedTiems: $manyTiems.projectedValue.wrappedValue))
//
//
//
//
                
                
                
                if self.cal == "" || self.foodName == "" || self.cup == "" {
                    
                    
                    showWarningAlert(title: "", message: "Please complete the data First ")
                    
                } else {
                    
                    addItemPressed()
                    
                    
                }
                
                
                
                
                
                
                
            }
                
                
                
            
            }
            
        }.padding()
            
            
           
        
        
        
        
}
    
    
    
    func addItemPressed () {
        var arrayOAns : [String] = []
        
        
        
        arrayOAns.append(
                $foodName.projectedValue.wrappedValue
            
            )
        
        
        arrayOAns.append(
            $cup.projectedValue.wrappedValue
        )
        arrayOAns.append(
            $cal.projectedValue.wrappedValue
        )
        
      
        arrayOAns.append(
            $manyTiems.projectedValue.wrappedValue
        )
        
      
        print("🔈 single array answer ..   ")
        print(arrayOAns)
        
        print("🔈 test view model  ")
        self.viewModel.Fooditems.append(arrayOAns)
        self.viewModel.currentQuestion.food?.append(arrayOAns)
        print(self.viewModel.Fooditems)
        
        print("🟡🟡🟡🟡🟡 selected food model print  ")
        print(SELECTEDFOOD)
        
        
        arrayOAns.removeAll()
        showSuccessAlert(title: "Great!", message: "Added Successfully .. ")

        self.manyTiems = ""
        self.cup = ""
        self.foodName = ""
        self.cal = ""
        self.foodNum += 1
        
        
    }
}

struct newFoodView_Previews: PreviewProvider {
    static var previews: some View {
        newFoodView()
    }
}



