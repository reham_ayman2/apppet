//
//  newFoodModel.swift
//  Vet Questionnaire SwiftUI
//
//  Created by Reham Ayman on 24/08/2022.
//

import Foundation
struct NewFooditemModel  : Identifiable , Codable , Hashable {
    var id : String
    var foodName : String
    var NcalPerUnit : String
    var unit : String
    var FedTiems : String
    
    
    
    init(id : String = UUID().uuidString ,  foodName : String , calPerUnit : String , unit : String , fedTiems : String  ) {
        self.id = id
        self.foodName = foodName
        self.NcalPerUnit = calPerUnit
        self.unit = unit
        self.FedTiems = fedTiems
    }
    
    
    
   
}
class listViewModel : ObservableObject {
    @Published var items : [NewFooditemModel] = [] {
        
        didSet {
            saveItemsToUserDefaults()
        }
    }
    
    func addNewItem ( id : Int  ) {
        let newItem = NewFooditemModel( foodName: "", calPerUnit:  "" , unit: "", fedTiems: "" )
        items.append(newItem)
        
    }
    
    
    func saveItemsToUserDefaults () {
        if let encodedData = try? JSONEncoder().encode(items) {
            UserDefaults.standard.set( encodedData , forKey: "Fooditems")
            
        }
    }
    
    
    
    
}
