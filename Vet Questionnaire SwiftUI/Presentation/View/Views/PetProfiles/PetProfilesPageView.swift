//
// Created by NoobMaster69 on 11/04/2022.
//

import Foundation
import SwiftUI

struct PetProfilesPageView: View {


    @EnvironmentObject var viewModel: PetProfilesViewModel
    
    let columns = [
        GridItem(.fixed(180)),
        GridItem(.fixed(180)),
    ]
    @State var addingPet = false

    var body: some View {

        ScrollView {
            VStack {
                HeaderView()
                LazyVGrid(columns: columns, spacing: 10) {
                    ForEach(viewModel.userPets) { pet in
                        NavigationLink(destination: PetProfilePageView().environmentObject(PetProfilePageViewModel(petId: pet.id))) {
                            PetCardView(pet: pet).padding(4)
                        }.accentColor(.black)
                    }
                    AddPetCardView().onTapGesture(perform: { addingPet = true })
                }
                        .padding(.bottom, 20)
                        .padding(.top, 50)
                        .background(RoundedCorners(color: AppColors.lightBlue, tl: 24, tr: 24, bl: 0, br: 0))
            }
                    .sheet(isPresented: $addingPet) {
//                        let viewModel = QuestionnaireViewModel();
                        QuestionnaireView(isDialog: true) {
                            addingPet = false
//                            viewModel.restartQuestionnaire()
                            self.viewModel.updateUserPets()
                        }
                                .environmentObject(viewModel)
                    }
                    .frame(alignment: .topLeading)
        }

    }

}
