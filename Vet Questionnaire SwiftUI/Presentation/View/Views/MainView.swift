//
//  ContentView.swift
//  Vet Questionnaire SwiftUI
//
//  Created by NoobMaster69 on 04/03/2022.
//

import SwiftUI


struct MainView: View {
    var body: some View {
        ZStack{
            let defaults = UserDefaults.standard
            
            // lw el user 3ndo token
            if defaults.string(forKey: UserConstants.authToken.rawValue) != nil {
               CoreAppContainerView()
                
            }
            
            
            // lw m3ndooh :)
            else {
//                let viewModel = OnBoardingViewModel(onBoardingSteps: OnBoardingSteps.firstOnBoardingSteps)
               //  newFirstWelcomeView()
                newTABview()

            }
        }.onAppear {
            print("🌸🌸🌸🌸🌸🌸🌸🌸🌸  token checked ")
            print("\(UserDefaults.standard.string(forKey: UserConstants.authToken.rawValue))")
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        MainView()
    }
}
