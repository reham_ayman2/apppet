//
//  HealthModel.swift
//  Vet Questionnaire SwiftUI
//
//  Created by Oufaa on 20/06/2022.
//

import Foundation
struct HealthModel: Hashable ,Identifiable {
  
    
    
    let id = UUID()
    var imageName:String
    var label: String
    var petConditionState :Int
    
}

struct HealthMockData {
    
    static let healthData = [
    HealthModel(imageName: "cat", label: "Hot Flashes",petConditionState: 0),
    HealthModel(imageName: "dog", label: "Heart palpitations",petConditionState: 0),
    HealthModel(imageName: "cat", label: "Insomnia",petConditionState: 0),
    HealthModel(imageName: "dog", label: "Cold Flashes",petConditionState: 0),
    HealthModel(imageName: "cat", label: "Joint pain",petConditionState: 0),
    HealthModel(imageName: "dog", label: "Anxiety",petConditionState: 0),
    HealthModel(imageName: "cat", label: "Dryness",petConditionState: 0),
    ]
}
