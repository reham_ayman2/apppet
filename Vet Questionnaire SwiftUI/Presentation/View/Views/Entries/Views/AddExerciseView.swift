//
//  AddActivityDialog.swift
//  Vet Questionnaire SwiftUI
//
//  Created by NoobMaster69 on 02/05/2022.
//

import SwiftUI
import Focuser

struct AddExerciseView: View {
    let onActivityClosePressed: () -> ()
    let onActivitySubmit: (_ exercise: String, _ hours: String, _ mins: String) -> ()


    let activityTypes = [
        PresentationActivity.ActivityType.exercise.rawValue,
        PresentationActivity.ActivityType.food.rawValue]

    let exercises = ["Agility training", "Dancing", "Fetch", "Frisbee", "Walking"]
  
    @State var currentActivityType = PresentationActivity.ActivityType.exercise.rawValue

    @State var currentExercise = "Agility training"



    @State var selectingActivity = false
    @State var selectingExercise = false

    @State var hours = ""
    @State var mins = ""
    @FocusStateLegacy var focusedField: String?


    var body: some View {
        VStack {
            HStack(alignment: .center, spacing: 40) {
                Text("Add Exercise Activity")
                        .font(.custom("Poppins-Bold", size: 24))

                Image(systemName: "xmark")
                        .resizable()
                        .frame(maxWidth: 24, maxHeight: 24)
                        .foregroundColor(AppColors.secondaryColor)
                        .onTapGesture(perform: onActivityClosePressed)

            }
            VStack {
//                Text("What activity did your pet do?")
//                        .font(.custom("Poppins-SemiBold", size: 18))
//                        .frame(maxWidth: .infinity, alignment: .leading)
//                        .padding(.horizontal)
//
//                DisclosureGroup(currentActivityType, isExpanded: $selectingActivity) {
//
//                    ForEach(activityTypes, id: \.self) { activity in
//                        Text(activity)
//                                .onTapGesture {
//                                    withAnimation(.easeInOut) {
//                                        currentActivityType = activity
//                                        selectingActivity = false
//                                    }
//                                }
//                                .frame(maxWidth: .infinity, alignment: .leading)
//                    }
//
//                }
//                        .accentColor(AppColors.secondaryColor)
//                        .foregroundColor(.black)
//                        .padding(8)
//                        .overlay(
//                                RoundedRectangle(cornerRadius: 8)
//                                        .stroke(AppColors.secondaryColor, lineWidth: 2)
//                        )
//                        .padding(.horizontal)
//
//
//                        .frame(maxWidth: .infinity, alignment: .leading)
//
              
                        VStack {
                            Text("What exercise did your pet do?")
                                    .font(.custom("Poppins-SemiBold", size: 18))
                                    .frame(maxWidth: .infinity, alignment: .leading)

                            DisclosureGroup(currentExercise, isExpanded: $selectingExercise) {

                                ForEach(exercises, id: \.self) { activity in
                                    Text(activity)
                                            .onTapGesture {
                                                withAnimation(.easeInOut) {
                                                    currentExercise = activity
                                                    selectingExercise = false
                                                }
                                            }
                                            .frame(maxWidth: .infinity, alignment: .leading)
                                }

                            }
                                    .accentColor(AppColors.secondaryColor)
                                    .foregroundColor(.black)
                                    .padding(8)
                                    .overlay(
                                            RoundedRectangle(cornerRadius: 8)
                                                    .stroke(AppColors.secondaryColor, lineWidth: 2)
                                    )

                            Text("Amount of time")
                                    .font(.custom("Poppins-SemiBold", size: 18))
                                    .frame(maxWidth: .infinity, alignment: .leading)
                                    .padding(.top)

                            HStack {
                                RoundedBorderTextField(title: "Hours", hint: "e.g. 4", type: .NUMBER, boundValue: $hours, focusedField: _focusedField)
                                        .padding(.trailing)
                                RoundedBorderTextField(title: "Min", hint: "e.g. 2", type: .NUMBER, boundValue: $mins, focusedField: _focusedField, isLast: true)
                                        .padding(.leading)
                            }
                                    .padding(.top, -16)
                        }
                        .padding()

                Button("Add") {
                    onActivitySubmit(
                            currentExercise,
                            hours,
                            mins)
                }
                        .padding()
                        .frame(maxWidth: .infinity)
                        .background(AppColors.secondaryColor)
                        .clipShape(RoundedRectangle(cornerRadius: 16))
                        .foregroundColor(.white)
                        .padding(.horizontal)

            }
                    .frame(maxWidth: .infinity, alignment: .leading)
                    .padding()
        }
    }


}

struct AddActivityDialog_Previews: PreviewProvider {
    static var previews: some View {
        AddExerciseView(onActivityClosePressed: {     }, onActivitySubmit: {  exercise,hours, mins in  })
    }
}
