//
//  AddFoodView .swift
//  Vet Questionnaire SwiftUI
//
//  Created by Oufaa on 20/06/2022.
//

import SwiftUI
import Focuser

struct AddFoodView: View {
    
    let foodTypes = ["Commercial brand", "Homemade"]
    @State var currentFoodType = "Commercial brand"
    @State var selectingFoodType = false
    @State var calories = ""
    @FocusStateLegacy var focusedField: String?
    let onActivityClosePressed: () -> ()
    let onActivitySubmit: (_ food: String, _ calories: String) -> ()
    
    var body: some View {
       
        VStack {
            HStack(alignment: .center, spacing: 40) {
                Text("Add Food Activity")
                        .font(.custom("Poppins-Bold", size: 24))

                Image(systemName: "xmark")
                        .resizable()
                        .frame(maxWidth: 24, maxHeight: 24)
                        .foregroundColor(AppColors.secondaryColor)
                        .onTapGesture(perform: onActivityClosePressed)
            }.padding(.bottom , 50)

            Text("What food type did your pet take?")
                    .font(.custom("Poppins-SemiBold", size: 18))
                    .frame(maxWidth: .infinity, alignment: .leading)

            DisclosureGroup(currentFoodType, isExpanded: $selectingFoodType) {

                ForEach(foodTypes, id: \.self) { foodType in
                    Text(foodType)
                            .onTapGesture {
                                withAnimation(.easeInOut) {
                                    currentFoodType = foodType
                                    selectingFoodType = false
                                }
                            }
                            .frame(maxWidth: .infinity, alignment: .leading)
                }

            }
                    .accentColor(AppColors.secondaryColor)
                    .foregroundColor(.black)
                    .padding(8)
                    .overlay(
                            RoundedRectangle(cornerRadius: 8)
                                    .stroke(AppColors.secondaryColor, lineWidth: 2)
                    )

            Text("Amount of calories")
                    .font(.custom("Poppins-SemiBold", size: 18))
                    .frame(maxWidth: .infinity, alignment: .leading)
                    .padding(.top)

            RoundedBorderTextField(title: "Calories (KCal)", hint: "e.g. 40", type: .NUMBER, boundValue: $calories, focusedField: _focusedField, isLast: true)
                    .padding(.top, -16)
                    .padding(.bottom, 20)
            Button("Add") {
                onActivitySubmit(
                        currentFoodType,
                        calories)
            }
                    .padding()
                    .frame(maxWidth: .infinity)
                    .background(AppColors.secondaryColor)
                    .clipShape(RoundedRectangle(cornerRadius: 16))
                    .foregroundColor(.white)
                    
        }
        .padding(25)
        
  
    }
}

struct AddFoodView__Previews: PreviewProvider {
    static var previews: some View {
        AddFoodView(onActivityClosePressed: {  }, onActivitySubmit: {  food,calories in  })
    }
}
