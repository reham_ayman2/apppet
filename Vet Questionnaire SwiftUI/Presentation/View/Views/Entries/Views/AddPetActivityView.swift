//
//  AddPetActivity.swift
//  Vet Questionnaire SwiftUI
//
//  Created by Oufaa on 17/06/2022.
//

import SwiftUI
import HalfASheet
struct AddPetActivityView: View {
    
    @State private var isShowing = false
    @State private var amount = 0.0
    @State  var selection = 1
    var contents = ["Food","Excersice","Health"]
    var body: some View {
        
        ZStack{
            Button {
                isShowing.toggle()
                let _ = print(isShowing)
            } label: {
                Image(systemName: "plus.circle.fill")
                    .resizable()
                    .scaledToFit()
                    .frame(minWidth:60, maxWidth:60, minHeight: 60,maxHeight: 60)
                .foregroundColor(AppColors.secondaryColor)        }
            
            HalfASheet(isPresented: $isShowing, title: "Log") {
                
                VStack{
                    HStack{
                        Button {
                            selection = 1
                        } label: {
                            Text("Food").foregroundColor(.black)
                            
                            
                        }
                        .frame(width: 100, height: 35)
                        .background(selection == 1 ? Color.white : Color.clear)
                        
                        Button {
                            selection = 2
                        } label: {
                            Text("Exercise").foregroundColor(.black)
                        }
                        .frame(width: 100, height: 35)
                        .background(selection == 2 ? Color.white : Color.clear)
                        Button {
                            selection = 3
                        } label: {
                            Text("Health").foregroundColor(.black)
                        } .frame(width: 100, height: 35)
                            .background(selection == 3 ? Color.white : Color.clear)
                        
                    }
                    
                    .frame(maxHeight: 50, alignment: .center)
                    .padding(.horizontal, 40)
                    .background(AppColors.lightGrey)
                    .cornerRadius(10)
                    
                    .padding(.bottom, 15)
                    
                    ContainerView(selection: $selection)
                }
                
                
                
                
            }.height(.proportional(0.60))
                .closeButtonColor(.white)
                .backgroundColor(.white)
            
            
            
        }
    }
}

struct AddPetActivity_Previews: PreviewProvider {
    static var previews: some View {
        AddPetActivityView(selection: 1)
            .previewDevice("iPhone 8 Plus")
    }
}

struct FoodView : View {
    
    let columns: [GridItem] = [GridItem(.flexible()),
                               GridItem(.flexible())
    ]
    @State var addingActivity: Bool = false
    var body: some View{
        LazyVGrid(columns: columns, spacing: 20){
            AddButtonView(content: "BreckFast",label: "40 Kcal Over",isPresenting: $addingActivity)
                .padding(.trailing, 10)
                .sheet(isPresented: $addingActivity) {
//                    AddActivityDialog {
//                        withAnimation {
//                            addingActivity = false
//                        }
//                    } onActivitySubmit: {exercise, hours, mins in
//                        print("Hello Food")
//                    }
                    AddFoodView {
                        addingActivity = false
                    } onActivitySubmit: { food, calories in
                        print("Food Is = \(food) And Calories = \(calories)")
                    }

                }
            AddButtonView(content: "Snack",label: "70 Kcal Left",isPresenting: $addingActivity)
                .sheet(isPresented: $addingActivity) {
                    AddFoodView {
                        addingActivity = false
                    } onActivitySubmit: { food, calories in
                        print("Food Is = \(food) And Calories = \(calories)")
                    }

                    
                }
            AddButtonView(content: "Lunch",label: "350 Kcal Left",isPresenting: $addingActivity)
                .padding(.trailing, 10)
                .sheet(isPresented: $addingActivity) {
                    AddFoodView {
                        addingActivity = false
                    } onActivitySubmit: { food, calories in
                        print("Food Is = \(food) And Calories = \(calories)")
                    }

                }
            AddButtonView(content: "Dinner",label: "400 Kcal Left",isPresenting: $addingActivity)
                .sheet(isPresented: $addingActivity) {
                    AddFoodView {
                        addingActivity = false
                    } onActivitySubmit: { food, calories in
                        print("Food Is = \(food) And Calories = \(calories)")
                    }

                }
        }
        
    }
    
}

struct ExerciseView : View {
    
    let columns: [GridItem] = [GridItem(.flexible()),
                               GridItem(.flexible())
    ]
    @State var addingActivity: Bool = false
    var body: some View{
        LazyVGrid(columns: columns){
            AddButtonView(content: "Dance" ,isPresenting: $addingActivity)
                .padding(.trailing, 10)
                .sheet(isPresented: $addingActivity) {
                    AddExerciseView {
                        withAnimation {
                            addingActivity = false
                        }
                    } onActivitySubmit: { exercise, hours, mins in
                        print("Exercise = \(exercise)  hours = \(hours)" )

                    }
                }
            AddButtonView(content: "PlayTime",isPresenting: $addingActivity)
                .sheet(isPresented: $addingActivity) {
                    AddExerciseView {
                        withAnimation {
                            addingActivity = false
                        }
                    } onActivitySubmit: {  exercise, hours, mins in
                        print("Exercise = \(exercise)  hours = \(hours)" )

                    }                }
            AddButtonView(content: "Walk",isPresenting: $addingActivity)
                .padding(.trailing, 10)
                .sheet(isPresented: $addingActivity) {
                    AddExerciseView {
                        withAnimation {
                            addingActivity = false
                        }
                    } onActivitySubmit: {  exercise, hours, mins in
                        print("Exercise = \(exercise)  hours = \(hours)" )
                    }
                }
        }
        
    }
    
}

struct AddButtonView : View {
    var content: String
    var label : String?
    @Binding var isPresenting: Bool
    var body: some View{
        
        VStack{
            Button {
                isPresenting = true
            } label: {
                Image(systemName: "plus.circle.fill")
                    .resizable()
                    .scaledToFit()
                    .frame(minWidth:60, maxWidth:60, minHeight: 60,maxHeight: 60)
                    .foregroundColor(AppColors.secondaryColor)
                
            }.padding(5)
            Text(content)
            Text(label ?? "")
            
        }.padding(20)
        
            .frame(maxWidth: 200, maxHeight: 170, alignment: .center)
            .overlay(RoundedRectangle(cornerRadius: 10).stroke(Color.gray,lineWidth: 1.0))
            .cornerRadius(10)
        
        
    }
    
}

struct HealthView : View {
    
    let columns: [GridItem] = [GridItem(.flexible()),
                               GridItem(.flexible())
    ]
    @State var addingActivity: Bool = false
    var body: some View{
        LazyVGrid(columns: columns){
            AddButtonView(content: "Symptoms",isPresenting: $addingActivity)
                .sheet(isPresented: $addingActivity) {
                    AddSymptomsView {
                        withAnimation {
                            addingActivity = false
                        }
                    }
                    
                }
            AddButtonView(content: "Symptoms",isPresenting: $addingActivity)
                .sheet(isPresented: $addingActivity) {
                    AddSymptomsView {
                        withAnimation {
                            addingActivity = false
                        }
                    }
                }
            AddButtonView(content: "Symptoms",isPresenting: $addingActivity)
                .sheet(isPresented: $addingActivity) {
                    AddSymptomsView {
                        withAnimation {
                            addingActivity = false
                        }
                    }
                    
                }        }
        
    }
    
}

struct ContainerView : View {
    
    @Binding var selection:Int
    
    
    var body: some View{
        if selection == 1 {
            FoodView()
        }else if selection == 2
        {
            ExerciseView()
        }else if selection  == 3 {
            HealthView()
        }
    }
    
}




