////
////  AddSymptomsView.swift
////  Vet Questionnaire SwiftUI
////
////  Created by Oufaa on 20/06/2022.
////
//
//import SwiftUI
//
//struct AddSymptomsView: View {
//      @State var value: Int = 0
//      var maximum: Int = 3
//      var height: CGFloat = 20
//      var spacing: CGFloat = 1
//      var selectedColor: Color = .accentColor
//      var unselectedColor: Color = Color.secondary.opacity(0.3)
//
//    var body: some View {
//        HStack{
//            Image("cat").resizable()
//                .aspectRatio(contentMode: .fit)
//                .frame( maxHeight: 30)
//                .padding(2)
//
//            Text("Hot Flashes")
//                .padding(.trailing)
//
//
//            Button {
//                value = 0
//            } label: {
//
//                Image(systemName: "circle.fill").foregroundColor(value == 0 ? Color(.blue) : Color(.white))
//                    .padding(2)
//                    .overlay(
//                               RoundedRectangle(cornerRadius: 25)
//                                .stroke(Color.gray, lineWidth: 0.5)
//                       )
//            }
//
//            HStack(spacing: spacing) {
//                ForEach(0 ..< maximum, id: \.self) { index in
//                    Button {
//                    self.value = index + 1
//                    } label: {
//            Rectangle()
//                .foregroundColor(index < self.value ? self.selectedColor : self.unselectedColor)
//                    }
//
//              }
//            }
//         .frame(maxHeight: 30)
//         .clipShape(Capsule())
//         .padding(4)
//         .overlay(
//                    RoundedRectangle(cornerRadius: 25)
//                     .stroke(Color.gray, lineWidth: 0.5)
//            )
//         .padding()
//    }
//    }
//}
//
//struct AddSymptomsView_Previews: PreviewProvider {
//    static var previews: some View {
//        AddSymptomsView()
//    }
//}
//
//
