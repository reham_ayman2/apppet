//
//  AddSymptomsView.swift
//  Vet Questionnaire SwiftUI
//
//  Created by Oufaa on 20/06/2022.
//

import SwiftUI

struct AddSymptomsView: View {
    let onActivityClosePressed: () -> ()
    var gridItems : [GridItem] = [GridItem(.flexible())]
    var body: some View {
        
        VStack(alignment:.leading){
            HStack{
                Image(systemName: "xmark")
                    .padding()
                    .onTapGesture(perform: onActivityClosePressed)
                Text("Log")
            }
            
            Text("Symptoms")
                .fontWeight(.bold)
                .underline()
                .padding(.leading)
                .padding(.bottom)
                
               
            LazyVGrid(columns: gridItems){
                Spacer()
                HStack{
                    Text("NONE").font(.system(size: 10)).fontWeight(.light)
//                        .padding()
                    Text("MILD").font(.system(size: 10))
                        .fontWeight(.light)
                        .padding(.leading,25)
                    Text("MID").font(.system(size: 10))
                        .fontWeight(.light)
                        .padding(.leading,15)
                    Text("SEVERE").font(.system(size: 10))
                        .fontWeight(.light)
                        .padding(.leading,12)
                }
                .frame(width: 350, alignment: .trailing)
                Divider()
               
            }.frame(maxWidth: .infinity)
            ScrollView{
            ForEach(HealthMockData.healthData) { health in
                SymptomsViewCell(value: health.petConditionState,labelName: health.label, imageName: health.imageName)
            }
            }.frame( maxHeight: .infinity)
            Spacer()
            Button("Submit") { }
            .frame(maxWidth: .infinity)
            .padding()
            .background(AppColors.secondaryColor)
            .clipShape(RoundedRectangle(cornerRadius: 16))
            .foregroundColor(.white)
            .padding(20)
        }.padding()
        
    }
}

struct AddSymptomsView_Previews: PreviewProvider {
    static var previews: some View {
        AddSymptomsView(onActivityClosePressed: {})
            .previewDevice("iPhone 8")
    }
}


struct SymptomsViewCell: View {
    @State var value: Int
    @State  var labelName : String
    @State var imageName : String
    var maximum: Int = 3
    var height: CGFloat = 20
    var spacing: CGFloat = 1
    var selectedColor: Color = .accentColor
    var unselectedColor: Color = Color.secondary.opacity(0.3)
    
    
    var body: some View {
        
        
        
        HStack{
            Image(imageName).resizable()
                .aspectRatio(contentMode: .fit)
                .frame( height: 50)
                .padding(10)
            
            Text(labelName)
                .frame(maxWidth: 90)
                .padding(.trailing)
            
            
            
            Button {
                value = 0
            } label: {
                
                Image(systemName: "circle.fill").foregroundColor(value == 0 ? Color(.blue) : Color(.white))
                    .padding(2)
                    .overlay(
                        RoundedRectangle(cornerRadius: 25)
                            .stroke(Color.gray, lineWidth: 0.5)
                    )
            }
            
            HStack(spacing: spacing) {
                ForEach(0 ..< maximum, id: \.self) { index in
                    Button {
                        value = index + 1
                    } label: {
                        Rectangle()
                            .foregroundColor(index < self.value ? self.selectedColor : self.unselectedColor)
                    }
                }
            }
            .frame( height: 25)
            .clipShape(Capsule())
            .padding(4)
            .overlay(
                RoundedRectangle(cornerRadius: 25)
                    .stroke(Color.gray, lineWidth: 0.5)
            )
            .padding()
        }
    }
}
