//
//  EntriesPageView.swift
//  Vet Questionnaire SwiftUI
//
//  Created by NoobMaster69 on 19/04/2022.
//

import SwiftUI

struct EntriesPageView: View {
    @EnvironmentObject var viewModel: EntriesViewModel
    let formatter = DateFormatter();
    @State var addingActivity = false

    init() {
        formatter.dateFormat = "dd-MM-yyyy"
    }

    var body: some View {
        ScrollView {
            VStack {
                HeaderView()
                ZStack(alignment: .topLeading) {
                    VStack {
                        Text("Let’s check out").font(.custom("Poppins-Bold", size: 20)).frame(maxWidth: .infinity, alignment: .leading)
                        HStack {
                            Text("Your")
                                    .font(.custom("Poppins-Bold", size: 20))
                            Text("Entries.")
                                    .foregroundColor(AppColors.primaryColor)
                                    .font(.custom("Poppins-Bold", size: 20))
                        }
                                .frame(maxWidth: .infinity, alignment: .leading)
                    }
                            .padding(.leading)
                    Image("entries")
                }
                        .padding(.top)

                VStack {
                    VStack {
                        Text("Choose A Pet").font(.custom("Poppins-Bold", size: 24)).frame(maxWidth: .infinity, alignment: .topLeading)
                                .padding()
                        ScrollView(.horizontal) {
                            HStack {
                                ForEach(viewModel.userPets) { pet in
                                    PetChipView(pet: pet)
                                            .opacity(pet.id == viewModel.currentPetId ? 1 : 0.5)
                                            .onTapGesture {
                                                viewModel.currentPetId = pet.id
                                            }
                                }
                            }
                                    .frame(minHeight: 90, maxHeight: 120)

                        }
                                .padding([.horizontal])
                                .onAppear {
                                    viewModel.fetchUserPets()
                                }
                    }

                    VStack {
                        Text("Pet Entries").font(.custom("Poppins-Bold", size: 24)).frame(maxWidth: .infinity, alignment: .topLeading)
                                .padding()
                        ScrollViewReader { proxy in
                            ScrollView(.horizontal) {

                                LazyHGrid(rows: [GridItem(.flexible(minimum: 120))]) {
                                    ForEach(viewModel.dates, id: \.self) { dateItem in

                                        DateChipView(date: formatter.date(from: dateItem) ?? Date(),
                                                selected: dateItem == viewModel.currentDate)
                                                .id(dateItem)
                                                .onTapGesture {
                                                    viewModel.currentDate = dateItem
                                                }
                                                .padding(.horizontal, 2)
                                    }

                                }
                                        .frame(minHeight: 0, maxHeight: .greatestFiniteMagnitude)

                            }
                                    .onAppear {
                                        proxy.scrollTo(viewModel.currentDate, anchor: .center)
                                    }

//                            AddButtonFullWidthView().padding(.horizontal)
//                                    .onTapGesture {
//                                        addingActivity = true
//                                    }
                            
                            AddPetActivityView().frame(maxWidth: .infinity,  alignment: .center)
                                .padding()
                            ForEach(viewModel.currentPetActivities) { activity in
                                ActivityListItemView(activity: activity)
                            }


                        }

                                .padding([.horizontal, .bottom])
                    }
                            .background(RoundedCorners(color: AppColors.lightBlue, tl: 24, tr: 24, bl: 0, br: 0))

                }
                        .background(RoundedCorners(color: AppColors.lightGrayBlue, tl: 24, tr: 24, bl: 0, br: 0))
                        .sheet(isPresented: $addingActivity) {
                            AddExerciseView {
                                withAnimation {
                                    addingActivity = false
                                }
                            } onActivitySubmit: { exercise, hours, mins in
                                withAnimation {
                                    viewModel.addExerciseActivity( exercise, hours, mins)
                                    addingActivity = false
                                }
                            }

                        }

            }
                    .frame(maxWidth: .infinity, alignment: .top)
     
        }
    }
}

struct EntriesPageView_Previews: PreviewProvider {
    static var previews: some View {
        EntriesPageView().environmentObject(EntriesViewModel())
    }
}


