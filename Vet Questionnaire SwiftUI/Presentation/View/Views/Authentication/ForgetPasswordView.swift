//
// Created by NoobMaster69 on 04/04/2022.
//

import SwiftUI
import Focuser
import KeyboardToolbars


struct ForgetPasswordView: View {
    @EnvironmentObject var viewModel: AuthViewModel
    @State var email: String = ""
    @State var mailSent: Bool = false
  
    @State var currentMessage: String? = ""
    @State var showingAlert: Bool = false
    @FocusStateLegacy var focusedField: String? = ""
    
    //MARK: -  LOADER VARS
    @State var  isPopUp = false
    @State var  rotate = false
    
    

    var body: some View {
        NavigationView {
            ZStack {
                AppColors.primaryColor.ignoresSafeArea()

                VStack {

                    Image(uiImage: UIImage(named: "SmallLogo")!)
                            .resizable()
                            .scaledToFit()
                            .frame(maxWidth: 150, alignment: .top)
                            .padding(50)

                    VStack {
                        Text("Forgot Password")
                                .font(.custom("Poppins-SemiBold", size: 30)).padding()
                        RoundedBorderTextField(title: "Email Address", hint: "e.g. sample@gmail.com", boundValue: $email, focusedField: _focusedField, isLast: true)
                                .padding(.top)


                        NavigationLink("", destination: SignInPageView(), isActive: $mailSent)
                        Button("Send reset E-mail") {
                            withAnimation(.spring()){isPopUp.toggle()}
                            viewModel.forgetPassword(email: $email.wrappedValue) { response in
                                withAnimation(.spring()){isPopUp.toggle()}
                                currentMessage = response.message
                                showingAlert = true
                                if response.isSuccessful() {
                                    mailSent = true
                                }
                            }
                        }
                                .frame(maxWidth: .infinity, alignment: .center)
                                .padding()
                                .foregroundColor(.white)
                                .background(AppColors.secondaryColor)
                                .clipShape(RoundedRectangle(cornerRadius: 12))
                                .padding()
                                .padding(.top, 30)

                        Spacer()

                        HStack {
                            Text("Don’t have an account?")
                                .font(.custom("Poppins-SemiBold", size: 16)).opacity(0.5)
                            NavigationLink(destination: SignUpPageView().environmentObject(AuthViewModel())) {
                                Text("Sign Up")
                                    .font(.custom("Poppins-SemiBold", size: 16))
                            }

                        }
                                .padding().frame(maxWidth: .infinity, maxHeight: .infinity, alignment: .center)
                                .padding(.bottom)

                    }
                            .background(RoundedCorners(color: .white, tl: 30, tr: 30))


                }
                        .navigationBarBackButtonHidden(true)
                        .navigationBarHidden(true)
                        .ignoresSafeArea(.all, edges: .bottom)
                        .padding(.top)
                        .frame(maxHeight: .infinity, alignment: .bottom)


            }
                    
                    .alert(isPresented: $showingAlert) {
                        Alert(title: Text("Forget password"),
                                message: Text($currentMessage.wrappedValue ?? ""))
                    }
                    .ignoresSafeArea(.all, edges: .bottom)
                    .navigationBarBackButtonHidden(true)
                    .navigationBarHidden(true)
        }
        
        //MARK: -  LOADER ISSSUES
        
        .overlay(
            ZStack {
                if isPopUp {
                    Color.primary.opacity(0.5)
                        .ignoresSafeArea()
                    
                    
                    animationViewLoader(ShowPopUp: $isPopUp, rotate: $rotate)
                }
            }
        
        
        )
        
        
        
                .navigationBarBackButtonHidden(true)
                .navigationBarHidden(true)
                .ignoresSafeArea(.all, edges: .bottom)
                .addHideKeyboardButton()


    }

}


struct ForgetPasswordView_Previews: PreviewProvider {
    static var previews: some View {
        ForgetPasswordView().environmentObject(AuthViewModel())
    }
}
