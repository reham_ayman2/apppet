//
//  SignInPageView.swift
//  Vet Questionnaire SwiftUI
//
//  Created by NoobMaster69 on 16/03/2022.
//

import SwiftUI
import Focuser
import KeyboardToolbars

struct SignInPageView: View {
    @EnvironmentObject var viewModel: AuthViewModel
    @FocusStateLegacy var focusedField: String? = ""
    
    

    @State var userName: String = ""
    @State var password: String = ""
    @State var isLoading: Bool = false
    @State var errorOccurred: Bool = false
    @State var currentError: String? = nil
    @State var destActivated: Bool = false
    
    //MARK: -  LOADER VARS
    @State var  isPopUp = false
    @State var  rotate = false
    
    
    
    let signInDest = CoreAppContainerView()

    var body: some View {
        NavigationView {
            ZStack {
                      Color("mainColor").ignoresSafeArea()

                VStack {

                    Image(uiImage: UIImage(named: "SmallLogo")!)
                            .resizable()
                            .scaledToFit()
                            .frame(maxWidth: 150, alignment: .top)
                            .padding(40)


                    VStack {

                        Text("Welcome to \nPetMetrics App")
                                .font(.custom("Poppins-SemiBold", size: 26))
                                .multilineTextAlignment(.center)
                                .foregroundColor(.black)
                                .padding(.vertical)
                                .lineLimit(2)
                                .frame(maxWidth:.infinity)
                        


                        RoundedBorderTextField(title: "Email", hint: "e.g. email@gmail.com", type: .EMAIL, boundValue: $userName, focusedField: _focusedField)
                            .padding(.top).padding(.horizontal)


                        RoundedBorderTextField(title: "Password", hint: "Enter your password", type: .PASSWORD, boundValue: $password, focusedField: _focusedField, isLast: true).padding(.horizontal)


                        NavigationLink("", destination: signInDest, isActive: $destActivated)
                        Button("Sign in") {
                            
                            // check validation
                            
                            if viewModel.isValidpassword(password: self.password) == false {
                                showWarningAlert(title: "", message: "Your Password Should be at least 6 characters long")
                            } else  if viewModel.isValidEmail(testStr: self.userName) == false {
                                showWarningAlert(title: "", message:  "Incorrect Email Format")
                            }
                            else
                            
                            {
                            
                                withAnimation(.spring()){isPopUp.toggle()}
                            viewModel.signIn($userName.wrappedValue, $password.wrappedValue) { response in
                                
                                
                                
                                print("responce back in login ")
                                print("🙋‍♀️🙋‍♀️🙋‍♀️🙋‍♀️🙋‍♀️🙋‍♀️🙋‍♀️🙋‍♀️🙋‍♀️🙋‍♀️🙋‍♀️🙋‍♀️🙋‍♀️ \(response)")
                                
                                
                                
                                
                                withAnimation(.spring()){isPopUp.toggle()}
                                print(String(describing: response))
                                print("✅✅✅✅✅✅✅✅✅ login successfully ✅✅✅✅✅ ")
                                if response.isSuccessful() {
                                    destActivated = true
                                    if let user = response.data?.user {
                                        
                                        
                                        
                                        viewModel.storeUserData(user, response.data?.token)
                                    }
                                    
                                    
                                    
                                } else {
                                    
                                    
                                    
                                    currentError = response.message
                                    
                                    errorOccurred = true
                                }
                            }
                                
                            }
                        }

                                .frame(maxWidth: .infinity, alignment: .center)
                                .padding()
                                .foregroundColor(.white)
                                .background(AppColors.secondaryColor)
                                .clipShape(RoundedRectangle(cornerRadius: 12))
                                .padding()
                                .padding(.top, 30)

                        NavigationLink(destination: ForgetPasswordView()) {
                            Text("Forget your password?")
                                .font(.custom("Poppins-Regular", size: 14))
                        }

                        HStack {
                            Text("Don’t have an account?")
                                .font(.custom("Poppins-Regular", size: 16))
                                .opacity(0.5)
                            NavigationLink(destination: SignUpPageView().environmentObject(AuthViewModel())) {
                                Text("Sign Up")
                                    .font(.custom("Poppins-Regular", size: 16))
                            }

                        }
                                .padding().frame(maxWidth: .infinity, maxHeight: .infinity, alignment: .center)
                                .padding(.bottom)

                    }
                            .padding(.top)
                            .ignoresSafeArea()
                            .background(RoundedCorners(color: .white, tl: 30, tr: 30))

                }
                        .overlay(LoadingView(isLoading: $isLoading))

            }
                    .onChange(of: isLoading) { v in
                        print(v)
                    }
                    .navigationBarBackButtonHidden(true)
                    .navigationBarHidden(true)
                    .ignoresSafeArea(.all, edges: .bottom)
                    .alert(isPresented: $errorOccurred) {
                        Alert(
                                title: Text("Sign in error"),
                                message: Text($currentError.wrappedValue ?? "")
                        )
                    }
        }
        
        
        
        
        //MARK: -  LOADER ISSSUES
        
        .overlay(
            ZStack {
                if isPopUp {
                    Color.primary.opacity(0.5)
                        .ignoresSafeArea()
                    
                    
                    animationViewLoader(ShowPopUp: $isPopUp, rotate: $rotate)
                }
            }
        
        
        )
        
        
        
        
                .navigationBarBackButtonHidden(true)
                .navigationBarHidden(true)
                .addHideKeyboardButton()


    }
    
    
    
}

struct SignInPageView_Previews: PreviewProvider {
    static let viewModel: AuthViewModel = AuthViewModel()
    static var previews: some View {
        SignInPageView().environmentObject(viewModel)
    }
}
