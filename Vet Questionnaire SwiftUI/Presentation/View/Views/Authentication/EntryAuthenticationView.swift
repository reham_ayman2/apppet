//
//  EntryAuthenticationView.swift
//  Vet Questionnaire SwiftUI
//
//  Created by Oufaa on 31/05/2022.
//

import SwiftUI


struct EntryAuthenticationView: View {
    @State var destActivated: Bool = false
    @EnvironmentObject var viewModel: AuthViewModel
    var body: some View {
        NavigationView{
       ZStack{
        AppColors.backgroundBlue.edgesIgnoringSafeArea(.all)
           VStack{
               Image(uiImage: UIImage(named: "white-logo-pet-metrics")!)
                       .scaledToFit()
                       .frame(maxWidth: .infinity, maxHeight: .infinity, alignment: .center)
               NavigationLink("",destination: SignUpPageView().environmentObject(AuthViewModel()),isActive: $destActivated)
               VStack {
                   Button("Sign Up") {
                    destActivated  = true
                   }
                           .frame(maxWidth: .infinity, alignment: .center)
                           .padding()
                           .foregroundColor(.white)
                           .background(Color.black)
                           .clipShape(RoundedRectangle(cornerRadius: 12))
                           .padding()
                           .padding(.top, 30)
                   
                   HStack{
                   Text("Already have an account?")
                       .font(.custom("Poppins-Regular", size: 16))
                       .opacity(0.5)
                       .foregroundColor(Color.white)
                       NavigationLink(destination: SignInPageView().environmentObject(AuthViewModel())) {
                       Text("Sign In")
                           .font(.custom("Poppins-Regular", size: 16))
                           .foregroundColor(Color.white)
                   }
               }
                       .padding().frame(maxWidth: .infinity, maxHeight: 30, alignment: .center)
                       .padding(.bottom)
               }
           }
           
       }
      }
        .navigationBarBackButtonHidden(true)
        .navigationBarHidden(true)
    }
}

struct EntryAuthenticationView_Previews: PreviewProvider {
    static var previews: some View {
//        let viewModel: AuthViewModel = AuthViewModel()
        EntryAuthenticationView()
    }
}
