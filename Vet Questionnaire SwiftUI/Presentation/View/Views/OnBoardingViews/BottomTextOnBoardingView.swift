//
// Created by NoobMaster69 on 30/03/2022.
//

import SwiftUI

struct BottomTextOnBoardingView: View {
    @StateObject var viewModel: OnBoardingViewModel
//    let questionnaireViewModel = QuestionnaireViewModel()
    let onEndDestination: AnyView

    var isNavigationDone: Bool {
        viewModel.stepsCount == viewModel.currentStepIdx + 1
    }

    var body: some View {

        NavigationView {
            VStack(alignment: .leading) {
                HStack {
                    Image("AppLogo").resizable()
                            .scaledToFit()
                            .frame(maxWidth: 115, maxHeight: 150)
                    Spacer()
                    NavigationLink {
                        onEndDestination
                    } label: {
                        Text("Skip").foregroundColor(.blue).padding()
                    }

                }.padding()

                Image(uiImage: viewModel.currentStep.image)
                Spacer()
                if let title = viewModel.currentStep.title {
                    Text(title)
                            .bold()
                            .font(.custom("Poppins-Bold",size: 32))
                            .padding(.horizontal)
                            .padding(.bottom,10)
                            .frame(maxWidth:.infinity,alignment: .center)

                }
                Text(viewModel.currentStep.content)
                    .font(.custom("Poppins-Bold",size: 18))
                        .padding(.horizontal)
                        .frame(maxWidth:.infinity,alignment: .center)
                        .multilineTextAlignment(.center)
                HStack {
                    FormProgressView(totalSteps: viewModel.stepsCount, currentStepIdx: viewModel.currentStepIdx + 1)

                    Spacer()
                    if isNavigationDone {
                        NavigationLink(destination: { onEndDestination }
                                , label: {
                            Image("button-right")

                        })
                    } else {
                        Image("button-right")

                                .onTapGesture {
                                    withAnimation {
                                        viewModel.fetchNextOnBoardingStep()
                                    }
                                }
                                .transition(.slide)
                                .animation(.linear)
                    }


                }.padding()

            }
                    .navigationBarBackButtonHidden(true)
                    .navigationBarHidden(true)

        }.navigationBarBackButtonHidden(true)
                .navigationBarHidden(true)
    }
}

struct BottomTextOnBoardingView_Previews: PreviewProvider {
    static var previews: some View {
        let viewModel = OnBoardingViewModel(onBoardingSteps: OnBoardingSteps.secondOnBoardingStep)
        //        let dest = QuestionnaireView(viewModel:questionnaireViewModel)
        let dest = SignInPageView()
        BottomTextOnBoardingView(viewModel: viewModel, onEndDestination: AnyView(dest))
    }
}
