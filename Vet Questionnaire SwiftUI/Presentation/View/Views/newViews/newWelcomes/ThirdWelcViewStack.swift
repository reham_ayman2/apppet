//
//  ThirdWelcViewStack.swift
//  Vet Questionnaire SwiftUI
//
//  Created by Reham Ayman on 28/08/2022.
//

import SwiftUI

struct ThirdWelcViewStack: View {
    var body: some View {
        VStack ( alignment:  .leading , spacing: 20  ){
        
            Image("did you know")
                .resizable()
                .frame(height: 140)
                .padding(.leading , 40)
                .padding(.trailing , 40)
            
            
                
            
            
            Text( "PetMetrics A.I. is designed to help give your pet a voice in their own care.")
                .font(.custom("Poppins-SemiBold", size: 20))
                .frame( maxWidth: .infinity)
                .multilineTextAlignment(.center)
                
              
            Image("dog talking")
                .resizable()
                .frame(height: 130 )
                .frame( maxWidth: .infinity)
                .padding(.leading , 40)
                .padding(.trailing , 40)
            
          
            HStack ( spacing: 12) {
               Capsule()
                    .fill(Color("mainColor").opacity(0.3))
                    .frame(width: 7 , height: 7)
                    
                Capsule()
                    .fill(Color("mainColor").opacity(0.3))
                     .frame(width: 7, height: 7)
                Capsule()
                    .fill(Color("mainColor"))
                     .frame(width: 18, height: 7)
                
            }
            .frame(maxWidth: .infinity)
            
           
        }
     
        
        
        
    }
}

struct ThirdWelcViewStack_Previews: PreviewProvider {
    static var previews: some View {
        ThirdWelcViewStack()
    }
}
