//
//  newFirstWelStackView.swift
//  Vet Questionnaire SwiftUI
//
//  Created by Reham Ayman on 28/08/2022.
//

import SwiftUI

struct newFirstWelStackView: View {
    var body: some View {
       
        VStack ( alignment:  .leading , spacing: 25  )  {
           
            HStack() {
                Image("pet profile")
                    .frame(width: 100, height: 100)
                    .scaledToFit()
                VStack (alignment: .leading, spacing: 5 ) {
                    Text ("Profile")
                        .font(.custom("Poppins-SemiBold", size: 22))
                    
                    
                    
                    Text("Create a health profile for each of your pets.")
                        .font(.custom("Poppins-Regular", size: 14))
                }
                
               
                   
            }
            
           
            
            HStack  {
                Image("nutrition")
                    .frame(width: 100, height: 100)
                    .scaledToFit()
                VStack(alignment: .leading, spacing: 5 ) {
                    Text ("Nutrition")
                        .font(.custom("Poppins-SemiBold", size: 22))
                    Text("Get AI Vet feeding recommendations tailored for each pet.")
                        .font(.custom("Poppins-Regular", size: 14))
                        .fixedSize(horizontal: false, vertical: true)
                }
            }
            //Poppins-Regular.
            HStack {
                Image("reminder")
                    .frame(width: 100, height: 100)
                    .scaledToFit()
                VStack (alignment: .leading, spacing: 5 ){
                    Text ("Reminders")
                        .font(.custom("Poppins-SemiBold", size: 22))
                    Text("Receive daily reminders about medication, supplements, and exercise.")
                        .font(.custom("Poppins-Regular", size: 14))
                        .fixedSize(horizontal: false, vertical: true)
                    
                    
                    
                }
                
            }
            
            Spacer()
            
            HStack ( spacing: 12) {
               Capsule()
                    .fill(Color("mainColor"))
                    .frame(width: 18, height: 7)
                    
                Capsule()
                    .fill(Color("mainColor").opacity(0.3))
                     .frame(width: 7, height: 7)
                Capsule()
                    .fill(Color("mainColor").opacity(0.3))
                     .frame(width: 7, height: 7)
                
            }
            .frame(maxWidth: .infinity)
            .padding()
        } .padding(.top , 10 )
        
        
        
    }
}

struct newFirstWelStackView_Previews: PreviewProvider {
    static var previews: some View {
        newFirstWelStackView()
    }
}
