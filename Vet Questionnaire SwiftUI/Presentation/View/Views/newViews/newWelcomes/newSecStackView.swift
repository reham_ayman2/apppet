//
//  newSecStackView.swift
//  Vet Questionnaire SwiftUI
//
//  Created by Reham Ayman on 28/08/2022.
//

import SwiftUI

struct newSecStackView: View {
    var body: some View {
        VStack ( alignment:  .leading , spacing: 20  ){
            
            HStack( ) {
                Image("tracking")
                    .frame(width: 100, height: 100)
                    .scaledToFit()
                VStack (alignment: .leading, spacing: 5 ) {
                    Text ("Tracking")
                        .font(.custom("Poppins-Bold", size: 22))
                    
                    
                    
                    Text("Complete daily surveys to track symptoms and treatment progress.")
                        .font(.custom("Poppins-Regular", size: 15))
                        .fixedSize(horizontal: false, vertical: true)
                }
                
               
                   
            }
            
            HStack( ) {
                Image("questions")
                    .frame(width: 100, height: 100)
                    .scaledToFit()
                VStack (alignment: .leading, spacing: 5 ) {
                    Text ("Questions")
                        .font(.custom("Poppins-Bold", size: 22))
                    
                    
                    
                    Text("Message A.I. Dr. Gary Richter about worrying issues, and figure out when to call a vet.")
                        .font(.custom("Poppins-Regular", size: 14))
                        .fixedSize(horizontal: false, vertical: true)
                }
                
               
                   
            }
            
            
            
            Text( "Give your pets the love they deserve.")
                .font(.custom("Poppins-Bold", size: 26))
                .fixedSize(horizontal: false, vertical: true)
                .frame(maxWidth: .infinity)
            Spacer()
            
            
            HStack ( spacing: 12) {
               Capsule()
                    .fill(Color("mainColor").opacity(0.3))
                    .frame(width: 7 , height: 7)
                    
                Capsule()
                    .fill(Color("mainColor"))
                     .frame(width: 18, height: 7)
                Capsule()
                    .fill(Color("mainColor").opacity(0.3))
                     .frame(width: 7, height: 7)
                
            }
        
            .frame(maxWidth: .infinity)
            
            
            
        
            
        } .padding(.top , 20 )
    }
}

struct newSecStackView_Previews: PreviewProvider {
    static var previews: some View {
        newSecStackView()
    }
}
