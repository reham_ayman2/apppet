//
//  newFirstWelcomeView.swift
//  Vet Questionnaire SwiftUI
//
//  Created by Reham Ayman on 23/08/2022.
//

import SwiftUI



struct newFirstWelcomeView: View {
    
    @State var showPopUp = false
    @State var rotatePop = false
    
    
    var body: some View {
        NavigationView  {
            
        ZStack {
      
            Color("mainColor").edgesIgnoringSafeArea(.all)
            VStack ( spacing: 80 ) {
                
          Image("SmallLogo")
                    
              .resizable()
           
              .frame(width: 200 , height: 100)
              .padding(.top , 50)
              .scaledToFit()
                
                
                
                
                
                
                
              
          
                VStack ( alignment:  .leading , spacing: 20  )  {
                   
                    HStack( ) {
                        Image("pet profile")
                            .frame(width: 100, height: 100)
                            .scaledToFit()
                        VStack (alignment: .leading, spacing: 5 ) {
                            Text ("Profile")
                                .font(.custom("Poppins-Bold", size: 20))
                            
                            
                            
                            Text("Create a health profile for each of your pets.")
                                .font(.custom("Poppins-Regular", size: 15))
                        }
                        
                       
                           
                    }
                    
                   
                    
                    HStack  {
                        Image("nutrition")
                            .frame(width: 100, height: 100)
                            .scaledToFit()
                        VStack(alignment: .leading, spacing: 5 ) {
                            Text ("Nutrition")
                                .font(.custom("Poppins-Bold", size: 20))
                            Text("Get AI Vet feeding recommendations tailored for each pet.")
                                .font(.custom("Poppins-Regular", size: 15))
                        }
                    }
                    //Poppins-Regular.
                    HStack {
                        Image("reminder")
                            .frame(width: 100, height: 100)
                            .scaledToFit()
                        VStack (alignment: .leading, spacing: 5 ){
                            Text ("title")
                                .font(.custom("Poppins-Bold", size: 20))
                            Text("sub title")
                                .font(.custom("Poppins-Regular", size: 15))
                        }
                        
                    }
                    
                    
                    
                    HStack ( spacing: 12) {
                       Capsule()
                            .fill(Color("mainColor"))
                            .frame(width: 18, height: 7)
                            
                        Capsule()
                            .fill(Color("mainColor").opacity(0.3))
                             .frame(width: 7, height: 7)
                        Capsule()
                            .fill(Color("mainColor").opacity(0.3))
                             .frame(width: 7, height: 7)
                        
                    }
                    .frame(maxWidth: .infinity)
                 
                   HStack ( spacing: 20 ) {
                       
                       NavigationLink(destination: EntryAuthenticationView () ) {
                           Text("Skip")
                               .font(.custom("Poppins-SemiBold", size: 16))
                       }
                       
                       
                       
                    .font(.custom("Poppins-Bold", size: 17 ))
                           .foregroundColor(Color.gray)
                       
                        NavigationLink("Start Now") {
                            
                            secWelcomeScreenView()
                            
                            
                        }.frame( maxWidth: .infinity)
                            .frame(height: 50)
                            
                            .foregroundColor(Color.white)
                            .background(Color.green)
                            .cornerRadius(10)
                            .padding()
                       
                   }.padding()
                    
                  
                  
                } .padding()
                
                
                .frame(maxWidth: .infinity)
                .frame( maxHeight: .infinity)
                
                
                
                .background(Color.white)
                
                
                
                .cornerRadius(30  )
                    
                .edgesIgnoringSafeArea(.all)
                
                
                
              

            
            }
           
             
        }
            .frame( maxWidth: .infinity , alignment: .top)
          
            
      
    }
        
        
        
        .navigationBarHidden(true)
        .navigationBarBackButtonHidden(true)
       
        
        //MARK: - LOADING VIEW
        
    
    }
        
    
    
        
    
}

struct newFirstWelcomeView_Previews: PreviewProvider {
    static var previews: some View {
      newFirstWelcomeView()
    }
}
