//
//  newTABview.swift
//  Vet Questionnaire SwiftUI
//
//  Created by Reham Ayman on 24/08/2022.
//

import SwiftUI

struct newTABview: View {
    
    @State private var selectedItem = 1
        
    
    
    var body: some View {
        
        
        NavigationView {
            ZStack (){
          
                Color("mainColor")
                VStack  {
                    
              Image("SmallLogo")
                        
                  .resizable()
               
                  .frame(width: 200 , height: 90)
                  .padding()
                  .padding(.top , 60 )
                 
                 
                    
                    Spacer(minLength: 50)
                  
                    
    VStack  {
        TabView (selection: $selectedItem) {
        newFirstWelStackView()
                .tag(1)
        newSecStackView()
                .tag(2)
        ThirdWelcViewStack()
                .tag(3)
            
        }
       
        .tabViewStyle(.page(indexDisplayMode: .never))
        .indexViewStyle(.page(backgroundDisplayMode: .always))
    
        
        
       
        
        if self.selectedItem == 1 {
            
            HStack ( spacing: 20 ) {
                                 
                NavigationLink(destination: EntryAuthenticationView () ) {
                    Text("Skip")
                        .font(.custom("Poppins-SemiBold", size: 16))
                }
                
                
                .font(.custom("Poppins-Bold", size: 17 ))
                 .foregroundColor(Color.gray)

                
                
                
                
                
                Button(action:     {
                    
                    withAnimation {
                    self.selectedItem = 2
                    
                    }
                    
                }, label: {
                    Text("Start Now")
                })

                .frame( maxWidth: .infinity)
                .frame(height: 50)
                .foregroundColor(Color.white)
                .background(Color.green)
                .cornerRadius(10)
                
                
                
                
          
                
                
                             }
            .padding()
          
            
        } else if self.selectedItem == 2 {
            HStack ( spacing: 20 ) {
                                 
                NavigationLink(destination: EntryAuthenticationView () ) {
                    Text("Skip")
                        .font(.custom("Poppins-SemiBold", size: 16))
                }
                
                
                .font(.custom("Poppins-Bold", size: 17 ))
                 .foregroundColor(Color.gray)

                
                
                
                
                
                Button(action:     {
                    
                    withAnimation {
                        self.selectedItem = 3
                    }
                  
                    
                    
                    
                }, label: {
                    Text("Create Pet Profile")
                })

                .frame( maxWidth: .infinity)
                .frame(height: 50)
                .foregroundColor(Color.white)
                .background(Color.green)
                .cornerRadius(10)
                
                
                
                
          
                
                
                             }
            .padding()
          
        } else {
            
            NavigationLink("Join The Movement", destination: {
                EntryAuthenticationView()
            })
            
            .frame( maxWidth: .infinity)
            .frame(height: 50)
            .foregroundColor(Color.white)
            .background(Color.green)
            .cornerRadius(10)
            .padding()
            
            

            
            
        }
     
          
     
          
                                
         }
        .padding()
        
        
        .frame(maxWidth: .infinity)
//     .frame( maxHeight: .infinity)
//        .frame( height: 500)
       // .background(Color.white)
        .background(RoundedCorners(color: .white, tl: 25 , tr: 25, bl: 0, br: 0))
      
     
                
    }
                
    }
            .edgesIgnoringSafeArea(.all )
        
}   .navigationBarHidden(true)
    .navigationBarBackButtonHidden(true)
    }}

struct newTABview_Previews: PreviewProvider {
    static var previews: some View {
        newTABview()
    }
}
