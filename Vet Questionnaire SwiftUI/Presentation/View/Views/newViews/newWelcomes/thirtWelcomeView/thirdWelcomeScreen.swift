//
//  thirdWelcomeScreen.swift
//  Vet Questionnaire SwiftUI
//
//  Created by Reham Ayman on 23/08/2022.
//

import SwiftUI

struct thirdWelcomeScreen: View {
    
    
    //MARK: -  LOADER VARS
    @State var  isPopUp = false
    @State var  rotate = false
    
    
    
    
    var body: some View {
        
        
        
        ZStack {
            Color("mainColor").edgesIgnoringSafeArea(.all)
            VStack ( spacing: 80 ) {
                
                Image("SmallLogo")
                          
                    .resizable()
                    .frame(width: 200 , height: 100)
                    .padding(.top , 30)
                    .scaledToFit()
                
                VStack ( alignment:  .leading , spacing: 20  ){
                
                    Image("did you know")
                        .resizable()
                        .frame(height: 140)
                        .padding(.leading , 40)
                        .padding(.trailing , 40)
                    
                    
                        
                    
                    
                    Text( "PetMetrics A.I. is designed to help give your pet a voice in their own care.")
                        .font(.custom("Poppins-SemiBold", size: 20))
                        .frame( maxWidth: .infinity)
                        .multilineTextAlignment(.center)
                        
                      
                    Image("dog talking")
                        .resizable()
                        .frame(height: 130 )
                        .frame( maxWidth: .infinity)
                        .padding(.leading , 40)
                        .padding(.trailing , 40)
                    
                  
                    HStack ( spacing: 12) {
                       Capsule()
                            .fill(Color("mainColor").opacity(0.3))
                            .frame(width: 7 , height: 7)
                            
                        Capsule()
                            .fill(Color("mainColor").opacity(0.3))
                             .frame(width: 7, height: 7)
                        Capsule()
                            .fill(Color("mainColor"))
                             .frame(width: 18, height: 7)
                        
                    }
                    .frame(maxWidth: .infinity)
                    
                    HStack ( spacing: 20 ) {
                        
                     
                         NavigationLink(destination:
                         
                                  EntryAuthenticationView()
                                            
                         ) {
                      
//
//
//
//                            withAnimation(.spring()){isPopUp.toggle()}
//                            rotate.toggle()
//                        }, label:
                            
                                
                                    Text("Join The Movement")
                                        .font(.custom("Poppins-SemiBold", size: 16))
                                }
                        
                        
                        
                        .frame( maxWidth: .infinity)
                             .frame(height: 50)
                             
                             .foregroundColor(Color.white)
                             .background(Color.green)
                             .cornerRadius(10)
                             .padding()
                        
                    }   .padding(.leading , 20)
                        .padding(.trailing , 20)
                    
                    
                
                
                }
               
                
                
                    .frame(maxWidth: .infinity)
                    .frame( maxHeight: .infinity)
                    .background(Color.white)
                    .cornerRadius(30  )
                    .edgesIgnoringSafeArea(.all)
                
                
                
            }
            
            //MARK: -  LOADER ISSSUES
            
            .overlay(
                ZStack {
                    if isPopUp {
                        Color.primary.opacity(0.5)
                            .ignoresSafeArea()
                        
                        
                        animationViewLoader(ShowPopUp: $isPopUp, rotate: $rotate)
                    }
                }
            
            
            )
        }
        
        .navigationBarHidden(true)
        .navigationBarBackButtonHidden(true)
        
    }
}

struct thirdWelcomeScreen_Previews: PreviewProvider {
    static var previews: some View {
        thirdWelcomeScreen()
    }
}
