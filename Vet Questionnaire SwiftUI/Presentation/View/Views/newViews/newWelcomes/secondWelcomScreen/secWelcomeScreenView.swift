//
//  secWelcomeScreenView.swift
//  Vet Questionnaire SwiftUI
//
//  Created by Reham Ayman on 23/08/2022.
//

import SwiftUI

struct secWelcomeScreenView: View {
    var body: some View {
      
        
        ZStack {
            Color("mainColor").edgesIgnoringSafeArea(.all)
            VStack ( spacing: 80 ) {
                
                Image("SmallLogo")
                          
                    .resizable()
                    .frame(width: 200 , height: 100)
                    .padding(.top , 50)
                    .scaledToFit()
                
                
                VStack ( alignment:  .leading , spacing: 20  ){
                    
                    HStack( ) {
                        Image("tracking")
                            .frame(width: 100, height: 100)
                            .scaledToFit()
                        VStack (alignment: .leading, spacing: 5 ) {
                            Text ("Tracking")
                                .font(.custom("Poppins-Bold", size: 22))
                            
                            
                            
                            Text("Complete daily surveys to track symptoms and treatment progress.")
                                .font(.custom("Poppins-Regular", size: 15))
                                .fixedSize(horizontal: false, vertical: true)
                        }
                        
                       
                           
                    }
                    
                    HStack( ) {
                        Image("questions")
                            .frame(width: 100, height: 100)
                            .scaledToFit()
                        VStack (alignment: .leading, spacing: 5 ) {
                            Text ("Questions")
                                .font(.custom("Poppins-Bold", size: 22))
                            
                            
                            
                            Text("Message A.I. Dr. Gary Richter about worrying issues, and figure out when to call a vet.")
                                .font(.custom("Poppins-Regular", size: 14))
                                .fixedSize(horizontal: false, vertical: true)
                        }
                        
                       
                           
                    }
                    
                    
                    
                    Text( "Give your pets the love they deserve.")
                        .font(.custom("Poppins-Bold", size: 26))
                        .fixedSize(horizontal: false, vertical: true)
                        .frame(maxWidth: .infinity)
                    
                    
                    
                    HStack ( spacing: 12) {
                       Capsule()
                            .fill(Color("mainColor").opacity(0.3))
                            .frame(width: 7 , height: 7)
                            
                        Capsule()
                            .fill(Color("mainColor"))
                             .frame(width: 18, height: 7)
                        Capsule()
                            .fill(Color("mainColor").opacity(0.3))
                             .frame(width: 7, height: 7)
                        
                    }
                
                    .frame(maxWidth: .infinity)
                    
                    HStack ( spacing: 20 ) {
                        
                        NavigationLink(destination: SignUpPageView().environmentObject(AuthViewModel())) {
                            Text("Skip")
                                .font(.custom("Poppins-SemiBold", size: 16))
                                .foregroundColor(Color.gray)
                        }
                        

                     .font(.custom("Poppins-Bold", size: 17 ))
                        
                         NavigationLink("Create Pet Profile") {
                            
                             thirdWelcomeScreen()
                                
                             
                         }.frame( maxWidth: .infinity)
                             .frame(height: 50)
                             
                             .foregroundColor(Color.white)
                             .background(Color.green)
                             .cornerRadius(10)
                             .padding()
                        
                    }.padding()
                    
                    
                    
                    
                    
                    
                    
                }.padding()
                
                
                    .frame(maxWidth: .infinity)
                    .frame( maxHeight: .infinity)
                    
                    
                    
                    .background(Color.white)
                    
                    
                    
                    .cornerRadius(30  )
                        
                    .edgesIgnoringSafeArea(.all)
                
            }
            
        }
        
        .frame(  maxWidth: .infinity,  maxHeight: .infinity)
        
        .navigationBarHidden(true)
        .navigationBarBackButtonHidden(true)
        
        
        
    }
}

struct secWelcomeScreenView_Previews: PreviewProvider {
    static var previews: some View {
        secWelcomeScreenView()
    }
}
