//
//  SwiftUIView.swift
//  Vet Questionnaire SwiftUI
//
//  Created by Reham Ayman on 25/08/2022.
//

import SwiftUI

struct animationViewLoader : View {
    @Environment (\.colorScheme) var scheme
    
    @Binding var ShowPopUp : Bool
    @Binding var rotate : Bool
   
    @State var animateBall = false
    @State var animateRotation = false
    
    
    
    var body: some View {
        ZStack {
            ( scheme == .dark ?  Color.black : Color.white )
                .frame(width: 150, height: 155)
                .cornerRadius(14)
                .shadow(color: Color.primary.opacity(0.07), radius: 5, x: 5, y: 5)
                .shadow(color: Color.primary.opacity(0.07), radius: 5, x: -5, y: -5)
            
            
            // .... cicle fill
            Circle()
                .fill(Color.gray.opacity(0.4))
                .frame(width: 40, height: 40)
            
            
                // rotating in x axies
            
                .rotation3DEffect(
                    
                    .init(degrees: 60)
                    
                    , axis: (x: 1 , y: 0 , z : 0.0)
                    , anchor: .center
                    , anchorZ: 0.0
                    , perspective: 1.0
                
                
                )
                .offset(y: 35)
                .opacity(animateBall ? 1 : 0 )
            
            //Icon-tennisball
            //Image-Demo
            Image("Image-Demo")
                .resizable()
                .aspectRatio(contentMode: .fit)
                .frame(width: 60, height: 60)
                .offset(y: animateBall ? 15 : -30)
                .rotationEffect(.init(degrees: rotate && animateRotation ? 360 : 0 ))
            
            
        
                
              

        }
            .overlay( Text ( "Loading...")
            .font(.custom("Poppins-Regular", size: 15))
            .padding(.bottom , 5)
            .foregroundColor(.gray)
            
                     
                      , alignment: .bottom )
        .onAppear(perform: {
            
            doAnimaton()
        })
    }
        func doAnimaton() {
            withAnimation(Animation.easeInOut(duration: 0.5) .repeatForever(autoreverses: true)) {
                animateBall.toggle()
            }
            
            
            withAnimation(Animation.easeInOut(duration: 0.8) .repeatForever(autoreverses: true )) {
                animateRotation.toggle()
            }
            
        }
    
}





