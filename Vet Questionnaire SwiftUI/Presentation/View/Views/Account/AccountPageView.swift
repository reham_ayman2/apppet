//
// Created by NoobMaster69 on 11/04/2022.
//

import Foundation
import SwiftUI
import KeyboardToolbars
import Focuser

struct AccountPageView: View {
    
    @Binding var destEnabled: Bool
    @EnvironmentObject var viewModel: AccountViewModel
    @FocusStateLegacy var focusedField: String? = ""
    @State var userName: String = UserDefaults.standard.string(forKey: UserConstants.userName.rawValue) ?? ""
    @State var emailAddress: String = UserDefaults.standard.string(forKey: UserConstants.email.rawValue) ?? ""
    @State var mobileNumber: String = UserDefaults.standard.string(forKey: UserConstants.phone.rawValue) ?? ""
    @State var isLoading: Bool = false
    @State var showingEditProfile = false
    @State var showingError = false
    @State var showDeleteConfirm = false
    @State var showingToast = false
    @State var addingPet = false
    @State var signOutActive = false
    let signOutDest = SignInPageView()
    @State var currentDeletingPet: PresentationPet? = nil
    var body: some View {
        
        NavigationView {
            ScrollView {
                VStack {
                    HeaderView()
                    Image(systemName: "person.fill")
                        .resizable()
                        .scaledToFit()
                        .foregroundColor(AppColors.primaryColor)
                        .frame(maxWidth: 100, maxHeight: 100)
                        .padding([.top, .leading, .trailing], 8)
                        .background(AppColors.lightBlue)
                        .clipShape(Circle())
                        .padding(.bottom, -50)
                        .zIndex(2)
                    ZStack(alignment: .top) {
                        
                        
                        VStack {
                            Text(viewModel.currentUser.name)
                                .bold()
                                .font(.custom("Poppins", fixedSize: 24))
                                .padding(.top, 60)
                            HStack {
                                Image("Icon-awesome-cat")
                                Text("\(viewModel.fetchCatCount())")
                                Image("Icon-awesome-dog")
                                Text("\(viewModel.fetchDogCount())")
                            }
                            Button("Edit Profile") {
                                showingEditProfile = true
                            }
                            .foregroundColor(.white)
                            .padding(.vertical, 12)
                            .padding(.horizontal, 40)
                            .background(Color.black)
                            .clipShape(RoundedRectangle(cornerRadius: 18))
                            .padding(.vertical, 16)
                            
                            Divider().padding(.horizontal)
                            
                            HStack {
                                Image(systemName: "pawprint.fill").foregroundColor(AppColors.primaryColor)
                                Text("My Pet(s)").bold().font(.custom("Poppins", size: 18))
                            }
                            .padding()
                            .frame(maxWidth: .infinity, alignment: .leading)
                            ScrollView(.horizontal) {
                                HStack {
                                    ForEach(viewModel.userPets) { pet in
                                        PetChipView(pet: pet, dismissible: true, onDismissClick: { deletedPet in
                                            showDeleteConfirm = true
                                            currentDeletingPet = pet
                                        })
                                    }
                                    .padding(8)
                                   
                                    
                                    AddPetChipView()
                                        .onTapGesture(perform: { addingPet = true })
                                    
                                }
                                .padding([.horizontal, .bottom])
                            }.onAppear(perform: {
                                self.viewModel.fetchUserPets()
                            })
                            .frame(minHeight: 80, maxHeight: 80)
                            
                            VStack {
                                NavigationLink(destination: PetProfilesPageView().environmentObject(PetProfilesViewModel())) {
                                    AccountMenuTabView(icon: "pawprint.fill", title: "Pet Profiles")
                                }
                                AccountMenuTabView(icon: "bell.fill", title: "Notifications")
                                AccountMenuTabView(icon: "gearshape.fill", title: "Settings")
                                AccountMenuTabView(icon: "questionmark.circle.fill", title: "Help & Support")
                                AccountMenuTabView(icon: "info.circle.fill", title: "About")
                                
                             
                                
                                NavigationLink(destination: signOutDest
                                        ) {
                                    AccountMenuTabView(icon: "arrow.forward.circle.fill", title: "Log out").onTapGesture {
                                        isLoading = true
                                        viewModel.signOut { apiResponse in
                                            isLoading = false
                                            showingToast = true
                                            if (apiResponse.isSuccessful()) {
                                                destEnabled = true
                                                
                                                print("logout user 👀 👋🏻👋🏻👋🏻👋🏻")
                                                
                                           
                                                    viewModel.clearUserData()
                                                    
                                                
                                               
                                               
                                                
                                                
                                            } else {
                                                showingError = true
                                                
                                                print("error ❌❌❌❌❌❌")
                                             
                                              
                                            }
                                        }
                                    }
                                }
                            }
                            .padding(.bottom)
                        }
                        .background(RoundedCorners(color: AppColors.lightBlue, tl: 24, tr: 24, bl: 0, br: 0))
                        
                    }
                }
                .overlay(LoadingView(isLoading: $isLoading))
                .alert(isPresented: $showingError) {
                    Alert(title: Text("Failed to sign out"), message: Text("Please try again"))
                }
            }
            .alert(isPresented: $showDeleteConfirm) {
                Alert(title: Text("Confirm deleting Pet"),
                      message: Text("Are you sure you wanna delete \(currentDeletingPet?.PetName ?? "")"),
                      primaryButton: .destructive(Text("Delete")) {
                    if let currentDeletingPet = currentDeletingPet {
                        viewModel.deletePet(currentDeletingPet) { response in
                            if response.isSuccessful() {
                                
                                viewModel.fetchUserPets()
                                print("Pet deleted")
                            }
                        }
                    }
                }, secondaryButton: .cancel())
            }
            .navigationBarTitle("")
            .navigationBarBackButtonHidden(true)
            .navigationBarHidden(true)
            .sheet(isPresented: $showingEditProfile, onDismiss: nil) {
                VStack {
                    Text("Edit Profile").bold().font(.title)
                    Image(systemName: "person.fill")
                        .resizable()
                        .scaledToFit()
                        .foregroundColor(AppColors.primaryColor)
                        .frame(maxWidth: 100, maxHeight: 100)
                        .padding([.top, .leading, .trailing], 8)
                        .background(AppColors.lightBlue)
                        .clipShape(Circle())
                        .padding()
                        .zIndex(2)
                    
                    RoundedBorderTextField(title: "Username", hint: "e.g. sampleusername", boundValue: $userName, focusedField: _focusedField)
                        .padding(.horizontal)
                    
                    
                    RoundedBorderTextField(title: "Email Address", hint: "e.g. sample@gmail.com", boundValue: $emailAddress, focusedField: _focusedField)
                        .padding(.horizontal)
                    
                    
                    RoundedBorderTextField(title: "Mobile Number", hint: "000 - 000 - 000", boundValue: $mobileNumber, focusedField: _focusedField, isLast: true)
                        .padding(.horizontal)
                    
                    Button("Save") {
                        isLoading = true
                        viewModel.updateProfile(name: userName, email: emailAddress, phone: mobileNumber) { updateResponse in
                            isLoading = false
                            showingEditProfile = false
                            showingToast = true
                            if updateResponse.isSuccessful() {
                                if let user = updateResponse.data?.updatedUser {
                                    viewModel.storeUserData(user)
                                }
                            }
                        }
                    }
                    .frame(maxWidth: .infinity, alignment: .center)
                    .padding()
                    .foregroundColor(.white)
                    .background(AppColors.secondaryColor)
                    .clipShape(RoundedRectangle(cornerRadius: 12))
                    .padding()
                }
                .overlay(LoadingView(isLoading: $isLoading).ignoresSafeArea())
            }
            .sheet(isPresented: $addingPet) {
                let viewModel = QuestionnaireViewModel();
                QuestionnaireView(isDialog: true) {
                    addingPet = false
                    //                            viewModel.restartQuestionnaire()
                    //                            self.viewModel.fetchUserPets()
                }
                .environmentObject(viewModel)
                //  Old Code
                //                     .environmentObject(QuestionnaireViewModel())
            }
            .toast(message: "Profile Updated successfully", isShowing: $showingToast, config: .init(duration: 0.5))
            .addHideKeyboardButton()
            
        }
        .navigationBarBackButtonHidden(true)
        .navigationBarHidden(true)
    }
}

struct AccountView_Previews: PreviewProvider {
    @State static var destEnables = true
    static var previews: some View {
        ZStack {
            AccountPageView(destEnabled: $destEnables).environmentObject(AccountViewModel())
        }
    }
}
