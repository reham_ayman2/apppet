//
//  AccountMenuTabView.swift
//  Vet Questionnaire SwiftUI
//
//  Created by NoobMaster69 on 11/04/2022.
//

import SwiftUI

struct AccountMenuTabView: View {
    let icon: String
    let title: String
    var body: some View {
        VStack {
            Divider()

            HStack {
                Image(systemName: icon)
                        .foregroundColor(AppColors.primaryColor)
                Text(title).font(.custom("Poppins-Bold", size: 18))

                Spacer()
                Image(systemName: "chevron.compact.right")
                    .resizable()
                    .frame(maxWidth: 12, maxHeight:16 )



            }
                    .padding()
        }
    }
}

struct AccountMenuTabView_Previews: PreviewProvider {
    static var previews: some View {
        AccountMenuTabView(icon: "gearshape.fill", title: "Settings")
    }
}
