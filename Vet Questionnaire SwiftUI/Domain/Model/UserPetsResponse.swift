//
// Created by NoobMaster69 on 13/04/2022.
//

import Foundation

struct UserPetsResponse: Codable {
    let createdPets: [APIPet]

    enum CodingKeys: String, CodingKey {
        case createdPets = "createdPet"
    }

}
