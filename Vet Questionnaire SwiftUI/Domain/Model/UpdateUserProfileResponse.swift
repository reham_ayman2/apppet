//
// Created by NoobMaster69 on 15/04/2022.
//

import Foundation

struct UpdateUserProfileResponse: Codable {
    let updatedUser: APIUser

    enum CodingKeys: String, CodingKey {
        case updatedUser = "UpdatedUser"
    }
}
