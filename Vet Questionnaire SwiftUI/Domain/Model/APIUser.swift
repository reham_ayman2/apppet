//
// Created by NoobMaster69 on 06/04/2022.
//

import Foundation




//struct APIUser: Codable {
//    let id : Int
//    let name : String
//    let email : String
//    let email_verified_at : String?
//    let password : String?
//    let created_at : String?
//    let updated_at : String?
//    let profile_photo_path : String?
//    let default_image : String?
//    let oauth_id : String?
//    let oauth_type : String?
//    let phone : String
//    let dogs_no : Int?
//    let cats_no : Int?
//
//    enum CodingKeys: String, CodingKey {
//
//        case id = "id"
//        case name = "name"
//        case email = "email"
//        case email_verified_at = "email_verified_at"
//        case password = "password"
//        case created_at = "created_at"
//        case updated_at = "updated_at"
//        case profile_photo_path = "profile_photo_path"
//        case default_image = "default_image"
//        case oauth_id = "oauth_id"
//        case oauth_type = "oauth_type"
//        case phone = "phone"
//        case dogs_no = "dogs_no"
//        case cats_no = "cats_no"
//    }}
//


struct APIUser: Codable {
    let id: Int
    let name: String
    let email: String?
    let phone: String?
    let dogsNo: Int?
    let catsNo: Int?

    enum CodingKeys: String, CodingKey {
        case id, name, email
        case phone
        case dogsNo = "dogs_no"
        case catsNo = "cats_no"
    }
}
