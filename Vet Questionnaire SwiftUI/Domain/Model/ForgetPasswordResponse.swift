//
// Created by NoobMaster69 on 05/04/2022.
//

import Foundation

struct ForgetPasswordResponse: Codable {
    let report: String
}
