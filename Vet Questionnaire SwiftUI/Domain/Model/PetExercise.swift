//
// Created by NoobMaster69 on 27/04/2022.
//

import Foundation

struct PetExercise: Codable {
    let id: Int
    let exerciseName: String
    let hours, mins: Int
    let createdAt, updatedAt: String
    let petEID: Int

    enum CodingKeys: String, CodingKey {
        case id, exerciseName, hours, mins
        case createdAt = "created_at"
        case updatedAt = "updated_at"
        case petEID = "petE_id"
    }
}
