//
// Created by NoobMaster69 on 06/04/2022.
//

import Foundation



struct SubmitPetResponse: Codable {
        let status : String?
        let message : String?
        let data : PetResponseData?

        enum CodingKeys: String, CodingKey {

            case status = "status"
            case message = "message"
            case data = "data"
        }
    
    
    
}


struct Conditions : Codable {
    let conditionsID : Int?
    let petCondition : String?
    let petConditionState : String?
    let created_at : String?
    let updated_at : String?

    enum CodingKeys: String, CodingKey {

        case conditionsID = "conditionsID"
        case petCondition = "PetCondition"
        case petConditionState = "PetConditionState"
        case created_at = "created_at"
        case updated_at = "updated_at"
    }
}


struct CreatedPet : Codable {
    let petName : String?
    let specie : String?
    let birthday : String?
    let petYears : Int?
    let petMonths : Int?
    let weight : Int?
    let gender : String?
    let spayedOr : Int?
    let image_path : String?
    let user_id : Int?
    let bodyConditionScore : Int?
    let worklevel : String?
    let watchingweight : Int?
    let losingweight : Int?
    let mixed : Int?
    let breedOne : String?
    let breedTwo : String?
    let pet_id : Int?
    let breedID : Int?
    let food : [CreatFood]?
    let medications : [CMedications]?
    let conditions : [Conditions]?

    enum CodingKeys: String, CodingKey {

        case petName = "PetName"
        case specie = "specie"
        case birthday = "birthday"
        case petYears = "PetYears"
        case petMonths = "PetMonths"
        case weight = "weight"
        case gender = "gender"
        case spayedOr = "spayedOr"
        case image_path = "image_path"
        case user_id = "user_id"
        case bodyConditionScore = "BodyConditionScore"
        case worklevel = "worklevel"
        case watchingweight = "watchingweight"
        case losingweight = "losingweight"
        case mixed = "mixed"
        case breedOne = "breedOne"
        case breedTwo = "breedTwo"
        case pet_id = "pet_id"
        case breedID = "BreedID"
        case food = "food"
        case medications = "medications"
        case conditions = "conditions"
    }
}

struct PetResponseData : Codable {
    let createdPet : [CreatedPet]?

    enum CodingKeys: String, CodingKey {

        case createdPet = "createdPet"
    }
}



    struct CreatFood : Codable {
        let foodID : Int?
        let unit : String?
        let caloriesPerUnit : Int?
        let noOfUnits : Int?
        let created_at : String?
        let updated_at : String?
        let foodName : String?

        enum CodingKeys: String, CodingKey {

            case foodID = "foodID"
            case unit = "unit"
            case caloriesPerUnit = "CaloriesPerUnit"
            case noOfUnits = "noOfUnits"
            case created_at = "created_at"
            case updated_at = "updated_at"
            case foodName = "foodName"
        }}
    
struct CMedications : Codable {
    let medicationsID : Int?
    let medicationName : String?
    let frequency : String?
    let updated_at : String?
    let created_at : String?

    enum CodingKeys: String, CodingKey {

        case medicationsID = "medicationsID"
        case medicationName = "medicationName"
        case frequency = "frequency"
        case updated_at = "updated_at"
        case created_at = "created_at"
    }}
    
