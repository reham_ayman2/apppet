//
//  StaticQuestions.swift
//  Vet Questionnaire SwiftUI
//
//  Created by NoobMaster69 on 04/03/2022.
//

import Foundation
import UIKit

// MARK: Static Questions
struct StaticQuestions {
    
    
    
    static func whatIsYourPetName(petName:String) -> PresentationQuestion{
        
        PresentationQuestion(content: "What's your pet name ?", answerType: .text, answers: [],questionType: .nonOptional) {
            builder, answer in
            builder.setName(answer as! String)
        }
    }
    
    
    
    static func whatIsYourPetSpecies(petName: String) -> PresentationQuestion {
        PresentationQuestion(content: "Tell us What's \(petName) Species ?",
                             answerType: .oneOfTwo,
                             answers: [PresentationAnswer(content: "Cat", image: UIImage(named: "cat")),
                                       PresentationAnswer(content: "Dog", image: UIImage(named: "dog"))],questionType: .nonOptional) { builder, answer in
            builder.setSpecies(answer as! String)
        }
    }
    
    
    
    static func whatIsYourCatBreed(petName: String) -> PresentationQuestion {
        PresentationQuestion(content: "What is \(petName) cat breed?",
                             answerType: .singleChoice,
                             answers: Breeds.catBreeds.map(PresentationAnswer.fromString),questionType: .optional) { builder, answer in
            
               return builder.setBreed(answer as! String)
            
    
        }
    }
    
    
    
    
    static func IsYourPetWorking(petName: String) -> PresentationQuestion {
        PresentationQuestion(content: "Is \(petName) Working Animal ?",
                             answerType: .oneOfTwo ,
                             answers: [PresentationAnswer.fromString("Yes"), PresentationAnswer.fromString("No")],questionType: .optional) { builder, answer in
            
               return builder.setIsAnimalWork(answer as! String == "Yes")
            
    
        }
    }
    
    static func whatIsYourWorkingLevel(petName: String) -> PresentationQuestion {
        PresentationQuestion(content: "What Is \(petName) Working Level",
                             answerType: .oneOfTwo ,
                             answers: Working.WorkBehave.map(PresentationAnswer.fromString),questionType: .nonOptional) { builder, answer in
            
               return builder.setWork(answer as! String )
            
    
        }
    }
    
    
    static func whatIspetGoal(petName: String) -> PresentationQuestion {
        PresentationQuestion(content: "What Is \(petName) Goal",
                             answerType: .medicalIssue ,
                             answers: goals.goalsBehave.map(PresentationAnswer.fromString),questionType: .nonOptional) { builder, answer in
            //(answer as! String == "Yes")
            return builder.setGoals(Iswatch: answer as! String == "Yes")
            
    
        }
    }
    
    
    
    static func whatIsYourDogBreed(petName: String) -> PresentationQuestion {
        PresentationQuestion(content: "What is \(petName) dog breed?",
                             answerType: .singleChoice,
                             answers: Breeds.dogsBreeds.map(PresentationAnswer.fromString),questionType: .optional) { builder, answer in
  
               return builder.setBreed(answer as! String)
        }
    }
    
    
    static func whatIsYourPetGender(petName: String) -> PresentationQuestion {
        PresentationQuestion(content: "What is \(petName) gender ?",
                             answerType: .oneOfTwo,
                             answers: [PresentationAnswer(content: "Male", image: UIImage(named: "male")),
                                       PresentationAnswer(content: "Female", image: UIImage(named: "female"))],questionType: .nonOptional) { builder, answer in
            builder.setGender(answer as! String)
        }
    }
    
    
    static func isYourPetSpayedOrNeutered(petName: String) -> PresentationQuestion {
        PresentationQuestion(content: "Is \(petName) Spayed or neutered ?",
                             answerType: .oneOfTwo,
                             answers: [PresentationAnswer.fromString("Yes"), PresentationAnswer.fromString("No")],questionType: .nonOptional) { builder, answer in
            builder.setSpayed(answer as! String == "Yes")
        }
    }
    
    static func whenIsYourPetBirthday(petName: String) -> PresentationQuestion {
        PresentationQuestion(content: "When is \(petName) birthday?",
                             answerType: .date, answers: [],questionType: .optional) { builder, answer in
            builder.setAge(age: answer as! String)
        }
    }
    
    
    static func whatIsYourPetWeight(petName: String) -> PresentationQuestion {
        PresentationQuestion(content: "What is \(petName)'s weight (Kg) ?", answerType: .number,
                             answers: (1...60).map { kgs in
            PresentationAnswer.fromString("\(kgs)")
        },questionType: .optional) { builder, answer in
            builder.setWeight(Int(answer as! String) ?? 0)
        }
    }
    
    
    
    
    // arrayes
    
    
    static func doesThePetTakeMedications(petName: String) -> PresentationQuestion {
        PresentationQuestion(content: "Does \(petName) take medications, including flea/tick or heartworm prevention?", answerType: .medicationQ,
                             answers: [],questionType: .optional) { builder, answer in
            
           // builder.setMedicens(answer as! [String] )
            
            print("doesThePetTakeMedications = \(answer)")
            
            
         return builder.setBreedTwo(answer as! String )
        }
    }
    
    
    
    
    
    
    static func letsTakePhoto(petName: String) -> PresentationQuestion {
        PresentationQuestion(content: "Let’s add a profile picture for \(petName).", answerType: .image,
                             answers: [],questionType: .optional) { builder, answer in
            builder.setImage((answer as! UIImage))
        }
    }
    
    
    static func NewFoodQuestions(petName: String) -> PresentationQuestion {
        PresentationQuestion(content: "What food do you feed \(petName) & how many calories?", answerType: .multi,
                             answers: [] ,questionType: .optional) { builder, answer in
            
            
            //food should add
           return builder.setFood(foods: answer as? [[String]] )
        }
    }
    
    
    
    
    //    static let whatCommercialBrandsDoYouUseInYourPetDiet =
    //            Question(content: "What commercial brands used in your pet diet", answerType: .multiChoice, answers: ["Ainsworth", "Blue Buffalo", "Diamond Pet Foods", "Nestlé Purina PetCare", "Mars Petcare / Royal Canin"].map(Answer.fromString)) { builder, answer in
    //                return builder
    //            }
    
    //    static let whatIsTheMealIngredients =
    //            Question(content: "What ingredients used in your pet diet", answerType: .multiChoice,
    //                    answers: [Answer.fromString("Egg"), Answer.fromString("ground beef"), Answer.fromString("Rice")]) { builder, answer in
    //                return builder
    //
    //            }
    //
    //
    
    static func whatIsYourPetBodyCondition() -> PresentationQuestion {
        PresentationQuestion( content: " Body Condition Score   (Check Image Below) ?", answerType: .bodyCondition, answers: [PresentationAnswer.fromString("1"), PresentationAnswer.fromString("3"),PresentationAnswer.fromString("5"), PresentationAnswer.fromString("7"),PresentationAnswer.fromString("9")],questionType: .nonOptional) { builder, answer in
            print("Body Condition Answer = \(answer)")
      return      builder.setWeight(Int(answer as! String) ?? 0)
        }
        
    }
    
    
    
    static func doesYourPetHaveAnyMedicalCondition(petName: String) -> PresentationQuestion {
        PresentationQuestion(content: "Does your pet have any specific medical condition?",
                             answerType: .oneOfTwo,
                             answers: [PresentationAnswer.fromString("Yes"), PresentationAnswer.fromString("No")],questionType: .optional) { builder, answer in
            print("doesYourPetHaveAnyMedicalCondition = \(answer)")
         return   builder.setSpayed(answer as! String == "Yes")
        }
    }
    
//    static func howManyCaloriesEat(petName: String) -> PresentationQuestion {
//        PresentationQuestion( content: "How many calories are in the food \(petName) is eating?", answerType: .caloriesEat, answers: [PresentationAnswer(content: ""), PresentationAnswer(content: "Kcal")],questionType: .nonOptional) { builder, answer in
//            builder.setBreedTwo(answer as! String )
//        }
//
//    }
    
    
    
    
    
    //
    static func howManyTimesFeedDaily(petName: String) -> PresentationQuestion {
        PresentationQuestion( content: "How many times is \(petName) fed daily?", answerType: .number, answers: [PresentationAnswer(content: ""), PresentationAnswer(content: "time(s)")],questionType: .nonOptional) { builder, answer in
            builder.setTimesFeed(answer as! String)
        }
        
    }
 
    
    static func whatCommercialBrandsDoesPetEat(petName: String) -> PresentationQuestion {
        
        PresentationQuestion(image: nil, content: "What food do you feed \(petName)?", answerType: .multiLineInserts, answers: Foods.petFood,questionType: .nonOptional) { builder, answer in
            builder.setBreedTwo(answer as! String )
        }
        
    }
    
    //    static let doYouWantToTrackYourPetActivity =
    //            Question(content: "Do you want to track your pet’s activity level?", answerType: .oneOfTwo, answers: ["Yes", "No"].map(Answer.fromString)) { builder, answer in
    //                return builder
    //            }
    //
    static let injuriesRanges = ["Mild", "Moderate", "Severe", "None"]
        .map {
        PresentationAnswer.fromString($0)
    }
    // added work
//    static let workingAns = ["light", "heavy"]
//        .map {
//        PresentationAnswer.fromString($0)
//    }
    
    static let discomfortProblems = [DiscomfortProblems.frontLeg.rawValue,DiscomfortProblems.backLeg.rawValue,DiscomfortProblems.spin.rawValue]
        .map {
        PresentationAnswer.fromString($0)
    }
    
   
    
    
    
    
    
    static func arthritisOtherJointProblemsDiscomfort(petName: String) -> PresentationQuestion {
        PresentationQuestion(content: "Does \(petName) have arthritis or joint discomfort?", answerType: .multiChoice,
                             answers: discomfortProblems,questionType: .optional) { builder, answer in
            print("Medeical Answers = \(answer)")
          return     builder
        }
    }

    
//    static func arthritisOtherJointProblemsFrontLeg(petName: String) -> PresentationQuestion {
//        PresentationQuestion(content: "Arthritis/other joint problems (Front Leg)", answerType: .singleChoice,
//                             answers: injuriesRanges,questionType: .optional) { builder, _ in
//            builder
//        }
//    }
//
//    static func arthritisOtherJointProblemsBackLeg(petName: String) -> PresentationQuestion {
//        PresentationQuestion(content: "Arthritis/other joint problems (Back Leg)", answerType: .singleChoice,
//                             answers: injuriesRanges,questionType: .optional) { builder, _ in
//            builder
//        }
//    }
//
//    static func arthritisOtherJointProblemsSpine(petName: String) -> PresentationQuestion {
//        PresentationQuestion(content: "Arthritis/other joint problems (Spine)", answerType: .singleChoice,
//                             answers: injuriesRanges,questionType: .optional) { builder, _ in
//            builder
//        }
//    }
    static func epilepsy(petName: String) -> PresentationQuestion {
        PresentationQuestion(content: "Does \(petName) have a history seizures?", answerType: .oneOfTwo,
                             answers: [PresentationAnswer.fromString("Yes"), PresentationAnswer.fromString("No")],questionType: .optional) { builder, answer in
            builder.setBreedTwo(answer as! String )
        }
    }
    
    static func anxiety(petName: String) -> PresentationQuestion {
        PresentationQuestion(content: "Does \(petName) have anxiety?", answerType: .oneOfTwo,
                             answers: [PresentationAnswer.fromString("Yes"), PresentationAnswer.fromString("No")],questionType: .optional) { builder, answer in
            builder.setBreedTwo(answer as! String)
        }
    }
    
    static func backProblems(petName: String) -> PresentationQuestion {



        PresentationQuestion(content: "Does \(petName) have back problems?", answerType: .multiChoicesCheckBox,
                             answers:[PresentationAnswer(content: "Intervertebral disc disease (IVDD)"),
                                      PresentationAnswer(content: "Spinal arthritis"),
                                      PresentationAnswer(content: "Other")
                                      ] ,questionType: .optional) { builder, answer in
            print("doesThePetTakeMedications = \(answer)")
         return builder

        }

    }
    
    static func skinAllergies(petName: String) -> PresentationQuestion {
        PresentationQuestion(content: "Does \(petName) have allergies?", answerType: .multiCheckBoxInserts,
                             answers: [PresentationAnswer(content: "Environmental allergies"),PresentationAnswer(content: "Spinal arthritis"),PresentationAnswer(content: "Flea allergies"),PresentationAnswer(content: "Other")],questionType: .optional) { builder, answer in
            
            builder
        }

    }
    
    static func gastrointestinalIssues(petName: String) -> PresentationQuestion {
        PresentationQuestion(content: "Does \(petName) have ongoing gastrointestinal issues such as vomiting or diarrhea?", answerType: .gastrointestinal,
                             answers: injuriesRanges,questionType: .optional) { builder, answer in
            builder.setBreedTwo(answer as! String )
        }

    }
    
    //  howManyCaloriesEat(petName: petName ?? ""),
     // howManyTimesFeedDaily(petName: petName ?? ""),
     // whatCommercialBrandsDoesPetEat(petName: petName ?? ""),
    static func initialQuestions(petName: String?) -> [PresentationQuestion] {
        
        print(" initialQuestions is called ")
        return  [whatIsYourPetName(petName: petName ?? ""), // 0
                 whatIsYourPetSpecies(petName: petName!), // 1
                 whatIsYourCatBreed(petName: petName ?? ""), // 2
                
                 whenIsYourPetBirthday(petName: petName ?? ""), // 3
                 whatIsYourPetWeight(petName: petName ?? ""), // 4
                 whatIsYourPetGender(petName: petName ?? ""), // 5
                 isYourPetSpayedOrNeutered(petName: petName ?? ""), // 6
                 whatIsYourPetBodyCondition(), // 7
                doesYourPetHaveAnyMedicalCondition(petName: petName ?? ""), // 8
                 
                 
                 
                 doesThePetTakeMedications(petName: petName ?? ""), // 9
                 arthritisOtherJointProblemsDiscomfort(petName: petName ?? ""), // 10
                    backProblems(petName: petName ?? ""), // 11
                    skinAllergies(petName: petName ?? ""), // 12
                    epilepsy(petName: petName ?? ""), // 13
                    anxiety(petName: petName ?? ""), // 14
                    gastrointestinalIssues(petName: petName ?? ""), // 15
                 
            // added work & goals
                 IsYourPetWorking(petName: petName ?? "" ), // 16
                 whatIsYourWorkingLevel(petName: petName ?? "" ), // 17
                 whatIspetGoal(petName: petName ?? "") , // 18
                 NewFoodQuestions(petName: petName ?? "" ) ,  // 19
                 letsTakePhoto(petName: petName ?? "") // 20
                 
                 //21 question
        ]
    }
    
    static func medicalQuestions(petName: String?) -> [PresentationQuestion] {
        
        [doesThePetTakeMedications(petName: petName ?? ""),
         arthritisOtherJointProblemsDiscomfort(petName: petName ?? ""),
                 backProblems(petName: petName ?? ""),
                 skinAllergies(petName: petName ?? ""),
                 epilepsy(petName: petName ?? ""),
                 anxiety(petName: petName ?? ""),
                 gastrointestinalIssues(petName: petName ?? "")
        ]
    }
}

enum DiscomfortProblems : String {
    case frontLeg = "Front legs"
    case backLeg = "Back legs"
    case spin = "Spin"
}
