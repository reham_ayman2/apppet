import Foundation
import UIKit

class OnBoardingSteps {
    static let firstOnBoardingSteps = [
        OnBoardingStep(content: "Offers Human Grade Care Your Pets Deserve", image: UIImage(named:"ob1")!),
        OnBoardingStep(content: "Making Pet Care\nAffordable", image: UIImage(named:"ob2")!),
        OnBoardingStep(content: "Making Pet Care\nAccessible", image: UIImage(named:"ob3")!)
    ]
    static let secondOnBoardingStep=[
        OnBoardingStep(title: "We’re Setting Up…", content: "We are setting up your profile. Your information will be saved for your future activities with Dr. Gary Richter."
                , image: UIImage(named:"ob5")!),

        OnBoardingStep(title: "Congratulations!", content:"Welcome to Dr. Gary Richter, (your pet’s name)! Your account has been successfully created in Dr. Gary Richter. Now you can enjoy all our services.",
                image: UIImage(named:"ob4")!)
    ]
}
