//
//  Cat breeds.swift
//  Vet Questionnaire SwiftUI
//
//  Created by NoobMaster69 on 10/03/2022.
//

import Foundation
class Breeds{
    static let catBreeds = ["Abyssinian Cat","British Shorthair Cat Breed","Bengal Cat"]
    static let dogsBreeds = ["Affenpinscher","Anatolian Shepherd Dog","American Staffordshire Terrier"]
}

class Working {
    static let WorkBehave = ["light","heavy"]
}

class goals {
    static let goalsBehave = ["Losing weight","Watching weight"]
}




class Foods {
    static let petFood = [
    PresentationAnswer(content: "Blue Buffalo"),
    PresentationAnswer(content: "Hills"),
    PresentationAnswer(content: "Just Food For Dogs"),
    PresentationAnswer(content: "Nutra Complete"),
    PresentationAnswer(content: "Primal"),
    PresentationAnswer(content: "Purina"),
    PresentationAnswer(content: "Taste of the Wild"),
    PresentationAnswer(content: "The Farmer’s Dog"),
    PresentationAnswer(content: "Royal Canin")
    ]
    
}
