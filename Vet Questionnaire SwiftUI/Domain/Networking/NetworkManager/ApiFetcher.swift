//
//  ApiFetcher.swift
//  ReusableNetworkLayer
//
//  Created by Bakr mohamed on 21/03/2022.
//

/*
    1- must be check preformance against alamofire (speed/processor/battery)
    2- check multi request diffrent and same result
 
 */

import Foundation
import Combine

protocol Fetcher {
    @discardableResult
    func fetch<ResponseType: Codable > (request: BaseRequestProtocol ,responseClass: ResponseType.Type) -> AnyPublisher<ResponseType,NetworkError>
    
    @discardableResult
    func fetchMultiPart<ResponseType>(request: MultipartRequestProtocol, responseClass: ResponseType.Type) -> AnyPublisher<ResponseType, NetworkError> where ResponseType : Decodable, ResponseType : Encodable
}


public class APIFetcher: Fetcher {
    func fetch<ResponseType>(request: BaseRequestProtocol, responseClass: ResponseType.Type) -> AnyPublisher<ResponseType, NetworkError> where ResponseType : Decodable, ResponseType : Encodable {
        //SessionConfiguration
        let sessionConfig = URLSessionConfiguration.default
        sessionConfig.timeoutIntervalForRequest = TimeInterval(request.requestTimeOut)
        let decoder = JSONDecoder()
        //Handel URL
        var urlRequest: URLRequest
        if let url =  URL(string: request.requestURL){
            urlRequest = generateUrlRequest(url: url, request: request)
        }else {
            // Return a fail publisher if the url is invalid
            return AnyPublisher(
                Fail<ResponseType, NetworkError>(error: NetworkError.badURL("Invalid Url"))
            )
        }
        
        // We use the dataTaskPublisher from the URLSession which gives us a publisher to play around with.
        return URLSession.shared
            .dataTaskPublisher(for: urlRequest)
            .receive(on: DispatchQueue.main)
            .validateResponse()
            .responseData()
            .decoding(type: ResponseType.self, decoder: decoder)
            .subscribe(on: DispatchQueue.main)
            .eraseToAnyPublisher()
        
    }
    
    fileprivate func generateUrlRequest(url: URL, request: BaseRequestProtocol) -> URLRequest {
        var urlRequest: URLRequest
        urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = request.httpMethod.rawValue
        urlRequest.allHTTPHeaderFields = request.baseHeaders
        
        switch request.httpMethod {
        case .GET:
            var urlComponents = URLComponents(url: url, resolvingAgainstBaseURL: false)
            var queryItems: [URLQueryItem] = []
            if let params = request.parameters {
                for (key, value) in params {
                    queryItems.append(URLQueryItem(name: key, value: value))
                }
                
            }
            urlComponents?.queryItems = queryItems
            urlRequest.url = urlComponents?.url ?? url
            
        default:
            
            if let params = request.parameters?.jsonData {
                urlRequest.httpBody = params
            }
        }
        
        return urlRequest
    }
    
}

//MARK: -Fetch MultiPart
extension APIFetcher {
    
    func fetchMultiPart<ResponseType>(request: MultipartRequestProtocol, responseClass: ResponseType.Type) -> AnyPublisher<ResponseType, NetworkError> where ResponseType : Decodable, ResponseType : Encodable {
        //SessionConfiguration
        let sessionConfig = URLSessionConfiguration.default
        sessionConfig.timeoutIntervalForRequest = TimeInterval(request.requestTimeOut)
        let decoder = JSONDecoder()
        //Handel URL
        var urlRequest: URLRequest
        
        // generate boundary string using a unique per-app string
        //A boundary is like a parameter for the API, letting the server know all the data sent in the request belongs to each other, as if they were all part of the same request.
        let boundary = generateBoundary()
       
        if let url =  URL(string: request.requestURL){
            urlRequest = URLRequest(url: url)
            urlRequest.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
            urlRequest.httpMethod = request.httpMethod.rawValue
            urlRequest.allHTTPHeaderFields = request.baseHeaders
            let body = generateMultiPartBodyData(withParamerters: request.parameters, attachments: request.attachments, boundary: boundary)
            urlRequest.httpBody = body
        }else {
            // Return a fail publisher if the url is invalid
            return AnyPublisher(
                Fail<ResponseType, NetworkError>(error: NetworkError.badURL("Invalid Url"))
            )
        }
        
        // We use the dataTaskPublisher from the URLSession which gives us a publisher to play around with.
        return URLSession.shared
            .dataTaskPublisher(for: urlRequest)
            .receive(on: DispatchQueue.main)
            .validateResponse()
            .responseData()
            .decoding(type: ResponseType.self, decoder: decoder)
            .eraseToAnyPublisher()
    }
    
    func generateBoundary() -> String {
        return "Boundary-\(NSUUID().uuidString)"
    }
    
    func generateMultiPartBodyData(withParamerters params: Parameters?, attachments: [MultipartAttachment]?, boundary: String) -> Data{
        var body = Data()
        let lineBreak = "\r\n"
        if let parameters = params{
            for (key,value) in parameters{
                body.append("--\(boundary + lineBreak)")
                body.append("Content-Disposition: form-data; name=\"\(key)\"\(lineBreak + lineBreak)")
                body.append("\(value + lineBreak)")

            }
        }
        
        if let attachments = attachments {
            for attachment in attachments {
                let data = attachment.data ?? Data()
                body.append("--\(boundary + lineBreak)")
                body.append("Content-Disposition: form-data; name=\"\(attachment.fileName)\"; filename=\"\(attachment.fileName)\"\(lineBreak)")
                body.append("Content-Type: \(data.mimeType + lineBreak + lineBreak)")
                body.append(data)
                body.append(lineBreak)
            }
        }
        body.append("--\(boundary)--\(lineBreak)")
        return body
    }
    
    /// Use this to check about internet connection
    static var isConnectedToInternet: Bool {
        return NetworkMonitor.shared.isReachable 
    }
}
