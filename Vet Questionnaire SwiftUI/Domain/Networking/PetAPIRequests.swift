//
// Created by NoobMaster69 on 29/04/2022.
//

import Foundation
import UIKit

class PetAPIRequests {

    static func submitPet(_ pet: PresentationPet, _ onPetSubmit: @escaping (APIResponse<SubmitPetResponse>) -> ()) {
        let requestURL = URL(string: APIService.BASE_URL + "CreatePet")
        let params: [String: Any] = ["PetName": pet.PetName ?? "",
                                     "specie": pet.species?.lowercased() ?? "",
                                     // birthday make sure to formate
                                     
                                     "birthday": pet.birthDate ?? "",
                                     "PetYears": pet.years ?? 0,
                                     "PetMonths": pet.months ?? 0,
                                     "weight": pet.weight ?? 0,
                                     "gender": pet.gender?.lowercased() ?? "male",
                                     "spayedOr": pet.isSpayed ?? false,
                                     "mixed": pet.isMixed ?? false,
                                     "breedOne": pet.breed ?? "",
                                     "image_path": pet.image?.jpegData(compressionQuality: 0.8) ?? "",
                                     
                                     // added
                                     "breedTwo":pet.breedTwo ?? "" ,
                                     "BodyConditionScore":pet.bodyScore ?? 0 ,
                                     "worklevel" : pet.work ?? "" ,
                                     "watchingweight" : pet.watchingweight ?? false ,
                                     "losingweight" : pet.losingweigh ?? false ,
       
                                     // 3 arrayes added "food" "medications"  "conditions"
                                     
                                     "food" : pet.food ?? [] ,
                                     "medications" : pet.medications ?? [] ,
                                     "conditions" : pet.conditions ?? []
                                     
                                     
                                     
                                     
                                     
        
        
       ]

        if let requestURL = requestURL {
//            APIService.uploadImage(urlString: APIService.BASE_URL + "CreatePet", paramName: "image_path", fileName: "sss", image: image)
            APIService.callPost(url: requestURL, params: params, finish: onPetSubmit)
        }
        
//        func uploadImage(data: Data, completionHandler: @escaping(_ result: APIResponse<SubmitPetResponse>) -> Void)
//           {
//
//               let imageUploadRequest = ImageRequest(attachment: data.base64EncodedString(), fileName: "multipartFormUploadExample")
//
//               APIService.postApiDataWithMultipartForm(requestUrl: requestURL!, request: imageUploadRequest, resultType: APIResponse<SubmitPetResponse>) {
//                   (response) in
//
//                   _ = completionHandler(response)
//
//               }
//    }
    }
    class func updatePet(_ pet: PresentationPet, finish: @escaping (_ apiResponse: APIResponse<UpdatePetResponse>) -> Void) {


        let params: [String: Any] = ["PetID": pet.id,
                                     "PetName": pet.PetName ?? "",
                                     "specie": pet.species ?? "cow",
                                     "birthday": pet.birthDate ?? "",
                                     "PetYears": pet.years ?? 0,
                                     "PetMonths": pet.months ?? 0,
                                     "weight": pet.weight ?? 0,
                                     "gender": pet.gender ?? "male",
                                     "user_id": UserDefaultsUtil.getCurrentUser().id,
                                     "BreedID":pet.breedId ?? 0,
                                     "spayedOr": pet.isSpayed ?? false,
//                                     "image_path": pet.image ?? "",
                                     "mixed": pet.isMixed  ?? false,
                                     "breedOne": pet.breed ?? "default",
                                     "breedTwo" : pet.breed ?? "default"
        ]
        let requestURL = URL(string: APIService.BASE_URL + "UpdatePetData")
        if let url = requestURL {
            APIService.callPost(url: url, params: params, finish: finish)
        }
    }

    static func deletePet(_ pet: PresentationPet, finish: @escaping (_ apiResponse: APIResponse<DeletePetResponse>) -> Void) {
        let requestURL = URL(string: APIService.BASE_URL + "DeletePet/\(pet.id)")
        if let url = requestURL {
            APIService.callDelete(url: url, finish: finish)
        }
    }
    
    //MARK: -  get user pets service

    static func getUserPets(onFinish: @escaping (_ apiResponse: APIResponse<UserPetsResponse>) -> ()) {
        let requestURL = URL(string: APIService.BASE_URL + "PetData")
        if let requestURL = requestURL {
            APIService.callGet(url: requestURL, params: [String: Any](), finish: onFinish)
        }
    }

    static func getPetActivities(petId: Int, onFinish: @escaping (_ apiResponse: APIResponse<PetActivitiesResponse>) -> ()) {
        let requestURL = URL(string: APIService.BASE_URL + "getExcercises/\(petId)")
        if let requestURL = requestURL {
            APIService.callGet(url: requestURL, params: [String: Any](), finish: onFinish)
        }
    }
//


    static func getPetFood(petId: Int, onFinish: @escaping (_ apiResponse: APIResponse<PetFoodResponse>) -> ()) {
        let requestURL = URL(string: APIService.BASE_URL + "getFoodActivity/\(petId)")
        if let requestURL = requestURL {
            APIService.callGet(url: requestURL, params: [String: Any](), finish: onFinish)
        }
    }
    
    
    static func getAllENtiers (petId: Int, onFinish: @escaping (_ apiResponse: APIResponse<AllENtiersdata>  ) -> ()) {
        let requestURL = URL(string: APIService.BASE_URL + "GetAllEntrie")
        print("🟢")
        print(requestURL)
        if let requestURL = requestURL {
            APIService.callGet(url: requestURL, params: [String: Any](), finish: onFinish)
        }
    }

    
    
    
    

    static func submitPetFood(_ id: Int, _ calories: String, _ onFinish: @escaping (APIResponse<SubmitPetFoodResponse>) -> ()) {

        let requestURL = URL(string: APIService.BASE_URL + "FoodActivity")
        if let url = requestURL {
            let params: [String: Any] = ["unit": "cup",
                                         "calPerUnit": 1,
                                         "noOfUnits": calories,
                                         "petA_id": id,
                                         "product[0]": 1,
                                         "product[1]": 2,
                                         "homeIngredient[0]": 1,
                                         "homeIngredient[1]": 2,
            ]
            APIService.callPost(url: url, params: params, finish: onFinish)
        }
    }

    static func submitPetExercise(_ id: Int, _ exercise: String, _ hours: Int, _ mins: Int, _ onFinish: @escaping (APIResponse<SubmitPetExerciseResponse>) -> ()) {
        let requestURL = URL(string: APIService.BASE_URL + "CreateExcercise")

        if let url = requestURL {
            let params: [String: Any] = ["exerciseName": exercise, "hours": hours, "mins": mins, "petE_id": id]
            APIService.callPost(url: url, params: params, finish: onFinish)
        }
    }
}
