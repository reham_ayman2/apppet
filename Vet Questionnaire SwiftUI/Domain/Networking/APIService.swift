//
// Created by NoobMaster69 on 05/04/2022.
//

import Foundation
import MobileCoreServices
import UniformTypeIdentifiers
import UIKit
import SwiftUI

class APIService {
    static let BASE_URL = "https://petappb.000webhostapp.com/api/"

    private static func getPostString(params: [String: Any]?) -> String? {
            var data = [String]()
        if let params = params {
            for (key, value) in params {
                data.append(key + "=\(value)")
            }
//            print("Data mapped = \(data.map {String($0)}.joined(separator: "&"))")
            return data.map {
                        String($0)
                    }
                    .joined(separator: "&")
        } else {
            return nil
        }
    }
    

 
    static func callDelete<T>(url: URL, finish: @escaping (_ apiResponse: APIResponse<T>) -> Void) {
        var request = URLRequest(url: url)
        request.httpMethod = "DELETE"

        var result: APIResponse<T> = APIResponse<T>(status: "0", message: "Failed", data: nil)
        let task = URLSession.shared.dataTask(with: request) { data, response, error in

            if (error != nil) {
                result.message = "Fail Error not null : \(error.debugDescription)"
                print("Error info: \(String(describing: error))")

            } else {
                if let data = data {
                    do {

                        result = try JSONDecoder().decode(APIResponse<T>.self, from: data)
                    } catch {
                        print("Error info: \(error)")
                    }
                }
            }
            finish(result)
        }
        task.resume()
    }

    static func callGet<T>(url: URL, params: [String: Any], finish: @escaping (_ apiResponse: APIResponse<T>) -> Void) {
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        if let token = UserDefaults.standard.string(forKey: UserConstants.authToken.rawValue) {
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
            request.addValue("application/json", forHTTPHeaderField: "Accept")
            request.addValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
            print(token)
        }
       
        if !params.isEmpty {
            
            let postString = getPostString(params: params)
            request.httpBody = postString?.data(using: .utf8)
        }

        var result: APIResponse<T> = APIResponse<T>(status: "0", message: "Failed", data: nil)
        let task = URLSession.shared.dataTask(with: request) { data, response, error in

            if (error != nil) {
                result.message = "Fail Error not null : \(error.debugDescription)"
            } else {
                if let data = data {
                    do {

                        result = try JSONDecoder().decode(APIResponse<T>.self, from: data)

                    } catch {
                        print("Error info: \(error)")
                    }
                }
            }
            finish(result)
        }
        task.resume()
    }

    static func callPost<T>(url: URL, params: [String: Any], finish: @escaping (_ apiResponse: APIResponse<T>) -> Void) {
        
        var request = URLRequest(url: url)

        request.httpMethod = "POST"
     
//        let lineBreak = "\r\n"
        
        if let token = UserDefaults.standard.string(forKey: UserConstants.authToken.rawValue) {
            request.addValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
            print(token)
        }
        

        let postString = getPostString(params: params)
        request.httpBody =  postString?.data(using: .utf8)
        var result: APIResponse<T> = APIResponse<T>(status: "0", message: "Failed", data: nil)
      
        let task = URLSession.shared.dataTask(with: request) { data, response, error in

            
            if (error != nil) {
                result.message = "Fail Error not null : \(error.debugDescription)"
            } else {
                if let data = data {
                    let stringResponse = String(data: data, encoding: .utf8)
                    print(stringResponse ?? "Error In String Response")
                    do {
                        result = try JSONDecoder().decode(APIResponse<T>.self, from: data)
                    } catch {
                     
                        //to view the data you receive from the API
                        print("Error info: \(error)")
                    }
                }
            }
             finish(result)
        }
        task.resume()
    }
}

