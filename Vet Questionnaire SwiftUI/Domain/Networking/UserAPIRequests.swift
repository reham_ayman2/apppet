//
// Created by NoobMaster69 on 15/04/2022.
//

import Foundation

class UserAPIRequests {

    static func login(email: String, password: String,
                      onFinish: @escaping (_ apiResponse: APIResponse<SignInResponse>) -> Void) {
        
        
        
   let requestURL = URL(string: APIService.BASE_URL + "Login")
        let params: [String: Any] = ["email": email, "password": password]
        
        
        if let requestURL = requestURL {
            APIService.callPost(url: requestURL, params: params, finish: onFinish)
        }
    }

    static func signUp(name: String, email: String, password: String, phone: String, onFinish: @escaping (_ apiResponse: APIResponse<SignUpResponse>) -> ()) {
        let requestURL = URL(string: APIService.BASE_URL + "SignUp")
        let params = ["name": name, "email": email, "password": password, "phone": phone]
        if let requestURL = requestURL {
            APIService.callPost(url: requestURL, params: params, finish: onFinish)
        }
    }

    static func signOut(onFinish: @escaping (_ apiResponse: APIResponse<LogOutResponse>) -> ()) {
        let requestURL = URL(string: APIService.BASE_URL + "Logout")

        if let requestURL = requestURL {
            APIService.callGet(url: requestURL, params: [String:Any](), finish: onFinish)
        }
    }

    static func forgetPassword(email: String, onFinish: @escaping (_ apiResponse: APIResponse<ForgetPasswordResponse>) -> ()) {
        let requestURL = URL(string: APIService.BASE_URL + "reset")
        let params = ["email": email]
        if let requestURL = requestURL {
            APIService.callPost(url: requestURL, params: params, finish: onFinish)
        }
    }

    static func updateProfile(name: String, email: String, phone: String, onFinish: @escaping (_ apiResponse: APIResponse<UpdateUserProfileResponse>) -> ()) {
        let requestURL = URL(string: APIService.BASE_URL + "UpdateUserData")
        let params = ["name": name, "email": email, "phone": phone]
        if let requestURL = requestURL {
            APIService.callPost(url: requestURL, params: params, finish: onFinish)
        }
    }
}
