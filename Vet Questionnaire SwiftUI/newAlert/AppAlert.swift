//
//  AppAlert.swift
//  SeaTrips
//
//  Created by Reham Ayman on 11/4/20.
//  Copyright © 2020 Reham Ayman. All rights reserved.
//

import Foundation
import UIKit
import SPAlert



func showSuccessAlert(title:String,message:String){
    SPAlert.present(title: message, preset: .done)
}

func showErrorAlert(title:String,message:String){
    SPAlert.present(title: message, preset: .error)

}
func showWarningAlert(title:String,message:String){
    SPAlert.present(title: message, preset: .custom(UIImage(named: "AppLogo")!))
    
    
}

func completData(title:String,message:String){
    

SPAlert.present(message:message, haptic: .error)

}
func showNoInterNetAlert(){

    SPAlert.present(title: "please make sure you are connected to the internet", preset: .error)
    
    
}



