//
//  ImageService.swift
//  Vet Questionnaire SwiftUI
//
//  Created by Oufaa on 16/06/2022.
//


import SwiftUI
/// user ObservableObject ImageLoader to support before ios 15 but if ios 15 can user AsyncImage
final class ImageLoaderServices: ObservableObject {
    @Published var image: Image? = nil
    
    func loadImage(loadFromUrlString urlString: String ) {
        ImageNetworkManager.shared.downloadImage(fromURLString: urlString) { [weak self] uiImage in
            guard let uiImage = uiImage else {return}
            DispatchQueue.main.async { [weak self] in
                self?.image = Image(uiImage: uiImage)
            }
        }
    }
}

final class ImageNetworkManager {
    static let shared = ImageNetworkManager()
    private let cache = NSCache<NSString,UIImage>()
    
    func downloadImage(fromURLString urlString: String, completion: @escaping (UIImage?)-> Void){
        let cacheKey = NSString(string: urlString)
        if let image = cache.object(forKey: cacheKey){
            completion(image)
            return
        }
        
        guard let url = URL(string: urlString) else {
            completion(nil)
            return
        }
        let task = URLSession.shared.dataTask(with: URLRequest(url: url)) { data, response, error in
            guard let data = data , let image = UIImage(data: data) else {
                completion(nil)
                return
            }
            self.cache.setObject(image, forKey: cacheKey)
            completion(image)
            
        }
        task.resume()
        
    }
}
