//
//  ImageLoader.swift
//  Vet Questionnaire SwiftUI
//
//  Created by Oufaa on 16/06/2022.
//

import Foundation
import SwiftUI



struct ActivityIndicator: UIViewRepresentable {
    
    
    func makeCoordinator() -> () {
        
    }
    
    func makeUIView(context: Context) -> UIActivityIndicatorView {
        let activityIndicatorView = UIActivityIndicatorView(style: .large)
        activityIndicatorView.color = .darkGray
        activityIndicatorView.startAnimating()
        return activityIndicatorView
    }
    
    func updateUIView(_ uiView: UIActivityIndicatorView, context: Context) {
    }
    
}

struct ImageLoadingView: View {
    var body: some View {
        ZStack {
            Color(.systemBackground)
                .edgesIgnoringSafeArea(.all)
            ActivityIndicator()
        }
    }
}
