//
//  StaticQuestions.swift
//  Vet Questionnaire SwiftUI
//
//  Created by NoobMaster69 on 04/03/2022.
//

import Foundation
import UIKit

// MARK: Static Questions
struct StaticQuestions {
    
    
    
    static func whatIsYourPetName(petName:String) -> PresentationQuestion{
        
        PresentationQuestion(content: "What's your pet name ?", answerType: .text, answers: []) { builder, answer in
                builder.setName(answer as! String)
            }
    }
    static func whatIsYourPetSpecies(petName: String) -> PresentationQuestion {
        PresentationQuestion(content: "Tell us What's \(petName) Species ?",
                answerType: .oneOfTwo,
                             answers: [PresentationAnswer(content: "Cat", image: UIImage(named: "cat")),
                          PresentationAnswer(content: "Dog", image: UIImage(named: "dog"))]) { builder, answer in
            builder.setSpecies(answer as! String)
        }
    }

    static func whatIsYourCatBreed(petName: String) -> PresentationQuestion {
        PresentationQuestion(content: "What is \(petName) cat breed?",
                answerType: .singleChoice,
                answers: Breeds.catBreeds.map(PresentationAnswer.fromString)) { builder, answer in
            builder.setBreed(answer as! String)
        }
    }


    static func whatIsYourDogBreed(petName: String) -> PresentationQuestion {
        PresentationQuestion(content: "What is \(petName) dog breed?",
                answerType: .singleChoice,
                answers: Breeds.dogsBreeds.map(PresentationAnswer.fromString)) { builder, answer in
            builder.setBreed(answer as! String)
        }
    }


    static func whatIsYourPetGender(petName: String) -> PresentationQuestion {
        PresentationQuestion(content: "What is \(petName) gender ?",
                answerType: .oneOfTwo,
                answers: [PresentationAnswer(content: "Male", image: UIImage(named: "male")),
                          PresentationAnswer(content: "Female", image: UIImage(named: "female"))]) { builder, answer in
            builder.setGender(answer as! String)
        }
    }


    static func isYourPetSpayedOrNeutered(petName: String) -> PresentationQuestion {
        PresentationQuestion(content: "Is \(petName) Spayed or neutered ?",
                answerType: .oneOfTwo,
                answers: [PresentationAnswer.fromString("Yes"), PresentationAnswer.fromString("No")]) { builder, answer in
            builder.setSpayed(answer as! String == "Yes")
        }
    }

    static func whenIsYourPetBirthday(petName: String) -> PresentationQuestion {
        PresentationQuestion(content: "When is \(petName) birthday?",
                answerType: .date, answers: []) { builder, answer in
            builder.setAge(age: answer as! String)
        }
    }


    static func whatIsYourPetWeight(petName: String) -> PresentationQuestion {
        PresentationQuestion(content: "What is \(petName)'s weight (pounds) ?", answerType: .number,
                answers: (1...60).map { kgs in
                    PresentationAnswer.fromString("\(kgs)")
                }) { builder, answer in
            builder.setWeight(Int(answer as! String) ?? 0)
        }
    }

    static func letsTakePhoto(petName: String) -> PresentationQuestion {
        PresentationQuestion(content: "Let’s add a profile picture for \(petName).", answerType: .image,
                answers: []) { builder, answer in
            builder.setImage((answer as! UIImage))
        }
    }

//    static let whatCommercialBrandsDoYouUseInYourPetDiet =
//            Question(content: "What commercial brands used in your pet diet", answerType: .multiChoice, answers: ["Ainsworth", "Blue Buffalo", "Diamond Pet Foods", "Nestlé Purina PetCare", "Mars Petcare / Royal Canin"].map(Answer.fromString)) { builder, answer in
//                return builder
//            }

//    static let whatIsTheMealIngredients =
//            Question(content: "What ingredients used in your pet diet", answerType: .multiChoice,
//                    answers: [Answer.fromString("Egg"), Answer.fromString("ground beef"), Answer.fromString("Rice")]) { builder, answer in
//                return builder
//
//            }
//
//
//    static let whatIsYourPetBodyCondition =
//            Question(image: UIImage(named: "bodies"), content: "Tell us the body condition of your pet.", answerType: .singleChoice, answers: ["Obese", "Overweight", "Ideal", "Very thin", "Under weight"].map(Answer.fromString)) { builder, answer in
//                return builder.setBodyScore(Int(answer) ?? 0)
//            }
//
//    static let doYouWantToTrackYourPetActivity =
//            Question(content: "Do you want to track your pet’s activity level?", answerType: .oneOfTwo, answers: ["Yes", "No"].map(Answer.fromString)) { builder, answer in
//                return builder
//            }
//
    static let injuriesRanges = ["Mild", "Moderate", "Severe", "None"].map {
        PresentationAnswer.fromString($0)
    }

    static func arthritisOtherJointProblemsFrontLeg(petName: String) -> PresentationQuestion {
        PresentationQuestion(content: "Arthritis/other joint problems (Front Leg)", answerType: .singleChoice,
                answers: injuriesRanges) { builder, _ in
            builder
        }
    }

    static func arthritisOtherJointProblemsBackLeg(petName: String) -> PresentationQuestion {
        PresentationQuestion(content: "Arthritis/other joint problems (Back Leg)", answerType: .singleChoice,
                answers: injuriesRanges) { builder, _ in
            builder
        }
    }

    static func arthritisOtherJointProblemsSpine(petName: String) -> PresentationQuestion {
        PresentationQuestion(content: "Arthritis/other joint problems (Spine)", answerType: .singleChoice,
                answers: injuriesRanges) { builder, _ in
            builder
        }
    }
    static func epilepsy(petName: String) -> PresentationQuestion {
        PresentationQuestion(content: "Epilepsy", answerType: .singleChoice,
                answers: injuriesRanges) { builder, _ in
            builder
        }
    }

    static func anxiety(petName: String) -> PresentationQuestion {
        PresentationQuestion(content: "Anxiety", answerType: .singleChoice,
                answers: injuriesRanges) { builder, _ in
            builder
        }
    }

    static func backProblems(petName: String) -> TwoAnswerTypesQuestion {
        TwoAnswerTypesQuestion(primaryTitle: "Back Problems",
                secondaryTitle: "Specify Back Problem",
                primaryAnswers: injuriesRanges,
                secondaryAnswers: ["Intervertebral Disc Disease", "Spinal arthritis", "Other back problem"]
                        .map {
                            PresentationAnswer.fromString($0)
                        },
                primaryType: .singleChoice,
                secondaryType: .multiChoice)
    }

    static func skinAllergies(petName: String) -> TwoAnswerTypesQuestion {
        TwoAnswerTypesQuestion(primaryTitle: "Skin Allergies",
                secondaryTitle: "Specify Skin Allergy",
                primaryAnswers: injuriesRanges,
                secondaryAnswers: ["Environmental allergies", "Food-related allergies", "Flea allergies", "Other skin allergy"]
                        .map {
                            PresentationAnswer.fromString($0)
                        },
                primaryType: .singleChoice,
                secondaryType: .multiChoice)
    }

    static func gastrointestinalIssues(petName: String) -> TwoAnswerTypesQuestion {
        TwoAnswerTypesQuestion(primaryTitle: "Gastrointestinal Issues",
                secondaryTitle: "Specify Gastrointestinal Issue",
                primaryAnswers: injuriesRanges,
                secondaryAnswers: ["Vomiting", "Diarrhea", "Other gastrointestinal Issue"]
                        .map {
                            PresentationAnswer.fromString($0)
                        },
                primaryType: .singleChoice,
                secondaryType: .multiChoice)
    }


    static func initialQuestions(petName: String?) -> [PresentationQuestion] {
        
        print(" initialQuestions is called ")
      return  [whatIsYourPetName(petName: petName ?? ""),whatIsYourPetSpecies(petName: petName!),
               whatIsYourCatBreed(petName: petName ?? ""),
               whatIsYourPetGender(petName: petName ?? ""),
     isYourPetSpayedOrNeutered(petName: petName ?? "")
                   , whenIsYourPetBirthday(petName: petName ?? ""),
         whatIsYourPetWeight(petName: petName ?? ""), letsTakePhoto(petName: petName ?? "")
      ]
    }

    static func medicalQuestions(petName: String) -> [PresentationQuestion] {
        [arthritisOtherJointProblemsFrontLeg(petName: petName),
         arthritisOtherJointProblemsBackLeg(petName: petName),
         arthritisOtherJointProblemsSpine(petName: petName),
         backProblems(petName: petName),
         skinAllergies(petName: petName),
         epilepsy(petName: petName),
         anxiety(petName: petName),
         gastrointestinalIssues(petName: petName)
        ]
    }
}
