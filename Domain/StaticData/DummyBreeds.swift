//
//  Cat breeds.swift
//  Vet Questionnaire SwiftUI
//
//  Created by NoobMaster69 on 10/03/2022.
//

import Foundation
class Breeds{
    static let catBreeds = ["Abyssinian Cat","British Shorthair Cat Breed","Bengal Cat"]
    static let dogsBreeds = ["Affenpinscher","Anatolian Shepherd Dog","American Staffordshire Terrier"]
}
