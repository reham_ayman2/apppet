//
//  DummyPet.swift
//  Vet Questionnaire SwiftUI
//
//  Created by NoobMaster69 on 07/04/2022.
//

import Foundation

class DummyPets{
    static let dummyPet = PresentationPet.Builder()
        .setName("Mahdy")
        .setSpecies("cat")
        .setAge(age: "2022-2-2,1,2")
        .setBreed("Catto")
        .build()

}
