//
// Created by NoobMaster69 on 05/04/2022.
//

import Foundation
import MobileCoreServices
import UniformTypeIdentifiers
import UIKit

class APIService {
    static let BASE_URL = "https://petappb.000webhostapp.com/api/"

    private static func getPostString(params: [String: Any]?) -> String? {
        var data = [String]()
        if let params = params {
            for (key, value) in params {
                data.append(key + "=\(value)")
            }
//            print("Data mapped = \(data.map {String($0)}.joined(separator: "&"))")
            return data.map {
                        String($0)
                    }
                    .joined(separator: "&")
        } else {
            return nil
        }
    }

 
    static func callDelete<T>(url: URL, finish: @escaping (_ apiResponse: APIResponse<T>) -> Void) {
        var request = URLRequest(url: url)
        request.httpMethod = "DELETE"

        var result: APIResponse<T> = APIResponse<T>(status: "0", message: "Failed", data: nil)
        let task = URLSession.shared.dataTask(with: request) { data, response, error in

            if (error != nil) {
                result.message = "Fail Error not null : \(error.debugDescription)"
                print("Error info: \(String(describing: error))")

            } else {
                if let data = data {
                    do {

                        result = try JSONDecoder().decode(APIResponse<T>.self, from: data)
                    } catch {
                        print("Error info: \(error)")
                    }
                }
            }
            finish(result)
        }
        task.resume()
    }

    static func callGet<T>(url: URL, params: [String: Any], finish: @escaping (_ apiResponse: APIResponse<T>) -> Void) {
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        if let token = UserDefaults.standard.string(forKey: UserConstants.authToken.rawValue) {
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
            request.addValue("application/json", forHTTPHeaderField: "Accept")
            request.addValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
        }
        if !params.isEmpty {
            let postString = getPostString(params: params)
            request.httpBody = postString?.data(using: .utf8)
        }

        var result: APIResponse<T> = APIResponse<T>(status: "0", message: "Failed", data: nil)
        let task = URLSession.shared.dataTask(with: request) { data, response, error in

            if (error != nil) {
                result.message = "Fail Error not null : \(error.debugDescription)"
            } else {
                if let data = data {
                    do {

                        result = try JSONDecoder().decode(APIResponse<T>.self, from: data)

                    } catch {
                        print("Error info: \(error)")
                    }
                }
            }
            finish(result)
        }
        task.resume()
    }

    static func callPost<T>(url: URL, params: [String: Any], finish: @escaping (_ apiResponse: APIResponse<T>) -> Void) {
        
        var request = URLRequest(url: url)

        request.httpMethod = "POST"
        
        
        
        if let token = UserDefaults.standard.string(forKey: UserConstants.authToken.rawValue) {
            request.addValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
            print(token)
        }
        
    
        let imageKey = params["image_path"] != nil
        
        if imageKey {
            
            let boundary = "Boundary-\(UUID().uuidString)"
            let fileURL = Bundle.main.url(forResource: "images_path", withExtension: "Jpeg") ?? URL(fileURLWithPath: "")
            
            request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
            do{
                request.httpBody = try createBody(with: params, filePathKey: "file", urls: [fileURL], boundary: boundary)}catch{print(error)}
        }else {
            let postString = getPostString(params: params)
            request.httpBody =  postString?.data(using: .utf8)
        }
        var result: APIResponse<T> = APIResponse<T>(status: "0", message: "Failed", data: nil)
      
        let task = URLSession.shared.dataTask(with: request) { data, response, error in

            if (error != nil) {
                result.message = "Fail Error not null : \(error.debugDescription)"
            } else {
                if let data = data {
                    let stringResponse = String(data: data, encoding: .utf8)
                    print(stringResponse ?? "Error In String Response")
                    do {
                        result = try JSONDecoder().decode(APIResponse<T>.self, from: data)
                    } catch {
                        print("Error info: \(error)")
                    }
                }
            }
             finish(result)
        }
        task.resume()
    }
    
    

//    private static func createBody(with parameters: [String: Any]? = nil, filePathKey: String, urls: [URL], boundary: String) throws -> Data {
//        var body = Data()
//
//        parameters?.forEach { (key, value) in
//            body.append("--\(boundary)\r\n")
//            body.append("Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n")
//            body.append("\(value)\r\n")
//        }
//
//        for url in urls {
//            let filename = url.lastPathComponent
//            let data = try Data(contentsOf: url)
//
//            body.append("--\(boundary)\r\n")
//            body.append("Content-Disposition: form-data; name=\"\(filePathKey)\"; filename=\"\(filename)\"\r\n")
//            body.append("Content-Type: \(url.mimeType)\r\n\r\n")
//            body.append(data)
//            body.append("\r\n")
//        }
//
//        body.append("--\(boundary)--\r\n")
//        return body
//    }

//    static  func postApiDataWithMultipartForm<T:Decodable>(requestUrl: URL, request: APIPet, resultType: T.Type, completionHandler:@escaping(_ result: T)-> Void)
//       {
//
//           var urlRequest = URLRequest(url: requestUrl)
//           let lineBreak = "\r\n"
//
//           if let token = UserDefaults.standard.string(forKey: UserConstants.authToken.rawValue) {
//               urlRequest.addValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
//               print(token)
//           }
//           urlRequest.httpMethod = "post"
//           let boundary = "---------------------------------\(UUID().uuidString)"
//           urlRequest.addValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "content-type")
//
//           var requestData = Data()
//
//           requestData.append("--\(boundary)\r\n" .data(using: .utf8)!)
//           requestData.append("content-disposition: form-data; name=\"attachment\" \(lineBreak + lineBreak)" .data(using: .utf8)!)
//
//           requestData.append(request.imagePath .data(using: .utf8)!)
//
//           requestData.append("\(lineBreak)--\(boundary)\r\n" .data(using: .utf8)!)
//           requestData.append("content-disposition: form-data; name=\"fileName\" \(lineBreak + lineBreak)" .data(using: .utf8)!)
//
//
//           requestData.append("--\(boundary)--\(lineBreak)" .data(using: .utf8)!)
//
//           urlRequest.addValue("\(requestData.count)", forHTTPHeaderField: "content-length")
//           urlRequest.httpBody = requestData
//
//          // let multipartStr = String(decoding: requestData, as: UTF8.self) //to view the multipart form string
//           URLSession.shared.dataTask(with: urlRequest) { (data, httpUrlResponse, error) in
//               if(error == nil && data != nil && data?.count != 0)
//               {
//               // let dataStr = String(decoding: requestData, as: UTF8.self) //to view the data you receive from the API
//                   do {
//                       let response = try JSONDecoder().decode(T.self, from: data!)
//                       _=completionHandler(response)
//                   }
//                   catch let decodingError {
//                       debugPrint(decodingError)
//                   }
//               }
//
//           }.resume()
//
//       }
//
}

extension Data {
    mutating func append(_ string: String) {
        self.append(string.data(using: .utf8, allowLossyConversion: true)!)
    }
}

extension URL {
    /// Mime type for the URL
    ///
    /// Requires `import UniformTypeIdentifiers` for iOS 14 solution.
    /// Requires `import MobileCoreServices` for pre-iOS 14 solution

    var mimeType: String {
        if #available(iOS 14.0, *) {
            return UTType(filenameExtension: pathExtension)?.preferredMIMEType ?? "application/octet-stream"
        } else {
            guard
                let identifier = UTTypeCreatePreferredIdentifierForTag(kUTTagClassFilenameExtension, pathExtension as CFString, nil)?.takeRetainedValue(),
                let mimeType = UTTypeCopyPreferredTagWithClass(identifier, kUTTagClassMIMEType)?.takeRetainedValue() as String?
            else {
                return "application/octet-stream"
            }

            return mimeType
        }
    }
}
