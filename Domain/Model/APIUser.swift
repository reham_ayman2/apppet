//
// Created by NoobMaster69 on 06/04/2022.
//

import Foundation

struct APIUser: Codable {
    let id: Int
    let name: String
    let email: String
    let phone: String
    let dogsNo: Int?
    let catsNo: Int?

    enum CodingKeys: String, CodingKey {
        case id, name, email
        case phone
        case dogsNo = "dogs_no"
        case catsNo = "cats_no"
    }
}
