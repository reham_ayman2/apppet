//
// Created by NoobMaster69 on 05/04/2022.
//

import Foundation

struct APIResponse<T: Decodable>: Decodable {
    var status: String
    var message: String?
    var data: T?

    func isSuccessful() -> Bool {
        status.trimmingCharacters(in: .whitespacesAndNewlines).starts(with: "2")
    }
}
