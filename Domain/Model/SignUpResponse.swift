//
// Created by NoobMaster69 on 05/04/2022.
//

import Foundation

struct SignUpResponse: Codable {
    let user: APIUser?
    let token: String
    enum CodingKeys: String, CodingKey {
        case token
        case user = "User"
    }
}
