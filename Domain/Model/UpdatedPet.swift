//
// Created by NoobMaster69 on 06/05/2022.
//

import Foundation

struct UpdatedPet: Codable {
    let petID: Int
    let petName, specie, birthday: String
    let petYears, petMonths, weight: Int
    let gender: String
    let spayedOr: Int
    let imagePath: String?
    let userID, breedID, mixed: Int
    let breedOne, breedTwo: String

    enum CodingKeys: String, CodingKey {
        case petID = "PetID"
        case petName = "PetName"
        case specie, birthday
        case petYears = "PetYears"
        case petMonths = "PetMonths"
        case weight, gender, spayedOr
        case imagePath = "image_path"
        case userID = "user_id"
        case breedID = "BreedID"
        case mixed, breedOne, breedTwo
    }
}
