//
// Created by NoobMaster69 on 29/04/2022.
//

import Foundation

struct FoodActivity: Codable {
    let foodActivityID: Int
    let unit: String
    let petAID, calPerUnit, noOfUnits: Int
    let createdAt, products, ingredients: String?

    enum CodingKeys: String, CodingKey {
        case foodActivityID = "foodActivity_id"
        case unit
        case petAID = "petA_id"
        case calPerUnit, noOfUnits
        case createdAt = "created_at"
        case products, ingredients = "ingerients"
    }
}
