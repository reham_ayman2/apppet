//
// Created by NoobMaster69 on 13/04/2022.
//

import Foundation

struct APIPet: Codable, Hashable {
    let id : Int
    let petName: String
    let specie: String
    let birthday: String
    let petYears, petMonths, weight: Int
    let gender: String
    let spayedOr: Int
    let imagePath: String
    let userID, breedID, mixed: Int
    let breedOne: String
    let breedTwo: String?

    enum CodingKeys: String, CodingKey {
        case id = "PetID"
        case petName = "PetName"
        case specie, birthday
        case petYears = "PetYears"
        case petMonths = "PetMonths"
        case weight, gender, spayedOr
        case imagePath = "image_path"
        case userID = "user_id"
        case breedID = "BreedID"
        case mixed, breedOne, breedTwo
    }

}
