//
// Created by NoobMaster69 on 12/04/2022.
//

import Foundation

struct LogOutResponse : Codable {
    let report: String?
}
