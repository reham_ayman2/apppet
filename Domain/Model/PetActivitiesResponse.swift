//
// Created by NoobMaster69 on 27/04/2022.
//

import Foundation

struct PetActivitiesResponse: Codable {
    let petExercises: [PetExercise]

    enum  CodingKeys : String , CodingKey{
        case petExercises = "petExcercises"
    }
}
