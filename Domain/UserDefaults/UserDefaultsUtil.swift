//
// Created by NoobMaster69 on 15/04/2022.
//

import Foundation

class UserDefaultsUtil {
    static func setUserToken(_ token: String) {
        let defaults = UserDefaults.standard
        defaults.set(token, forKey: UserConstants.authToken.rawValue)
    }

    static func setUser(_ user: APIUser) {
        let defaults = UserDefaults.standard
        defaults.set(user.name, forKey: UserConstants.userName.rawValue)
        defaults.set(user.phone, forKey: UserConstants.phone.rawValue)
        defaults.set(user.email, forKey: UserConstants.email.rawValue)
        defaults.set(user.id, forKey: UserConstants.uid.rawValue)
        defaults.set(user.catsNo, forKey: UserConstants.catsNo.rawValue)
        defaults.set(user.dogsNo, forKey: UserConstants.dogsNo.rawValue)
        
    }

    static func getCurrentUser() -> APIUser {
        let defaults = UserDefaults.standard
        return APIUser(id: defaults.integer(forKey: UserConstants.uid.rawValue),
                name: defaults.string(forKey: UserConstants.userName.rawValue) ?? "",
                email: defaults.string(forKey: UserConstants.email.rawValue) ?? "",
                phone: defaults.string(forKey: UserConstants.phone.rawValue) ?? "",
                dogsNo: defaults.integer(forKey: UserConstants.dogsNo.rawValue),
                catsNo: defaults.integer(forKey: UserConstants.catsNo.rawValue))

    }
}
