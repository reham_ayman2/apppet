//
//  UserDefaultsConstants.swift
//  Vet Questionnaire SwiftUI
//
//  Created by NoobMaster69 on 12/04/2022.
//

import Foundation

enum UserConstants : String {
   case authToken, userName, phone, email, uid, catsNo , dogsNo
}